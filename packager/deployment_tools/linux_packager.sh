#!/bin/sh

if [ ! "$1" ] 
then
   echo "Qt dir not set";
   exit
else
   if [ ! "$2" ]
   then
	echo "qmake executable not set";
	exit
   fi
fi


if [ -e linuxdeployqt-continuous-x86_64.AppImage ] 
then
    echo "Found linuxdeployqt"
else
    echo "linuxdeployqt does not work..."
    exit
fi
chmod a+x linuxdeployqt-continuous-x86_64.AppImage
if ./linuxdeployqt-continuous-x86_64.AppImage | grep -q 'linuxdeployqt'; then
  echo "linuxdeployqt works!!!"
fi
rm -rf ../../deploy_build
mkdir ../../deploy_build
cmake -G"Unix Makefiles" -B../../deploy_build/ -H../../
make -C ../../deploy_build -j
cp ../../deploy_build/omen ../../deploy_build/anex ../packages/com.delphis.omen/data/
if "$2" --version | grep -q 'QMake version'; then
  echo "qmake works!!!"
  ./linuxdeployqt-continuous-x86_64.AppImage ../packages/com.delphis.omen/data/omen -qmake="$2"
else

  ./linuxdeployqt-continuous-x86_64.AppImage ../packages/com.delphis.omen/data/omen
  echo "invalid qmake"
fi

"$1"/Tools/QtInstallerFramework/3.0/bin/binarycreator --offline-only -c ../config/config.xml -p ../packages/ -t "$1"/Tools/QtInstallerFramework/3.0/bin/installerbase OmenInstaller
