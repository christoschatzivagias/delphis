#include <Utils.h>

#include <string>
#include <stdlib.h>
#include <sstream>

using namespace std;

namespace SpectralClustering {

Utils::Utils() {
	// TODO Auto-generated constructor stub

}

Utils::~Utils() {
	// TODO Auto-generated destructor stub
}

/*
 * Converts a given char* (string) to an integer
 */
int Utils::convertToInt(char* s){
	return atoi(s);
}

/*
 * Converts the given string into a double datatype
 */
double Utils::convertToDouble(string& s) {
	istringstream instr(s);

	double value;
	instr >> value;

	return value;
}

}
