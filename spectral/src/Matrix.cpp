#include <Matrix.h>
#include <stdio.h>
#include <vector>
#include <stdlib.h>
#include <cmath>
#include <string>
#include <sstream>
using namespace std;

namespace SpectralClustering {

Matrix::~Matrix() {
	//	for(int i=0; i< rows; i++){
	//		delete matrix[i];
	//	}

	//delete matrix;
}

/*
 * Initializes the matrix by allocating the necessary memory space
 */
void Matrix::initMatrix() {
	try {
		matrix = new double *[rows];

		for (int i = 0; i < rows; i++) {
			matrix[i] = new double[cols];
		}
	} catch (std::bad_alloc &exc) {
		throw "Error when allocating matrix space";
		//		exit(1);
	}
}

/*
 *
 */
double** Matrix::getMatrixDataTable() {
	return matrix;
}

/*
 * getter
 */
int Matrix::getHeight() {
	return rows;
}

/*
 * getter
 */
int Matrix::getWidth() {
	return cols;
}

/*
 * setter
 */
void Matrix::setValue(int row, int column, double value) {
	matrix[row][column] = value;
}

/*
 * getter
 */
double Matrix::getValue(int row, int column) {
	return matrix[row][column];
}

/*
 * returns the matrix' row, identified by the parameter "row"
 */
double* Matrix::getMatrixRow(int row) {
	return matrix[row];
}

/*
 * returns the matrix' column, identified by the parameter "column"
 */
double* Matrix::getMatrixColumn(int column) {
	double* resultColumn = new double[rows];

	for (int i = 0; i < rows; i++) {
		resultColumn[i] = matrix[i][column];
	}

	return resultColumn;
}

double Matrix::computeRowSum(int rowIndex) {
	double sum = 0;

	for (int i = 0; i < getWidth(); i++) {
		sum = sum + getValue(rowIndex, i);
	}

	return sum;
}

/*******************************************
 * BEGIN Operator overloadings
 *******************************************/
Matrix& Matrix::operator=(const Matrix& operand) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			matrix[i][j] = operand.matrix[i][j];
		}
	}
	return *this;
}

Matrix Matrix::operator -(const Matrix& operand) {
	Matrix result = Matrix(rows, cols);

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			double value = matrix[i][j] - operand.matrix[i][j];
			result.setValue(i, j, value);
		}
	}

	return result;
}

Matrix Matrix::operator*(const Matrix& operand) {
	Matrix result = Matrix(rows, cols);

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			double *temp = calculateColumn(operand, j);
			double value = multiply(matrix[i], temp);
			delete temp;
			result.setValue(i, j, value);
		}
	}

	return result;
}

/*******************************************
 * END Operator overloadings
 *******************************************/

/*******************************************
 * BEGIN Utility functions
 *******************************************/
double* Matrix::calculateColumn(Matrix matrix, int colIndex) {
	double* resultColumn = new double[rows];

	for (int i = 0; i < rows; i++) {
		resultColumn[i] = matrix.getValue(i, colIndex);
	}

	return resultColumn;
}

double Matrix::multiply(double* row, double* column) {
	double result = 0;

	//iterate by number of rows or cols, should be equal
	//since we have a square matrix
	for (int i = 0; i < rows; i++) {
		result += row[i] * column[i];
	}

	return result;
}

/*******************************************
 * END Utility functions
 *******************************************/

string Matrix::toString(void) {
	ostringstream os;

	for (int i = 0; i < getHeight(); i++) {
		os << "[";
		for (int j = 0; j < getWidth(); j++) {
			if(j == getWidth()-1)
				os << getValue(i, j) << " ";
			else
				os << getValue(i, j) << ", ";
		}
		os << "]\n";
	}

	return os.str();

}

}
