#include "DBScenery.h"
#include "spectral/CSVReader.h"
#include "spectral/DataPoint.h"
#include "spectral/KMeans.h"
#include "spectral/ClusteringController.h"
#include "spectral/Matrix.h"
#include "spectral/Utils.h"
#include <data_struct/CsvData.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

using namespace std;
using namespace SpectralClustering;

/*
 * Usage:
 * ./SpectralClustering <k> <sigma> < <PathToCSVFile>
 */

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow("DBScenery");

	Utils* util = new Utils();

	CsvData *userData = NULL;
	//if we have a second parameter, use it as the path
	//to the user-defined csv data file
	if(argc > 1 && argc < 3){
		string path = argv[1];
		userData = new CsvData(argv[1], "User's dataset");
	}else if(argc > 2){
		//user defined a wrong number of parameters
		cout << "Panic, wrong number of parameters!" << endl;
		cout << "Usage:" << endl;
		cout << "\tSpectralClustering <pathToCSVFile>"  << endl;
		cout << "Example:" << endl;
		cout << "\t./SpectralClustering ../csvFiles/dataSet1.csv" << endl;
		return EXIT_FAILURE;
	}

	DBScenery *scenery = new DBScenery();
	scenery->initOpenGL();

	//initializing the algorithm
	ClusteringController* clustering = new ClusteringController();
	CsvData *set1 = new CsvData("../data/dataSet1.csv", "Good dataset");
	CsvData *set2 = new CsvData("../data/dataSet2.csv", "Bad dataset");
	CsvData *set3 = new CsvData("../data/spiralRingsK2S9.csv", "Spiral rings (2D)");
	CsvData *set4 = new CsvData("../data/two3DLines.csv", "Two 3D lines (3D)");
	CsvData *set5 = new CsvData("../data/parabolas.csv", "Parabolas (3D)");
	CsvData *set6 = new CsvData("../data/spheresWith100Noise.csv", "Spheres with uniform noise (3D)");
	CsvData *set7 = new CsvData("../data/twoSpheresSpiralK3S14.csv", "Spheres and spiral ring (3D)");
	CsvData *set8 = new CsvData("../data/twoLinesSphereSpiralK4S2.csv", "Lines, sphere and and spiral (3D)");


	//adding the algorithm
	scenery->addAlgorithm(clustering);

	//if different from NULL, add the user-defined dataset
	if(userData != NULL)
		scenery->addRelation(userData);

	//add other, predefined datasets
	scenery->addRelation(set1);
	scenery->addRelation(set2);
	scenery->addRelation(set3);
	scenery->addRelation(set4);
	scenery->addRelation(set5);
	scenery->addRelation(set6);
	scenery->addRelation(set7);
	scenery->addRelation(set8);

	glutMainLoop();
	delete scenery;

	return EXIT_SUCCESS;
}
