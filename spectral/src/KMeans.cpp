#include <KMeans.h>
#include <DataPoint.h>
#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include "CommonFunctionsContainer.hpp"
namespace SpectralClustering
{

KMeans::KMeans()
{
    // TODO Auto-generated constructor stub
}

KMeans::~KMeans()
{
    // TODO Auto-generated destructor stub
}

int* KMeans::k_means(Matrix matrix, int k, double t, map<string, int> mapIndex, double **centroids, vector<DataPoint> *rcentroids, int distance_metric, int minklamda)
{
    double** data = matrix.getMatrixDataTable();

    int n = matrix.getHeight();
    int m = matrix.getWidth();

    /* output cluster label for each data point */
    int *labels = (int*) calloc(n, sizeof(int));

    int h, i, j; /* loop counters, of course :) */
    int *counts = (int*) calloc(k, sizeof(int)); /* size of each cluster */
    double old_error, error = DBL_MAX; /* sum of squared euclidean distance */
    double **centrs = centroids ? centroids : (double**) calloc(k, sizeof(double*));
    double **c1 = (double**) calloc(k, sizeof(double*)); /* temp centroids */

    assert(data && k> 0 && k <= n && m> 0 && t >= 0); /* for debugging */

    /****
     ** initialization */

    for (h = i = 0; i < k; h += n / k, i++)
    {
        c1[i] = (double*) calloc(m, sizeof(double));
        if (!centroids)
        {
            centrs[i] = (double*) calloc(m, sizeof(double));
        }
        /* pick k points as initial centroids */
        for (j = m; j-- > 0; centrs[i][j] = data[h][j])
            ;
    }

    /****
     ** main loop */

    do
    {
        /* save error from last step */
        old_error = error, error = 0;

        /* clear old counts and temp centroids */
        for (i = 0; i < k; counts[i++] = 0)
        {
            for (j = 0; j < m; c1[i][j++] = 0)
                ;
        }

        for (h = 0; h < n; h++)
        {
            /* identify the closest cluster */
            double min_distance = DBL_MAX;
            for (i = 0; i < k; i++)
            {
                double distance = 0;
                vector<double> pt1, pt2,weights;
               // for(j = m; j-- > 0; distance += pow(data[h][j] - centrs[i][j], 2));
                for(j = m; j-- > 0;)
                {
                    pt1.push_back(data[h][j]);
                    pt2.push_back(centrs[i][j]);
                    weights.push_back(1);
                }


                switch(distance_metric)
                {
                    case Euclidean:
                        distance = euklidean(pt1, pt2, weights);
                        break;
                    case Manhattan:
                        distance = manhattan(pt1, pt2, weights);
                        break;
                    case Canberra:
                        distance = canberra(pt1, pt2, weights);
                        break;
                    case Czekanowski:
                        distance = czekanowski(pt1, pt2, weights);
                        break;
                    case Chebychev:
                        distance = chebychev(pt1, pt2, weights);
                        break;
                    case Kaufmann_Roussee:
                        //distance = kaufman_rousseeuw(pt1, pt2, target);
                        break;
                    case Minkowski:
                        distance = minkowski(pt1, pt2, weights, minklamda);
                        break;
                    case InvalidDistanceMetric:
                        break;
                }

                if (distance < min_distance)
                {
                    labels[h] = i;
                    min_distance = distance;
                }
            }
            /* update size and temp centroid of the destination cluster */
            for (j = m; j-- > 0; c1[labels[h]][j] += data[h][j])
                ;
            counts[labels[h]]++;
            /* update standard error */
            error += min_distance;
        }

        for (i = 0; i < k; i++)   /* update all centroids */
        {
            for (j = 0; j < m; j++)
            {
                centrs[i][j] = counts[i] ? c1[i][j] / counts[i] : c1[i][j];
            }
        }

    }
    while (fabs(error - old_error) > t);

    /****
     ** housekeeping */
    rcentroids->clear();


    for (i = 0; i < k; i++)
    {
        DataPoint centroid;
        map<string, int>::iterator it = mapIndex.begin();
        for(it =  mapIndex.begin(); it !=  mapIndex.end(); ++it)
            //for (j = 0; j < m; j++)
        {
            centroid.addAttribute(it->first, centrs[i][it->second]);
        }
        rcentroids->push_back(centroid);
    }

    for (i = 0; i < k; i++)
    {
        if (!centroids)
        {
            free(centrs[i]);
        }
        free(c1[i]);
    }

    if (!centroids)
    {
        free(centrs);
    }
    free(c1);

    free(counts);

    return labels;
}
}
