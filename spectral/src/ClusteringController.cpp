#include <ClusteringController.h>
#include <DataPoint.h>
#include <Matrix.h>
#include <KMeans.h>
//#include <data_struct/SceneryData.h>
#include "GlobalObjectsContainer.hpp"
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>
#include <stdio.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include "BraceEnums.c"


using namespace std;

namespace SpectralClustering
{

ClusteringController::ClusteringController()
{
    ASSIGNEDCLUSTERS = NULL;
    DEBUGPRINT = false;
    EULERNUMBER = EULER_NUMBER;
    numberOfClusters = 1;
    sigma = 1;
    normalizedMatrix = new Matrix(1,1);
    centroids = new vector<DataPoint>;
}

ClusteringController::~ClusteringController()
{
    delete centroids;
}

int *ClusteringController::getClusterIndexes()
{
    return ASSIGNEDCLUSTERS;
}

void ClusteringController::setNumberOfClusters(int value)
{
    numberOfClusters = value;
}

int ClusteringController::getNumberOfClusters()
{
    //the number of clusters must be at least 1
    if(numberOfClusters == 0)
        numberOfClusters = 1;
    return numberOfClusters;
}

void ClusteringController::setSigma(double value)
{
    sigma = value;
}

double ClusteringController::getSigma()
{
    if(sigma == 0)
        sigma = 1;
    return sigma;
}

void ClusteringController::debug(bool debug)
{
    DEBUGPRINT = debug;
}

void ClusteringController::doClustering(vector<DataPoint*> &dataPoints, int k, int dimension, double sigma, int distance_metric, int minklamda)
{
    KMeans* kMeans = new KMeans();
    if(DEBUGPRINT)
    {
        cout << "Chosen number of clusters: " << k << "\n";
        cout << "Chosen sigma value: " << sigma << "\n";
        cout << "Input data:\n";
        for (int i = 0; i < dataPoints.size(); i++) {
            cout << dataPoints[i]->toString() << "\n";
        }
        cout << "Computing similarity matrix...\n";
    }

    Matrix similarityMatrix = createSimilarityMatrix(dataPoints, sigma);


    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Similarity matrix:\n";
        cout << "---------------------------------------\n";
        cout << "Similarity matrix:\n";
        cout << similarityMatrix.toString();
        cout << "Computing degree matrix...\n";
    }


    //compute degree matrix
    Matrix degMatrix = createDegreeMatrix(similarityMatrix);

    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Degree matrix:\n";
        cout << degMatrix.toString();
        cout << "Computing square root degree matrix...\n";
    }

    Matrix sqrtDegMatrix = createSqrtDegreeMatrix(degMatrix);

    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Square root degree matrix:\n";
        cout << sqrtDegMatrix.toString();
        cout << "Computing multiplied matrix...\n";
    }



    Matrix multMatrix = sqrtDegMatrix * similarityMatrix * sqrtDegMatrix;

    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Multiplied matrix: (D^(-1/2)*S)*D^(-1/2)\n";
        cout << multMatrix.toString();
        cout << "Creating identity matrix...\n";
    }


    Matrix identityMatrix = createIdentityMatrix(dataPoints.size());

    if (DEBUGPRINT)
    {
        cout << "Creating laplacian matrix...\n";
    }

    Matrix laplacian = identityMatrix - multMatrix;

    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Laplacian: I-(D^(-1/2)*S)*D^(-1/2)\n";
        cout << laplacian.toString();
    }

    //conversion step
    double* dmatrix = convertMatrixToArray(laplacian);

    if (DEBUGPRINT)
    {
        cout << "Computing eigenvectors...\n";
    }


    Matrix resultMatrix = computeEigenpairs(dmatrix, dimension, dataPoints.size());


    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Eigenvectors\n";
        cout << resultMatrix.toString();
        cout << "---------------------------------------\n";
        cout << "Eigenvectors\n";
        cout << resultMatrix.toString();
        cout << "Normalizing matrix...\n";
    }



    delete normalizedMatrix;
    Matrix tmpNormalizedMatrix = computeNormalizedRowSumMatrix(resultMatrix);
    normalizedMatrix = new Matrix(tmpNormalizedMatrix.getHeight(), tmpNormalizedMatrix.getWidth());

    *normalizedMatrix = tmpNormalizedMatrix;
    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Normalized matrix\n";
        cout << normalizedMatrix->toString();
    }



    centroids->clear();
    int* c = kMeans->k_means(tmpNormalizedMatrix, k, 1e-4, dataPoints[0]->getMapIndex(),  0, centroids, distance_metric, minklamda);

    ASSIGNEDCLUSTERS = c;

    if (DEBUGPRINT)
    {
        cout << "---------------------------------------\n";
        cout << "Clustering output\n";
        cout << "Data Point:\t\t\tCluster:\n";
        for (int i = 0; i < dataPoints.size(); i++)
        {
            string output = dataPoints[i]->toString();
            if (output.length() < 6)
                cout << "(" << output << ")\t\t\t\t" << c[i] << "\n";
            else
                cout << "(" << output << ")\t\t\t" << c[i] << "\n";
        }
    }

}

Matrix ClusteringController::getEigenMatrix()
{
    return *normalizedMatrix;
}

vector<DataPoint> ClusteringController::getCentroids()
{
    return *centroids;
}

/*
 * creates a similarity matrix by using the following similarity function:
 * s(i,j) = e^-((x1-x2....
 */
Matrix ClusteringController::createSimilarityMatrix(vector<DataPoint*> &dataPoints, double sigma)
{
    Matrix matrix = Matrix(dataPoints.size(), dataPoints.size());

    for (int i = 0; i < matrix.getHeight(); i++)
    {
        for (int j = 0; j < matrix.getWidth(); j++)
        {

            double result = 1;
            if (i != j)
            {
                double eucledianDist = calculateEucledianDist(*dataPoints.at(i), *dataPoints.at(j));

                double exp = -(pow(eucledianDist, 2) / 2 * pow(sigma, 2));

                result = pow(EULERNUMBER, exp);
            }

            matrix.setValue(i, j, result);
        }
    }

    return matrix;
}

/*
 * Computes the eucledian distance between datapoint1 and dataPoint2
 */
double ClusteringController::calculateEucledianDist(DataPoint dataPoint1,DataPoint dataPoint2)
{
    double result;

    double sum = 0;

    for (int i = 0; i < dataPoint1.getDimension(); i++)
    {
        sum = sum + pow(dataPoint1.getAttribute(i) - dataPoint2.getAttribute(i), 2);
    }

    result = sqrt(sum);

    return result;
}

/*
 * Creates the degree matrix by taking the similarity matrix as input and computing
 * its row sums, listing them on the diagonal:
 * 	3	0	0
 * 	0	4	0
 * 	0	0	1
 *
 */
Matrix ClusteringController::createDegreeMatrix(Matrix similarityMatrix)
{
    Matrix matrix = Matrix(similarityMatrix.getHeight(),
                           similarityMatrix.getWidth());

    for (int i = 0; i < similarityMatrix.getHeight(); i++)
    {
        for (int j = 0; j < similarityMatrix.getWidth(); j++)
        {
            if (i == j)
            {
                matrix.setValue(i, j, similarityMatrix.computeRowSum(i));
            }
            else
            {
                matrix.setValue(i, j, 0);
            }
        }
    }

    return matrix;
}

/*
 * Creates an identity matrix
 * 		1	0	0
 * 		0	1	0
 * 		0	0	1
 */
Matrix ClusteringController::createIdentityMatrix(int n)
{
    Matrix matrix = Matrix(n, n);

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == j)
            {
                matrix.setValue(i, j, 1);
            }
            else
            {
                matrix.setValue(i, j, 0);
            }
        }
    }

    return matrix;
}

/*
 * Creates the D^-1/2 matrix, that is
 * 		sqrt(x)		x			x
 * 		x			sqrt(x)		x
 * 		x			x			sqrt(x)
 */
Matrix ClusteringController::createSqrtDegreeMatrix(Matrix degreeMatrix)
{
    Matrix matrix = Matrix(degreeMatrix.getHeight(), degreeMatrix.getWidth());

    for (int i = 0; i < degreeMatrix.getHeight(); i++)
    {
        for (int j = 0; j < degreeMatrix.getHeight(); j++)
        {
            if (i == j)
            {
                double value = 1 / sqrt(degreeMatrix.getValue(i, j));
                matrix.setValue(i, j, value);
            }
            else
            {
                matrix.setValue(i, j, 0);
            }
        }
    }

    return matrix;
}

/*
 * Converts the matrix to an array representation for being able
 * to use the gsl (gnu scientific library) to compute the eigenvectors
 */
double* ClusteringController::convertMatrixToArray(Matrix matrix)
{
    //width and height should be equal
    double* result = new double[matrix.getHeight() * matrix.getWidth()];

    int pos = 0;
    for (int i = 0; i < matrix.getHeight(); i++)
    {
        for (int j = 0; j < matrix.getWidth(); j++)
        {
            result[pos] = matrix.getValue(i, j);
            pos++;
        }
    }

    return result;
}

/*
 * Do the eigenvector computation
 */
Matrix ClusteringController::computeEigenpairs(double* matrix, int k, int n)
{
    Matrix result = Matrix(n, k);

    gsl_matrix_view m = gsl_matrix_view_array(matrix, n, n);

    gsl_vector *eval = gsl_vector_alloc(n);
    gsl_matrix *evec = gsl_matrix_alloc(n, n);

    gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc(n);

    gsl_eigen_symmv(&m.matrix, eval, evec, w);

    gsl_eigen_symmv_free(w);

    gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_ASC);

    {
        int i;

        for (i = 0; i < k; i++)
        {
            double eval_i = gsl_vector_get(eval, i);
            gsl_vector_view evec_i = gsl_matrix_column(evec, i);

            //printf("eigenvalue = %g\n", eval_i);

            for (int j = 0; j < n; j++)
            {
                result.setValue(j, i, gsl_vector_get(&evec_i.vector, j));
            }
        }
    }

    gsl_vector_free(eval);
    gsl_matrix_free(evec);

    return result;
}

/*
 * computes the normalized row-sum matrix
 */
Matrix ClusteringController::computeNormalizedRowSumMatrix(Matrix matrix)
{
    Matrix result = Matrix(matrix.getHeight(), matrix.getWidth());

    for (int i = 0; i < matrix.getHeight(); i++)
    {
        double rowSum = 0;
        for (int j = 0; j < matrix.getWidth(); j++)
        {
            rowSum += pow(matrix.getValue(i, j), 2);
        }
        for (int j = 0; j < matrix.getWidth(); j++)
        {
            result.setValue(i, j, matrix.getValue(i, j) / sqrt(rowSum));
        }
    }
    return result;
}

//initializeControls ************************************
//initialize the menu
void ClusteringController::initializeControls()
{
}

//dataReceived ******************************************
//called by the system in a case of new data available
void ClusteringController::dataReceived()
{
}


/*
 * Takes the data currently present on the space and puts them on a vector<DataPoints*> to be
 * processed by the main clustering algorithm
 * //TODO vector<DataPoints*> should be exchanged with the Matrix object
 */

}
