#include <DataPoint.h>
#include <DimensionException.h>
#include <cmath>
#include <string>
#include <sstream>
#include <vector>
#include <exception>

namespace SpectralClustering {
	DataPoint::DataPoint()
	{
		attributes = new vector<double>;
	}

	DataPoint::DataPoint(const DataPoint& dp)
	{
		this->attributes = new vector<double>(*dp.attributes);
	}

	DataPoint::~DataPoint()
	{
		delete attributes;
	}
#if 0
	void DataPoint::addAttribute(double value)
	{
		attributes->push_back(value);
	}

	void DataPoint::addAttributes(vector<double> *attributes)
	{
		for (int i=0; i<(int)attributes->size(); i++)
			addAttribute((*attributes)[i]);
	}
#endif

    void DataPoint::addAttribute(string name,double value)
	{
	    mapIndex[name] = attributes->size();
		attributes->push_back(value);
	}



	double DataPoint::getAttribute(int index)
	{
		return (*attributes)[index];
	}

	double DataPoint::getAttribute(string name)
	{
        return (*attributes)[mapIndex[name]];
	}

	vector<double>* DataPoint::getAttributes(void)
	{
		return attributes;
	}

	double DataPoint::getVectorLength(void)
	{
		return sqrt(squared());
	}

	int DataPoint::getDimension(void)
	{
		return attributes->size();
	}

	map<string, int> DataPoint::getMapIndex()
	{
	    return mapIndex;
	}

	DataPoint DataPoint::operator+(const DataPoint& operand)
	{
		if (getDimension() != (int)operand.attributes->size())
			throw DimensionException();
		DataPoint sum = DataPoint();
        string attrname;
		map<string, int> mapInd = operand.mapIndex;

		map<string, int>::iterator it = mapIndex.begin();
		for (it = mapIndex.begin(); it != mapIndex.end(); ++it) {
		    attrname = it->first;
            if(mapInd.find(attrname) == mapInd.end())
                throw "Attribute name does not exist error!";
                sum.addAttribute(it->first, getAttribute(it->second)+(*operand.attributes)[it->second]);
		}

		return sum;
	}

	DataPoint& DataPoint::operator+=(const DataPoint& operand)
	{
		if (getDimension() != (int)operand.attributes->size())
			throw "Dimension error!";

        string attrname;
		map<string, int> mapInd = operand.mapIndex;

        map<string, int>::iterator it = mapIndex.begin();
		for (it = mapIndex.begin(); it != mapIndex.end(); ++it) {
		    attrname = it->first;
            if(mapInd.find(attrname) == mapInd.end())
                throw "Attribute name does not exist error!";
                (*attributes)[it->second] += (*operand.attributes)[it->second];
		}

		return *this;
	}


	DataPoint DataPoint::operator-(const DataPoint& operand)
	{
		if (getDimension() != (int)operand.attributes->size())
			throw "Dimension error!";
		DataPoint diff = DataPoint();

        string attrname;
		map<string, int> mapInd = operand.mapIndex;

        map<string, int>::iterator it = mapIndex.begin();
        for (it = mapIndex.begin(); it != mapIndex.end(); ++it) {
		    attrname = it->first;
            if(mapInd.find(attrname) == mapInd.end())
                throw "Attribute name does not exist error!";
			diff.addAttribute(it->first,getAttribute(it->second)-(*operand.attributes)[it->second]);
        }
		return diff;
	}

	DataPoint DataPoint::operator/(double divisor)
	{
		DataPoint div = DataPoint();
		//TODO
		//for (int i=0; i<getDimension(); i++)
		//	div.addAttribute(getAttribute(i)/divisor);
		return div;

	}

	double DataPoint::operator*(const DataPoint& operand)
	{
		double sum = 0.0;
		if (getDimension() != (int)operand.attributes->size())
			throw "Dimension error!";
		for (int i = 0; i < (int)attributes->size(); i++)
			sum += (*attributes)[i]*(*operand.attributes)[i];
		return sum;
	}

	double DataPoint::squared(void)
	{
		double sum = 0.0;
		for (int i = 0; i<(int)attributes->size(); i++)
			sum += (*attributes)[i]*(*attributes)[i];
		return sum;
	}

	string DataPoint::toString(void)
	{
		ostringstream os;
		for (int i=0; i<getDimension(); i++){
			if(i == getDimension()-1)
				os << (*attributes)[i] << "";
			else
				os << (*attributes)[i] << ",";
		}
		return os.str();

	}

}
