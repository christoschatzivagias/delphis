#ifndef DIMENSIONEXCEPTION_H
#define DIMENSIONEXCEPTION_H

namespace SpectralClustering{
	class DimensionException{
		public:
			DimensionException();
			virtual ~DimensionException();
	};
}

#endif
