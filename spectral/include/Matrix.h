#ifndef MATRIX_H_
#define MATRIX_H_

#include <vector>
#include <stdlib.h>
#include <string>
#include <cmath>
using namespace std;

namespace SpectralClustering {

class Matrix {

private:
	double** matrix;
	int rows; //n
	int cols; //m

	void initMatrix();
	double multiply(double* row, double* col);
	double* calculateColumn(Matrix matrix, int colIndex);

public:
	Matrix(int n, int m) :
		rows(n), cols(m) {
		initMatrix();
	}
	virtual ~Matrix();

	virtual double** getMatrixDataTable();

	virtual int getHeight();
	virtual int getWidth();

	virtual void setValue(int row, int column, double value);
	virtual double getValue(int row, int column);
	virtual double* getMatrixRow(int row);
	virtual double* getMatrixColumn(int column);
	virtual double computeRowSum(int rowIndex);

	virtual Matrix& operator=(const Matrix& operand);
	virtual Matrix operator-(const Matrix& operand);
	virtual Matrix operator*(const Matrix& operand);

	virtual string toString(void);
};

}

#endif /* MATRIX_H_ */
