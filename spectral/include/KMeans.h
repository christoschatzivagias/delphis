#ifndef KMEANS_H_
#define KMEANS_H_

#include <Matrix.h>
#include <DataPoint.h>

namespace SpectralClustering {

class KMeans {
public:
	KMeans();
	virtual ~KMeans();
	int* k_means(Matrix matrix, int k, double t, map<string, int> mapIndex, double** centroids, vector<DataPoint> *rcentroids, int distance_metric = Euclidean, int minklamda = 3);
};

}

#endif /* KMEANS_H_ */
