#ifndef UTILS_H_
#define UTILS_H_

#include <string>

using namespace std;

namespace SpectralClustering{

class Utils {
public:
	Utils();
	virtual ~Utils();

	double convertToDouble(string& s);
	int convertToInt(char* s);
};

}

#endif /* UTILS_H_ */
