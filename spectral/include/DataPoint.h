#ifndef DATAPOINT_H
#define DATAPOINT_H
#include <DataPoint.h>
#include <vector>
#include <string>
#include <map>

namespace SpectralClustering
{
	using namespace std;

	class DataPoint{
		private:
			vector<double> *attributes;
			map<string, int> mapIndex;

		public:
			DataPoint();
			DataPoint(const DataPoint& dp);
			virtual ~DataPoint();
#if 0
			virtual void addAttribute(double value);
			virtual void addAttributes(vector<double> *attributes);
#endif
			virtual void addAttribute(string name,double value);
            virtual map<string, int> getMapIndex();
			virtual double getAttribute(int index);
			virtual double getAttribute(string name);
			virtual vector<double> *getAttributes(void);
			virtual double getVectorLength(void);
			virtual int getDimension(void);
			virtual DataPoint operator+(const DataPoint& operand);
			virtual DataPoint operator-(const DataPoint& operand);
			virtual DataPoint operator/(double divisor);
			virtual double operator*(const DataPoint& operand);
			virtual DataPoint& operator+=(const DataPoint& operand);
			virtual double squared(void);
			virtual string toString(void);
	};
}

#endif
