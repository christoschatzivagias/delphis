#ifndef CLUSTERINGCONTROLLER_H_
#define CLUSTERINGCONTROLLER_H_

#include <DataPoint.h>
#include <Matrix.h>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include "Enums.c"

#define EULER_NUMBER 2.71828;

using namespace std;

namespace SpectralClustering{

class ClusteringController
{

public:
	ClusteringController();

	virtual ~ClusteringController();

	int *getClusterIndexes();
    vector<DataPoint> getCentroids();

	void debug(bool debug);

	void doClustering(vector<DataPoint*> &dataPoints, int k, int dimension, double sigma,int distance_metric = Euclidean, int minklamda = 3);

	Matrix createSimilarityMatrix(vector<DataPoint*> &dataPoints, double sigma);
	Matrix  createDegreeMatrix(Matrix similarityMatrix);
	Matrix createIdentityMatrix(int n);
	Matrix createSqrtDegreeMatrix(Matrix degreeMatrix);
	double* convertMatrixToArray(Matrix matrix);
	Matrix computeEigenpairs(double* matrix, int k, int n);
	Matrix computeNormalizedRowSumMatrix(Matrix matrix);

    Matrix getEigenMatrix();

	void setNumberOfClusters(int value);
	int getNumberOfClusters();
	void setSigma(double value);
	double getSigma();

	//initialize the menu
	void initializeControls();

	//called by the system while rendering the object. Put here only the code for visualisation.
	void render();

	//called by the system in a case of new data available
	void dataReceived();

private:
	vector<DataPoint*> readSceneryData();
	double calculateEucledianDist(DataPoint dataPoint1, DataPoint dataPoint2);
	double computeSum(Matrix similarityMatrix, int row);
    Matrix *normalizedMatrix;
	vector<DataPoint*> DATAPOINTS;
	int* ASSIGNEDCLUSTERS;
	bool DEBUGPRINT;
	double EULERNUMBER;
    vector<DataPoint> *centroids;
	int numberOfClusters;
	double sigma;
};

}

#endif /* CLUSTERINGCONTROLLER_H_ */
