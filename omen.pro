######################################################################
# Automatically generated by qmake (3.1) Sat Jul 28 20:27:53 2018
######################################################################

TEMPLATE = app
TARGET = omen
INCLUDEPATH += .
INCLUDEPATH += include/delphis/
INCLUDEPATH += include/omen/
INCLUDEPATH += ARFF/include/
INCLUDEPATH += excel/include/
INCLUDEPATH += spectral/include/
CONFIG += c++14
CONFIG -= app_bundle

QT += core
QT += widgets
QT += sql
QT += concurrent
QT += charts
# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += ARFF/include/arff_attr.h \
           ARFF/include/arff_data.h \
           ARFF/include/arff_instance.h \
           ARFF/include/arff_lexer.h \
           ARFF/include/arff_parser.h \
           ARFF/include/arff_scanner.h \
           ARFF/include/arff_token.h \
           ARFF/include/arff_utils.h \
           ARFF/include/arff_value.h \
           excel/include/BasicExcel.hpp \
           include/delphis/AnalogyMethod.hpp \
           include/delphis/AsciiFileManager.hpp \
           include/delphis/Attribute.hpp \
           include/delphis/BestParameters.hpp \
           include/delphis/CategoricalTransformer.hpp \
           include/delphis/CommonFunctionsContainer.hpp \
           include/delphis/ConfigExporter.hpp \
           include/delphis/CSVReader.h \
           include/delphis/DataBase.hpp \
           include/delphis/DistanceMetrics.hpp \
           include/delphis/Enums.h \
           include/delphis/Estimators.hpp \
           include/delphis/Exception.hpp \
           include/delphis/Jackknife.hpp \
           include/delphis/Project.hpp \
           include/delphis/ThreadManager.hpp \
           include/delphis/Timer.hpp \
           include/delphis/AnalogyOutlierDetector.hpp \
           include/omen/AboutWindow.hpp \
           include/omen/AttributesTab.hpp \
           include/omen/CalculationParametersTab.hpp \
           include/omen/CategoricalTransformWindow.hpp \
           include/omen/DataBaseModel.hpp \
           include/omen/DistanceMetricsModel.hpp \
           include/omen/ErrorEntryDialog.hpp \
           include/omen/EstimatorsModel.hpp \
           include/omen/MainMenuBar.hpp \
           include/omen/MainToolBar.hpp \
           include/omen/MainWindow.hpp \
           include/omen/MethodModel.hpp \
           include/omen/ProjectsTab.hpp \
           include/omen/SimpleAnalogySearchDialog.hpp \
           include/omen/TabArea.hpp \
           include/omen/ValueCompleter.hpp \
           include/omen/ProjectsTableView.hpp \
           include/omen/SpinBoxDelegate.hpp \
           include/omen/AnalogyOutlierDetectorDialog.hpp

SOURCES += ARFF/src/arff_attr.cpp \
           ARFF/src/arff_data.cpp \
           ARFF/src/arff_instance.cpp \
           ARFF/src/arff_lexer.cpp \
           ARFF/src/arff_parser.cpp \
           ARFF/src/arff_scanner.cpp \
           ARFF/src/arff_token.cpp \
           ARFF/src/arff_utils.cpp \
           ARFF/src/arff_value.cpp \
           excel/src/BasicExcel.cpp \
           src/delphis/AnalogyMethod.cpp \
           src/delphis/AsciiFileManager.cpp \
           src/delphis/Attribute.cpp \
           src/delphis/BestParameters.cpp \
           src/delphis/CategoricalTransformer.cpp \
           src/delphis/CommonFunctionsContainer.cpp \
           src/delphis/ConfigExporter.cpp \
           src/delphis/CSVReader.cpp \
           src/delphis/DataBase.cpp \
           src/delphis/DistanceMetrics.cpp \
           src/delphis/Estimators.cpp \
           src/delphis/Jackknife.cpp \
           src/delphis/Project.cpp \
           src/delphis/ThreadManager.cpp \
           src/delphis/AnalogyOutlierDetector.cpp \
           src/omen/AboutWindow.cpp \
           src/omen/AttributesTab.cpp \
           src/omen/CalculationParametersTab.cpp \
           src/omen/CategoricalTransformWindow.cpp \
           src/omen/ClusterAnalysisInfo.cpp \
           src/omen/DataBaseModel.cpp \
           src/omen/DistanceMetricsModel.cpp \
           src/omen/ErrorEntryDialog.cpp \
           src/omen/EstimatorsModel.cpp \
           src/omen/main.cpp \
           src/omen/MainMenuBar.cpp \
           src/omen/MainToolBar.cpp \
           src/omen/MainWindow.cpp \
           src/omen/MethodModel.cpp \
           src/omen/ProjectsTab.cpp \
           src/omen/SimpleAnalogySearchDialog.cpp \
           src/omen/TabArea.cpp \
           src/omen/ValueCompleter.cpp \
           src/omen/ProjectsTableView.cpp \
           src/omen/SpinBoxDelegate.cpp \
           src/omen/AnalogyOutlierDetectorDialog.cpp



RESOURCES += resources.qrc
