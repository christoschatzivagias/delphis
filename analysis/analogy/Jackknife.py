# -*- coding: utf-8 -*-
"""
Created on Sun Jul  8 12:16:41 2018

@author: christoschatzivagias
"""
import AnalogyMethod
import EvaluationMethod

class Jackknife:
    def __init__(self):
        self.trainMatrix = []
        self.estimVector = []
        self.realValues = []
        self.estimatedValues = []
        self.analogies = 0
        self.distanceMetric = ""
        self.estimator = "" 
        self.evaluationMethod = ""
        self.weights = []
        
    
    def setTrainMatrix(self, trainMatrix):
        self.trainMatrix = trainMatrix
        self.distances = []
        
        
    def setEstimVector(self, estimVector):
        self.estimVector = estimVector
        
    def setRealValue(self, realValue):
        self.realValue = realValue
        
    def setEstimator(self, estimator):
        self.estimator = estimator
        
    def setDistanceMetric(self, distanceMetric):
        self.distanceMetric = distanceMetric
        
    def setAnalogies(self, analogies):
        self.analogies = analogies
        
    def setWeights(self, weights):
        self.weights = weights
    
    def setEvaluationMethod(self, evaluationMethod):
        self.evaluationMethod = evaluationMethod
    
    def perform(self):
        analogy = AnalogyMethod.AnalogyMethod()
        analogy.setAnalogies(self.analogies)
        analogy.setDistanceMetric(self.distanceMetric)
        analogy.setEstimator(self.estimator)
        analogy.setWeights(self.weights)
        
        evalMethod = EvaluationMethod.EvaluationMethodFactory.getEvaluationMethodByName(self.evaluationMethod)

        for index in range(0, len(self.trainMatrix)):
            estimVec = self.estimVector[:]
            del estimVec[index:index+1]
            analogy.setEstimVector(estimVec)
            
            tmpMat = self.trainMatrix[:]
            del tmpMat[index:index+1]
            analogy.setTrainMatrix(tmpMat)
            analogy.setTestVector(self.trainMatrix[index])
            self.estimatedValues.append(analogy.perform())
            
        
        evalMethod.setData(self.estimatedValues, self.estimVector)
        self.estimatedValues = []
        return evalMethod.calculate();
