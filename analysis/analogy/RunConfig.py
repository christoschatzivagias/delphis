#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 11:53:27 2018

@author: christoschatzivagias
"""
import configparser
import MethodSearcher
import ConfigReader
import argparse
import os

def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-c', help='Set Configuration File')
    parser.add_argument('-cp', help='Set the population of the genetic entities')
    parser.add_argument('-cg', help='Set the generation number of the genetic evolution')
    parser.add_argument('-ce', help='Set the number of parallel genetic evolution')

    parser.add_argument('-a', help='Set The Search Algorithm-BruteForce,Genetic')

    args = parser.parse_args()
    
    if not args.c:
        quit()
        
    config = configparser.ConfigParser()
    fileBaseName = os.path.basename(args.c);
    fileDir = os.path.dirname(args.c)
    config.read(args.c)
    
    Jparams = ConfigReader.JParameters()
    Jparams.read(config)
    
    Jmat = ConfigReader.JMatrix()
    Jmat.read(config)
    bestParameters = []
    if args.a == "Genetic":
        als = MethodSearcher.AlgorithmicSearcher()
        als.setEvaluationType(Jparams.evaluationMethod)
        als.setAnalogyRange(Jparams.minAnalogies, Jparams.maxAnalogies)
        als.setDistanceMetriList(Jparams.distanceMetrics)
        als.setEstimatorList(Jparams.estimators)
        als.setTrainStdMatrix(Jmat.trainStdMatrix)
        als.setTrainMatrix(Jmat.trainMatrix)
        als.setOrigTrainMatrix(Jmat.origTrainMatrix)
        als.setAttributeNames(Jmat.attributeNames)
        als.setAttributeTypes(Jmat.attributeTypes)
        als.setProjectDescriptions(Jmat.projectDescriptions)
        als.setEstimVector(Jmat.estimVector)
        #als.setSubsetList(Jparams.subsets)
        als.setEstimAttrName(Jparams.estimAttrName)
        if args.cp is not None:
            als.setPopulation(int(args.cp))
        
        if args.cg is not None:
            als.setGenerations(int(args.cg))
			
        if args.ce is not None:
            als.setParallelEvolutions(int(args.ce))
		
        bestParameters = als.findBestMethod()
    elif args.a == "BruteForce" or args.a is None:
        ms = MethodSearcher.BruteForceSearcher()
        ms.setEvaluationType(Jparams.evaluationMethod)
        ms.setAnalogyRange(Jparams.minAnalogies, Jparams.maxAnalogies)
        ms.setDistanceMetriList(Jparams.distanceMetrics)
        ms.setEstimatorList(Jparams.estimators)
        ms.setTrainStdMatrix(Jmat.trainStdMatrix)
        ms.setTrainMatrix(Jmat.trainMatrix)
        ms.setOrigTrainMatrix(Jmat.origTrainMatrix)
        ms.setAttributeNames(Jmat.attributeNames)
        ms.setAttributeTypes(Jmat.attributeTypes)
        ms.setProjectDescriptions(Jmat.projectDescriptions)
        ms.setEstimVector(Jmat.estimVector)
        ms.setSubsetList(Jparams.subsets)
        ms.setEstimAttrName(Jparams.estimAttrName)
        print(ms.origTrainMatrix)
        bestParameters = ms.findBestMethod()
    
    for index in range(0,len(bestParameters)):
        bestParameters[index].export(fileDir + "/" + "AnalogyMethod" + str(index + 1) + "_" + fileBaseName)


if __name__ == "__main__":
    main()
# 
   

                
