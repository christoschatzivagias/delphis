#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 18:23:21 2018

@author: christoschatzivagias
"""
import Estimator
import DistanceMetric

class AnalogyMethod:
    def __init__(self):
        self.trainMatrix = []
        self.estimVector = []
        self.realValue = 0
        self.estimatedValue = 0
        self.analogies = 0
        self.distanceMetric = ""
        self.estimator = ""
        self.distances = []
        self.weights = []
        self.test = []
        
    def setTestVector(self, test):
        self.test = test
    
    def setTrainMatrix(self, trainMatrix):
        self.trainMatrix = trainMatrix
        self.distances = []
        
        
    def setEstimVector(self, estimVector):
        self.estimVector = estimVector
        
    def setRealValue(self, realValue):
        self.realValue = realValue
        
    def setEstimator(self, estimator):
        self.estimator = estimator
        
    def setDistanceMetric(self, distanceMetric):
        self.distanceMetric = distanceMetric
        
    def setAnalogies(self, analogies):
        self.analogies = analogies
        
    def setWeights(self, weights):
        self.weights = weights
        
    def getEstimatedValue(self):
        return self.estimatedValue
    
    def __calculateDistances(self):
        if len(self.trainMatrix) == 0:
            return;
        dstMetric = DistanceMetric.DistanceMetricFactory.getEstimatorByName(self.distanceMetric)
        for mat_index in range(0, len(self.trainMatrix)):

            dstMetric.setData(self.test, self.trainMatrix[mat_index],self.weights)
            self.distances.append([mat_index, dstMetric.calculate()])
        
        
    def perform(self):

        self.__calculateDistances();
#        print("Distances:" + str(self.distances))
        self.distances.sort(key=lambda tup: tup[1])

        analogy_distances = self.distances[0:self.analogies ]
        nearest_ids = [tup[0] for tup in analogy_distances]
#        print("analogy_distances:" + str(analogy_distances))

        estimVector = []
        trainMatrix = []

        for i in nearest_ids:
            estimVector.append(self.estimVector[i])
            trainMatrix.append(self.trainMatrix[i])
        
 #       print("estimVector:" + str(estimVector))

        estim = Estimator.EstimatorFactory.getEstimatorByName(self.estimator)
        
        estim.setData([],estimVector)
        estimation = estim.estimate();
#        print("estimation:" + str(estimation))
        return estimation

        
        
    
    
        