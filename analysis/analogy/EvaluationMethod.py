# -*- coding: utf-8 -*-
"""
Created on Sun Jul  8 02:18:41 2018

@author: christoschatzivagias
"""
from abc import ABC, abstractmethod
import numpy
class EvaluationMethod(ABC):
    def __init__(self):
        super().__init__()
        self.estimValues = []
        self.origValues = []
        self.value = []

    def setData(self,estimValues, origValues):
        self.estimValues = estimValues
        self.origValues = origValues
    
    def validate(self):
        if len(self.estimValues) != len(self.origValues):
            return False;
        return True;
    
   
    
    @abstractmethod
    def calculate(self):
        pass

    
    
class MMRE(EvaluationMethod):
    def __init__(self):
        super().__init__()
        
        
    def calculate(self):
        if not self.validate():
           return -1;
        
        MREMat = []
        for index in range(0, len(self.origValues)):
            MREMat.append(abs(self.origValues[index] - self.estimValues[index])/(1 + abs(self.origValues[index])))
        
#        print("estimValues:" + str(self.estimValues))
#        print("origValues:" + str(self.origValues))
#        print("MRES:" + str(MREMat))

        return numpy.mean(MREMat)
    
    
class MdMRE(EvaluationMethod):
    def __init__(self):
        super().__init__()
        
        
    def calculate(self):
        if not self.validate():
           return -1;
       
        MREMat = []
        for index in range(0, len(self.origValues)):
            MREMat.append(abs(self.estimValues[index] - self.origValues[index])/(1 + abs(self.origValues[index])))
        
        
        return numpy.median(MREMat)
    


class Pred25(EvaluationMethod):
    def __init__(self):
        super().__init__()
        
        
    def calculate(self):
        if not self.validate():
           return -1;
       
        pred = 0;
        for index in range(0, len(self.origValues)):
            if abs(self.estimValues[index] - self.origValues[index])/self.origValues[index] < 25:
                pred = pred + 1
        
        
        return 100 * pred/float(len(self.origValues))
    
    
    
class EvaluationMethodFactory:
    def __init__(self):
        pass
       
    
    evalutationMethods = {
                'MMRE' : MMRE(), 
                'MdMRE' : MdMRE(), 
                'PRED25' : Pred25()
                }
    
    @staticmethod
    def getEvaluationMethodByName(name):
        return EvaluationMethodFactory.evalutationMethods[name]
    
