#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 12:15:26 2018

@author: christoschatzivagias
"""
from abc import ABC, abstractmethod
import numpy as np


class DistanceMetric(ABC):
    def __init__(self):
        super().__init__()
        self.vec1 = []
        self.vec2 = []
        self.weights = []
        
        
    def setData(self, vec1, vec2, weights):
        self.vec1 = vec1
        self.vec2 = vec2
        self.weights = weights
    
    
    def validateData(self):
        if len(self.vec1) == 0 or len(self.vec2) == 0 or len(self.weights) == 0:
            return False
        if len(self.vec1) != len(self.vec2) or len(self.vec1) != len(self.weights) or len(self.vec2) != len(self.weights) :
            return False
        return True
        
    @abstractmethod
    def calculate(self):
        pass
    
    
class Euclidean(DistanceMetric):
     def __init__(self):
        super().__init__()
        
     def calculate(self):
        if not self.validateData():
            return -1;
        ss = 0;
        for it in range(0, len(self.vec1)):
            v1 = self.vec1[it]
            v2 = self.vec2[it]

            if v1 < 0 and v2 < 0:
                if v1 != v2:
                    ss += self.weights[it]
            else:
                ss += self.weights[it] * np.power(v2 - v1,2)

        return np.sqrt(ss);
    
        
class Manhattan(DistanceMetric):
     def __init__(self):
        super().__init__()

     def calculate(self):
        if not self.validateData():
            return -1;
       
        ss = 0;
        for it in range(0, len(self.vec1)):
            v1 = self.vec1[it]
            v2 = self.vec2[it]

            if v1 < 0 and v2 < 0:
                if v1 != v2:
                    ss += self.weights[it]
            else:
                ss += self.weights[it] * np.absolute(v2 - v1)
        
        return ss;
        
class Canberra(DistanceMetric):
     def __init__(self):
        super().__init__()
     
     def calculate(self):
        if not self.validateData():
            return -1;
        
        ss = 0;
        for it in range(0, len(self.vec1)):
            v1 = self.vec1[it]
            v2 = self.vec2[it]

            if v1 < 0 and v2 < 0:
                if v1 != v2:
                    ss += self.weights[it]
            else:
                ss += self.weights[it] * np.absolute(v2 - v1)/ (np.absolute(v2) + np.absolute(v1))
        
        return ss;
    
class Chebychev(DistanceMetric):
     def __init__(self):
        super().__init__()
        
     def calculate(self):
        if not self.validateData():
            return -1;
        
        ss = 0;
        for it in range(0, len(self.vec1)):
            v1 = self.vec1[it]
            v2 = self.vec2[it]

            if v1 < 0 and v2 < 0:
                if v1 != v2:
                    ss += self.weights[it]
            else:
                temp = self.weights[it] * np.absolute(v2 - v1)
                if temp > ss:
                    ss = temp
        
        return ss;
        
class Czekanowski(DistanceMetric):
     def __init__(self):
        super().__init__()
    
     def calculate(self):
        if not self.validateData():
            return -1;
        
        ss1 = 0
        ss2 = 0
        for it in range(0, len(self.vec1)):
            v1 = self.vec1[it]
            v2 = self.vec2[it]

            if v1 < 0 and v2 < 0:
                if v1 != v2:
                    v1 = 1
                    v2 = 0
                else:
                    v1 = 1
                    v2 = 1
            if v1 > v2:
                ss1 += self.weights[it] * v2;
            else:
                ss1 += self.weights[it] * v1;
            ss2 += (v2 + v1);
        
        return 1 - ((2*ss1)/ss2);
        
class Minkowski(DistanceMetric):
     def __init__(self):
        self.mlambda = 3
        super().__init__()

     def setLambda(self, mlambda):
        self.mlambda = mlambda

     def calculate(self):
        if not self.validateData():
            return -1;
        
        ss = 0;
        for it in range(0, len(self.vec1)):
            v1 = self.vec1[it]
            v2 = self.vec2[it]

            if v1 < 0 and v2 < 0:
                if v1 != v2:
                    ss += self.weights[it]
            else:
                ss += self.weights[it] * np.power(v2 - v1, self.mlambda)
        
        return np.power(ss, 1/float(self.mlambda))



class DistanceMetricFactory:
    def __init__(self):
        pass
    
    distanceMetrics = {
               'Euclidean' : Euclidean(), 
               'Manhattan' : Manhattan(), 
               'Canberra' : Canberra(),
               'Chebychev' : Chebychev(),
               'Czekanowski' : Czekanowski(), 
               'Minkowski' : Minkowski()

               }
    @staticmethod
    def getEstimatorByName(name):
        return DistanceMetricFactory.distanceMetrics[name]
