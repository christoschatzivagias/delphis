#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 15 14:44:55 2018

@author: christoschatzivagias
"""
import numpy
from sklearn.preprocessing import MinMaxScaler


def convertToNumber (s):
    return int.from_bytes(s.encode(), 'little')

def convertToFloat(value):
    ret = 0
    try:
        ret = float(value)
    except:
        ret = convertToNumber(value)
    return ret


class JParameters:
	
    def __init__(self):
        self.distanceMetrics = []
        self.estimators = []
        self.evaluationMethod = ""
        self.minAnalogies = 0
        self.maxAnalogies = 0
        self.attributeNames = []

    
    
    def out(self):
        print("Distance Metrics:" + str(self.distanceMetrics))
        print("Estimators:" + str(self.estimators))
        print("Evaluation Method:" + self.evaluationMethod)
        print("Min Analogies:" + str(self.minAnalogies))
        print("Max Analogies:" + str(self.maxAnalogies))
        print("Estimation Attribute:" + self.estimAttrName)
        print("AtrributeNames:" + str(self.attributeNames))       

        print("Subsets:" + str(self.subsets))

    def read(self,config):
        self.distanceMetrics = config['Parameters']['DistanceMetrics'].replace(" ", "").split(",")
        self.estimators = config['Parameters']['Estimators'].replace(" ", "").split(",")
        self.evaluationMethod = config['Parameters']['EvaluationMethod']
        self.minAnalogies = int(config['Parameters']['MinAnalogies'])
        self.maxAnalogies = int(config['Parameters']['MaxAnalogies'])
        self.estimAttrName = config['Parameters']['EstimAttrName']
        self.subsets = None
        if 'Subsets' in config:
                self.subsets = config['Subsets']['Raw'].split("\\n")
                self.subsets = self.subsets[:len(self.subsets) - 1]
                for index in range(0, len(self.subsets)):
                        self.subsets[index] = [int(self.subsets[index][i:i+1], 2) for i in range(0, len(self.subsets[index]), 1)]


class JMatrix:
    def __init__(self):
        self.trainMatrix = []
        self.origTrainMatrix = []
        self.trainStdMatrix = []

        self.estimVector = []
        self.attributeTypes = []    
        self.projectDescriptions = []
        self.attributeNames = []

    def read(self, config):
        projectNum = config['Projects']['ProjectsNum']
        self.attributeTypes = list(map(int,config['Projects']['AttributeTypes'].replace(" ", "").split(",")))
        self.projectDescriptions = config['Projects']['ProjectDescriptions'].replace(" ", "").replace("'","").split(",")
        self.attributeNames = config['Projects']['AttributeNames'].replace("'","").split(",")

        for prjCounter in range(0, int(projectNum)):
            results = config['Projects']['Project' + str(prjCounter)].replace(" ", "").split(',')
            self.origTrainMatrix.append(results)
            results = list(map(convertToFloat, results))
            
            self.trainMatrix.append(results)
        
        
        self.trainMatrix = numpy.array(self.trainMatrix);

        scaler = MinMaxScaler()
        scaler.fit(self.trainMatrix)

        self.trainStdMatrix  = scaler.transform(self.trainMatrix)
        for i in range(0, len(self.attributeTypes)):
            if self.attributeTypes[i] == 2:
                self.trainStdMatrix[:,i] *= -1
                self.trainStdMatrix[:,i] += -1

                                
        self.trainStdMatrix  = self.trainStdMatrix.tolist()
            
        self.estimVector = config['Parameters']['EstimAttrVector'].replace(" ", "").split(',')
        self.estimVector = list(map(float, self.estimVector))

               
    def out(self):
        print("Train Matrix:" + str(self.trainMatrix))
        print("Estim Vector:" + str(self.estimVector))
		
