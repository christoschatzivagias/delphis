#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 14 17:21:03 2018

@author: dio
"""

import configparser

class AnalogyParameters:
    def __init__(self):
        self.analogies = 0;
        self.distanceMetric = ""
        self.estimator = ""
        self.subset = []
        self.evaluationType = ""
        self.evaluationValue = 99999999
        self.trainMatrix = []
        self.origTrainMatrix = []
        self.estimationValues = []
        self.attributeNames = []
        self.attributeTypes = []
        self.projectDescriptions = []
        self.estimAttributeName = ""
        
    def setSubset(self, subset):
        self.subset = subset
        
    def setDistanceMetric(self, distanceMetric):
        self.distanceMetric = distanceMetric
        
    def setEstimator(self, estimator):
        self.estimator = estimator
        
    def setAnalogies(self, analogies):
        self.analogies = analogies
        
    def setEstimationValues(self, estimationValues):
        self.estimationValues = estimationValues
        
    def setAttributeNames(self, attributeNames):
        self.attributeNames = attributeNames
        
    def setEstimAttrName(self, estimAttributeName):
        self.estimAttributeName = estimAttributeName
    
    def setAttributeTypes(self, attributeTypes):
        self.attributeTypes = attributeTypes
        
    def setProjectDescriptions(self, projectDescriptions):
        self.projectDescriptions = projectDescriptions
        
    def setEvaluationType(self, evaluationType):
        self.evaluationType = evaluationType
        
    def setEvaluationValue(self, evaluationValue):
        self.evaluationValue = evaluationValue
        
    def setTrainMatrix(self, trainMatrix):
        self.trainMatrix = trainMatrix
    
    def setOrigTrainMatrix(self, origTrainMatrix):
        self.origTrainMatrix = origTrainMatrix
    
    def out(self):
        print("subset:" + str(self.subset))
        print("analogies:" + str(self.analogies))
        print("distance metric:" + str(self.distanceMetric))
        print("estimator:" + str(self.estimator))
        
    def export(self,filename):
        config = configparser.ConfigParser()
        cfgfile = open(filename,'w')

        config.add_section('General')
        config.set('General','Type','AnalogyMethodParameters')
        config.set('General','Version','1.0')
        
        config.add_section('AnalogyMethod')
        config.set('AnalogyMethod','Analogies',str(self.analogies))
        config.set('AnalogyMethod','DistanceMetric',str(self.distanceMetric))
        config.set('AnalogyMethod','EstimationAttribute',str(self.estimAttributeName))
        config.set('AnalogyMethod','EstimationValues',str(self.estimationValues).replace("[","").replace("]",""))
        config.set('AnalogyMethod','Estimator',self.estimator)
        config.set('AnalogyMethod','EvaluationMethod',self.evaluationType)
        config.set('AnalogyMethod','EvaluationValue',str(self.evaluationValue))
        config.add_section('Projects')
        config.set('Projects','AttributeNames',str(self.attributeNames).replace("[","").replace("]","").replace("'",""))
        config.set('Projects','AttributeTypes',str(self.attributeTypes).replace("[","").replace("]",""))
        config.set('Projects','ProjectDescriptions',str(self.projectDescriptions).replace("[","").replace("]","").replace("'",""))

        config.set('Projects','AttributeWeights',str(self.subset).replace("[","").replace("]",""))
        
        config.set('Projects','ProjectsNum', str(len(self.origTrainMatrix)));
        for i in range(len(self.origTrainMatrix)):
            config.set('Projects','Project' + str(i + 1), str(self.origTrainMatrix[i]).replace("[","").replace("]",""))

        config.write(cfgfile)
        cfgfile.close()


        
        
