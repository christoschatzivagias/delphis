#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 14 17:05:59 2018

@author: christoschatzivagias
"""

from abc import ABC, abstractmethod
import AnalogyParameters
import Jackknife
import tqdm
import pygmo

class MethodSearcher(ABC):
    def __init__(self):
        super().__init__();
        self.subsetList = []
        self.minAnalogies = 0
        self.maxAnalogies = 0
        self.analogy_len = 0

        self.evaluationType = ""
        self.trainMatrix = []
        self.origTrainMatrix = []
        self.trainStdMatrix = []

        self.estimVector = []
        self.distanceMetricList = []
        self.dstMetric_len = 0
        self.estimatorList = []
        self.estimator_len = 0
        self.attributeNames = []
        self.attributeTypes = []
        self.projectDescriptions = []

        self.jack = Jackknife.Jackknife()

    def setSubsetList(self, subsetList):
        self.subsetList = subsetList
        
    def setAnalogyRange(self, minAnalogies, maxAnalogies):
        self.minAnalogies = minAnalogies
        self.maxAnalogies = maxAnalogies
        self.analogy_len = self.maxAnalogies - self.minAnalogies


    def setDistanceMetriList(self, distanceMetricList):
        self.distanceMetricList = distanceMetricList
        self.dstMetric_len = len(self.distanceMetricList)

    def setEstimAttrName(self, estimAttrName):
        self.estimAttrName = estimAttrName
        
    def setEstimatorList(self, estimatorList):
        self.estimatorList = estimatorList
        self.estimator_len = len(self.estimatorList)

    
    def setEvaluationType(self, evaluationType):
        self.evaluationType = evaluationType
    
    def setTrainStdMatrix(self, trainStdMatrix):
        self.trainStdMatrix = trainStdMatrix
    
    def setTrainMatrix(self, trainMatrix):
        self.trainMatrix = trainMatrix
    
    def setOrigTrainMatrix(self, origTrainMatrix):
        self.origTrainMatrix = origTrainMatrix
    
    def setEstimVector(self, estimVector):
        self.estimVector = estimVector
    
    def setAttributeNames(self, attributeNames):
        self.attributeNames = attributeNames
        
    def setAttributeTypes(self, attributeTypes):
        self.attributeTypes = attributeTypes
        
    def setProjectDescriptions(self, prjDescriptions):
        self.projectDescriptions = prjDescriptions
    
    @abstractmethod
    def findBestMethod(self):
        pass
    
    
    
class BruteForceSearcher(MethodSearcher):
    def __init__(self):
        super().__init__();
        self.bestParameters = AnalogyParameters.AnalogyParameters()
       
    
    
    def findBestMethod(self):
        self.jack.setEvaluationMethod(self.evaluationType)
        self.jack.setTrainMatrix(self.trainStdMatrix)
        self.jack.setEstimVector(self.estimVector)
        self.bestParameters.setEvaluationType(self.evaluationType)
        self.bestParameters.setEstimAttrName(self.estimAttrName)
        self.bestParameters.setEstimationValues(self.estimVector)
        self.bestParameters.setTrainMatrix(self.trainMatrix)
        self.bestParameters.setOrigTrainMatrix(self.origTrainMatrix)
        self.bestParameters.setAttributeNames(self.attributeNames)
        self.bestParameters.setAttributeTypes(self.attributeTypes)
        self.bestParameters.setProjectDescriptions(self.projectDescriptions)

        bestEval = 999999999
        curEval = 0;
        for i in tqdm.tqdm(range(len(self.subsetList))):
            self.jack.setWeights(self.subsetList[i])
            for distanceMetricName in self.distanceMetricList:
                self.jack.setDistanceMetric(distanceMetricName)                
                for estimatorName in self.estimatorList:
                    self.jack.setEstimator(estimatorName)
                    for analogies in range(self.minAnalogies, self.maxAnalogies):
#                        print("=========================================")
#                        print("analogies:" + str(analogies))
#                        print("estimatorName:" + str(estimatorName))
#                        print("distanceMetricName:" + str(distanceMetricName))
#                        print("self.subsetList:" + str(self.subsetList[i]))

                        self.jack.setAnalogies(analogies)

                        curEval = self.jack.perform()
                        if curEval < bestEval:
                            bestEval = curEval
                            self.bestParameters.setDistanceMetric(distanceMetricName)
                            self.bestParameters.setEstimator(estimatorName)
                            self.bestParameters.setAnalogies(analogies)
                            self.bestParameters.setSubset(self.subsetList[i])
                            self.bestParameters.setEvaluationValue(bestEval)
        
        return  [self.bestParameters]
        
    

        
class AlgorithmicSearcher(MethodSearcher):
    def __init__(self):
        super().__init__();
        self.bestParameters = AnalogyParameters.AnalogyParameters()
        self.population = 10
        self.pevols = 2
        self.gens = 2

    
    def setPopulation(self,pop):
        self.population = pop
		
    def setGenerations(self,gen):
        self.gens = gen
		
    def setParallelEvolutions(self,pevols):
        self.pevols = pevols
        
    def findBestMethod(self):
        self.jack.setEvaluationMethod(self.evaluationType)
        self.jack.setTrainMatrix(self.trainStdMatrix)
        self.jack.setEstimVector(self.estimVector)
        self.bestParameters.setEvaluationType(self.evaluationType)
        self.bestParameters.setEstimAttrName(self.estimAttrName)
        self.bestParameters.setEstimationValues(self.estimVector)
        self.bestParameters.setTrainMatrix(self.trainMatrix)
        self.bestParameters.setOrigTrainMatrix(self.origTrainMatrix)
        self.bestParameters.setAttributeNames(self.attributeNames)
        self.bestParameters.setAttributeTypes(self.attributeTypes)
        self.bestParameters.setProjectDescriptions(self.projectDescriptions)

        class JackknifeProblem(object):
            def __init__(self, dim, algoSearcher):
                 self.dim = dim
                 self.algoSearcher= algoSearcher
            
            @staticmethod
            def getParameterersByX(algoSearcher, x):
                analogy_luck = int(100000*x[0])
                dstMetric_luck = int(100000*x[1])
                estimator_luck = int(100000*x[2])
                weights = x[:len(x)-3]

                analogy_offset = analogy_luck % algoSearcher.analogy_len
                dstMetricId = dstMetric_luck % algoSearcher.dstMetric_len;
                estId = estimator_luck % algoSearcher.estimator_len;


                return [algoSearcher.minAnalogies + analogy_offset,
                        algoSearcher.distanceMetricList[dstMetricId],
                        algoSearcher.estimatorList[estId], weights]
                
                
            def fitness(self,x):
                params = JackknifeProblem.getParameterersByX(self.algoSearcher,x)
            
                self.algoSearcher.jack.setAnalogies(params[0])
                self.algoSearcher.jack.setDistanceMetric(params[1])
                self.algoSearcher.jack.setEstimator(params[2])
                self.algoSearcher.jack.setWeights(params[3])
                
                
                est = self.algoSearcher.jack.perform()

                return [est]
        
        
        
            def get_bounds(self):
                return ([0] * self.dim, [1] * self.dim)
        
            def get_name(self):
                return "JackknifeProblem"

            def get_extra_info(self):
                return "\tDimensions: " + str(self.dim)
        
        
        
        prob = pygmo.problem(JackknifeProblem(3 + len(self.attributeNames),self))
        
        algo = pygmo.algorithm(pygmo.sga(gen = self.gens))


        algo.set_verbosity(10)
        #pop = pygmo.population(prob, 10)
        
        archi = pygmo.archipelago(self.pevols, algo=algo, prob=prob, pop_size=self.population)

        #pop = algo.evolve(pop)

        archi.evolve(self.gens)
        archi.wait()
        #res = [isl.get_population().champion_f for isl in archi]
        best_params = []
        for isl in archi:
            bestparams = JackknifeProblem.getParameterersByX(self,isl.get_population().champion_x)
            params = AnalogyParameters.AnalogyParameters()
            params.setEvaluationType(self.evaluationType)
            params.setEstimAttrName(self.estimAttrName)
            params.setEstimationValues(self.estimVector)
            params.setTrainMatrix(self.trainMatrix)
            params.setOrigTrainMatrix(self.origTrainMatrix)
            params.setAttributeNames(self.attributeNames)
            params.setAttributeTypes(self.attributeTypes)
            params.setProjectDescriptions(self.projectDescriptions)
            params.setAnalogies(bestparams[0])
            params.setDistanceMetric(bestparams[1])
            params.setEstimator(bestparams[2])
            params.setSubset(bestparams[3].tolist())
            params.setEvaluationValue(isl.get_population().champion_f[0])
            best_params.append(params);
			
        return best_params;


        
       
    
        

        
