#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 11:06:03 2018

@author: christoschatzivagias

"""
import statistics

from abc import ABC, abstractmethod
from math import sqrt

def fibonacci_generator(n):
    f = []
    for i in range(0, n):
        f.append(((1+sqrt(5))**n-(1-sqrt(5))**n)/(2**n*sqrt(5)))
        
    return f
 
class Estimator(ABC):
 
    def __init__(self):
        self.estimVector = []
        self.trainMatrix = []
        super().__init__()
    
    def setData(self,trainMatrix, estimVector):
        self.trainMatrix = trainMatrix
        self.estimVector = estimVector
    
    @abstractmethod
    def estimate(self):
        pass
    
    
class Mean(Estimator):
        def __init__(self):
            super().__init__()
            
        def estimate(self):
            return statistics.mean(self.estimVector);
        
        
class Median(Estimator):
        def __init__(self):
            super().__init__()
            
        def estimate(self):
            return statistics.median(self.estimVector);
        
        

class FibonacciSerie(Estimator):
    def __init__(self):
        super().__init__()
            
    def estimate(self):
        fiboVec = fibonacci_generator(len(self.estimVector) + 2)
        fiboVec = fiboVec[2:]
        fibonacciSum = sum(fiboVec);
        ss = 0.;
        for counter in range(0, len(self.estimVector)):
            ss += fiboVec[counter] * self.estimVector[counter]
        
        return ss/fibonacciSum;
        
        
class EstimatorFactory:
    def __init__(self):
        pass
       
    
    estimators = {
                'Mean' : Mean(), 
                'Median' : Median(), 
                'FibonacciSerie' : FibonacciSerie()
                }
    
    @staticmethod
    def getEstimatorByName(name):
        return EstimatorFactory.estimators[name]
    
                