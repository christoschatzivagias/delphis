#include <QCoreApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <iostream>
#include <algorithm>
#include <functional>
#include <limits>
#include "ConfigExporter.hpp"
#include "DistanceMetrics.hpp"
#include "DataBase.hpp"
#include "Jackknife.hpp"
#include "AnalogyMethod.hpp"
#include "Estimators.hpp"
#include "BestParameters.hpp"



void runMethod(Configuration config, const QString &file)
{
    QVector<QStringList> prjsVecStr = config.getProjectsMatrix();
    QStringList attributeTypesStr = config.getAttributeTypes();
    QStringList attributeNames = config.getAttributeNames();
    QVector<QVector<double> > prjsVec;


    QVector<int> attributeTypes;
    numerifyIntVector(attributeTypesStr, attributeTypes);

    numerifyMatrix(prjsVecStr, attributeTypes.toList(), prjsVec);

    QStringList dstMetricsStrList = config.getSelectedDistanceMetrics();
    QStringList estimatorsStrList = config.getSelectedEstimators();
    int minAnalogies = config.getMinAnalogies();
    int maxAnalogies = config.getMaxAnalogies();
    QString estimationAttr = config.getEstimAttrName();
    QStringList projectDescs = config.getProjectDescriptions();
    DistanceMetricsContainer dstMetricContainer(dstMetricsStrList);
    const std::vector<const Distance *> &dstVec = dstMetricContainer.getCurrentDistances();
    EstimatorsContainer estContainer(estimatorsStrList);
    const std::vector<const Estimator *> &estVec = estContainer.getCurrentEstimators();

    QVector<QVector<QPair<QString, double> > > subsetWeights = config.getSubsetWeights();
    QVector<double> vals = config.getEstimAttrVals();
    QVector<double> sizeAdjustedVals = config.getSizeAdjustedAttrVals();
    QVector<double> stdSizeAdjustedVals;


    standardizeVector(sizeAdjustedVals, stdSizeAdjustedVals);



    QVector<QVector<double> > stdPrjVec;
    QVector<QPair<int, QVector<double> > > indexedMatrix,indexedStdMatrix;
    DataBase::standardizeMatrix(prjsVec, attributeTypes.toList(), stdPrjVec);
    DataBase::createIndexMatrix(stdPrjVec, indexedStdMatrix);
    DataBase::createIndexMatrix(prjsVec, indexedMatrix);

    EvaluationMethodFactory eval(config.getEvaluationMethod(), estimationAttr, projectDescs, prjsVecStr, vals, attributeNames, attributeTypesStr);

    size_t totalIterations = (maxAnalogies - minAnalogies + 1) * subsetWeights.size() * estVec.size() * dstVec.size();
    size_t progressIndex = 0;
    printf( "\n" );

    for(int analogies = minAnalogies; analogies <= maxAnalogies; analogies++) {
#ifdef TEST_PRINT
        printf("===================\n");

        printf("Analogies = %d\n",analogies);
#endif

        auto runDstMetrics = [&](const Distance *dstmetric) {
#ifdef TEST_PRINT

                printf("Distance %s\n",dstmetric->getName().c_str());
#endif
            auto runEstimators = [&](const Estimator *estimator) {
#ifdef TEST_PRINT

                printf("Estimator %s\n",estimator->getName().c_str());
#endif

                auto runSubsets = [&](const QVector<QPair<QString, double> > &weightsPair) {
#ifdef TEST_PRINT


                    qDebug() << "Subset:" << weightsPair;
                    qDebug() << "indexedMatrix:" << indexedStdMatrix;
#endif

                    progressIndex++;
                    AnalogyMethod analogy(weightsPair, dstmetric, estimator, analogies);

                    Jackknife jack(analogy, indexedMatrix, indexedStdMatrix, vals, weightsPair);
                    jack.perform();
                    //estimateForMatrix(analogy, indexedMatrix, indexedStdMatrix, vals, weightsPair, estimates);
                    eval.evaluate(jack.getEstimates(), vals, analogy);
                    printProgress("Total",(100.0f*progressIndex)/totalIterations,100);
                };

                std::for_each(subsetWeights.begin(), subsetWeights.end(), runSubsets);
            };

            std::for_each(estVec.begin(), estVec.end(), runEstimators);
        };

        std::for_each(dstVec.begin(), dstVec.end(), runDstMetrics);
    }
    printf( "\n" );

    eval.bestPar.exportFile("AnalogyMethod_" + QString(file).replace(".ini","") + ".ini");
}

int main(int argc, char *argv[])
{

    Configuration config;

    QCoreApplication anexapp(argc,argv);


    QCoreApplication::setApplicationName("ANEX");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;

    QCommandLineOption help( "h", "help");
    parser.addOption(help);

    QCommandLineOption configopt( "c", "import configuration file");
    parser.addOption(configopt);


    QCommandLineOption version( "v", "version");
    parser.addOption(version);

    parser.process(anexapp);


    if(parser.isSet(version)){
        std::cout << QCoreApplication::applicationName().toStdString() << " " << QCoreApplication::applicationVersion().toStdString() << std::endl;
        return 0;
    } else if(parser.isSet(help)){
        std::cout << "Options:\n";
        std::cout << "\t-v            Application Version\n";
        std::cout << "\t-c filename   Import Configuration File\n";

        return 0;
    } else if(parser.isSet(configopt)) {
        if(argc == 3) {
            try {
                config.importConfig(argv[2]);
            } catch(std::exception &e) {
                std::cout << e.what() << std::endl;
                return EXIT_FAILURE;
            }

            runMethod(config, argv[2]);
            return EXIT_SUCCESS;
        }
        std::cout << "Error filename argument missing" <<std::endl;

        return EXIT_FAILURE;
    }


    std::cout << "Error in file arguments try -h to see options" <<std::endl;


    return EXIT_FAILURE;
}
