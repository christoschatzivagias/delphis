#include "DataBaseModel.hpp"
#include "Attribute.hpp"
#include "DataBase.hpp"

#include <QSqlField>
#include <QSqlRecord>

AttributesModel::AttributesModel(DataBase *database, QSqlDatabase db):QSqlRelationalTableModel(database, db)
{
    mp_database = database;
    setTableConfig();
}

void AttributesModel::reset()
{
    while (canFetchMore()) {
        fetchMore();
    }
    mp_database->exec("delete from Attributes;");
    this->clear();
    setTableConfig();
}

void AttributesModel::setTableConfig()
{
    if(!mp_database->getDb().tables().contains("Attributes")) {
        mp_database->exec("create table Attributes (OMEN_ID integer primary key autoincrement, Description varchar, Status integer, Type integer)");
    }
    setTable("Attributes");
    setRelation(TYPE_INDEX, QSqlRelation("Type", "ID", "Description"));
    setHeaderData(TYPE_INDEX, Qt::Horizontal,"Type");

}

QVariant AttributesModel::data(const QModelIndex &idx, int role) const
{
    QVariant value;
    if(role == Qt::BackgroundColorRole) {
        value = this->record(idx.row()).field(STATUS_INDEX).value().toString() == "0" ? QColor(Qt::gray) : QSqlRelationalTableModel::data(idx, role);
    } else if(role == Qt::CheckStateRole) {
        if(idx.column() == STATUS_INDEX) value = this->record(idx.row()).field(STATUS_INDEX).value().toInt() == 1 ? Qt::Checked : Qt::Unchecked;
    } else if(role == Qt::DisplayRole) {
        if(idx.column() == STATUS_INDEX) value = this->record(idx.row()).field(STATUS_INDEX).value().toString() == "1" ? QString("Active") : QString("Inactive");
    }

    return value.isNull() ? QSqlRelationalTableModel::data(idx, role) : value;
}

Qt::ItemFlags AttributesModel::flags(const QModelIndex &index) const
{
    if (index.column() == STATUS_INDEX) {
        return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsEditable;;
    } else if (index.column() == TYPE_INDEX) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable ;
    }
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
}

bool AttributesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == STATUS_INDEX && role == Qt::CheckStateRole) {
        QSqlField fld = record(index.row()).field(index.column());
		bool status = fld.value() == "0";
        DataBase::instance()->setAttributeStatus(this->index(index.row(),0).data().toUInt(), status ? 1 : 0);
        return true;
    }
    return QSqlRelationalTableModel::setData(index, value, role);
}

ProjectsModel::ProjectsModel(DataBase *database, AttributesModel *attrsModel, QSqlDatabase db):QSqlTableModel(database, db)
{
    mp_database = database;
    attributesModel = attrsModel;

    setTableConfig();
}

void ProjectsModel::reset()
{
    while (canFetchMore()) {
       fetchMore();
    }
    mp_database->getDb().transaction();
    mp_database->exec("delete from Projects;");
    mp_database->getDb().commit();
    mp_database->getDb().transaction();
    mp_database->exec("drop table Projects;");
    mp_database->getDb().commit();
    this->clear();
    ProjectsModel::setTableConfig();
}

void ProjectsModel::setTableConfig()
{
    if(!mp_database->getDb().tables().contains("Type")) {
        mp_database->exec("create table Type (ID integer primary key, Description varchar(32))");
        mp_database->exec("insert into Type values(1, 'Numeric');");
        mp_database->exec("insert into Type values(2, 'Categorical');");
    }
    std::string projectDbCreateSql = "create table Projects (OMEN_ID integer primary key autoincrement, Description varchar, Status integer";

    auto completeProjectDBSqlFunction = [&](const Attribute &attr) {
        projectDbCreateSql += ",'" + attr.getDescription() + "' varchar(32)";
    };

    std::for_each(mp_database->getDBAttributes().begin(), mp_database->getDBAttributes().end(), completeProjectDBSqlFunction);

    projectDbCreateSql += ")";
    if(!mp_database->getDb().tables().contains("Projects")) {
        mp_database->exec(projectDbCreateSql.c_str());
    }
    this->setTable("Projects");
    select();
}

Qt::ItemFlags ProjectsModel::flags(const QModelIndex &index) const
{
    if (index.column() == STATUS_INDEX)
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsEditable;

    return Qt::ItemIsSelectable | Qt::ItemIsEnabled ;
}

QVariant ProjectsModel::data(const QModelIndex &idx, int role) const
{
    if(role == Qt::BackgroundColorRole) {
        QString projectStatus = this->record(idx.row()).field(STATUS_INDEX).value().toString();
        QString attrStatus;
        if(idx.column() > columns.size() - 1)
            attrStatus = attributesModel->record(idx.column() - columns.size()).field(2).value().toString();

        return (attrStatus == "0") || (projectStatus == "0") ? QColor(Qt::gray) : QSqlTableModel::data(idx, role);
    } else if(role == Qt::CheckStateRole) {
        if(idx.column() == STATUS_INDEX)
            return this->record(idx.row()).field(idx.column()).value().toString() == "1" ? Qt::Checked : Qt::Unchecked;
    } else if(role == Qt::DisplayRole) {
        if(idx.column() == STATUS_INDEX)
            return this->record(idx.row()).field(STATUS_INDEX).value().toString() == "1" ? QString("Active") : QString("Inactive");
    }

    return QSqlTableModel::data(idx, role);
}

bool ProjectsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column() == STATUS_INDEX && role == Qt::CheckStateRole) {
        QSqlField fld = record(index.row()).field(index.column());
		bool status = fld.value().toString() == "0";
        DataBase::instance()->setProjectStatus(this->index(index.row(), 0).data().toUInt(), status ? 1 : 0);
        return true;
    }

    return  QSqlTableModel::setData(index, value, role);
}

TargetsModel::TargetsModel(DataBase *database, AttributesModel *attrsModel, QSqlDatabase db):ProjectsModel(database, attrsModel, db)
{
    mp_database = database;
    TargetsModel::setTableConfig();
    connect(this,  &QAbstractItemModel::dataChanged, [=](const QModelIndex &topLeft, const QModelIndex &bottomRight){
        QString value = this->data(topLeft, Qt::DisplayRole).toString();

        QTimer::singleShot(0, Qt::PreciseTimer, [=](){
            if(topLeft.column() > 2) {
                QString attrname = mp_database->getAttributeDescriptionById(topLeft.column() - 2);
                QString omen_id = this->index(topLeft.row(),0).data().toString();
                QString query = QString("UPDATE projects SET '") + attrname + "'='" + value + "' WHERE OMEN_ID=" + omen_id + ";";
                mp_database->exec(query.toStdString());
            }
        });
    });
}

void TargetsModel::reset()
{
    while (canFetchMore()) {
       fetchMore();
    }
    mp_database->getDb().transaction();
    mp_database->exec("delete from Targets;");
    mp_database->getDb().commit();
    mp_database->getDb().transaction();
    mp_database->exec("drop table Targets;");
    mp_database->getDb().commit();
    this->clear();

    TargetsModel::setTableConfig();
}

void TargetsModel::setTableConfig()
{
    std::string projectDbCreateSql = "create table Targets (OMEN_ID integer primary key autoincrement, Description varchar, Status integer";

    auto completeProjectDBSqlFunction = [&](const Attribute &attr) {
        projectDbCreateSql += ",'" + attr.getDescription() + "' varchar(32)";
    };

    std::for_each(mp_database->getDBAttributes().begin(),  mp_database->getDBAttributes().end(), completeProjectDBSqlFunction);

    projectDbCreateSql += ")";
    if(!mp_database->getDb().tables().contains("Targets")) {
        mp_database->exec(projectDbCreateSql.c_str());
    }

    this->setTable("Targets");
    select();
}

Qt::ItemFlags TargetsModel::flags(const QModelIndex &index) const
{
	if (index.column() == STATUS_INDEX)
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsEditable;

	if (index.column() > STATUS_INDEX)
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;

	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

bool TargetsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (index.column() == STATUS_INDEX && role == Qt::CheckStateRole) {
		QSqlField fld = record(index.row()).field(index.column());
		bool status = fld.value().toString() == "0";
        DataBase::instance()->setTargetStatus(this->index(index.row(), 0).data().toUInt(), status ? 1 : 0);
		return true;
	}

	return  QSqlTableModel::setData(index, value, role);
}

QVariant TargetsModel::data(const QModelIndex &idx, int role) const
{
    if(role == Qt::BackgroundColorRole) {
        QString projectStatus = this->record(idx.row()).field(STATUS_INDEX).value().toString();
        return (projectStatus == "0") ? QColor(Qt::gray) : QSqlTableModel::data(idx, role);
    }
    return ProjectsModel::data(idx, role);
}

AttributesSelectionModel::AttributesSelectionModel(DataBase *db, QObject *parent) : QStandardItemModel(parent)
{
    database = db;
    const QStringList &activeAttrs = database->getActiveAttributes();

    int index = 0;
    QStandardItem *nameitem = nullptr;
    QStandardItem *statusitem = nullptr;
    QStandardItem *weightItem = nullptr;

    auto insertDstItem = [&] (const QString &str) {
        nameitem = new QStandardItem(str);
        nameitem->setEditable(false);
        setItem(index,0, nameitem);
        statusitem = new QStandardItem(true);
        statusitem->setCheckable(true);
        statusitem->setEditable(false);

        statusitem->setCheckState(Qt::Checked);
        setItem(index, 1, statusitem);

        weightItem = new QStandardItem("1");
        setItem(index, 2, weightItem);

        index++;
    };
    std::for_each(activeAttrs.cbegin(), activeAttrs.cend(), insertDstItem);
}

QPair<QStringList, QList<int>> AttributesSelectionModel::getSelectedAttributes()
{
    QList<int> attrtypes;
    QStringList attrnames;
    attrnames.reserve(this->rowCount());
    attrtypes.reserve(this->rowCount());
    QString itemname;
    for(int index = 0; index < this->rowCount(); index++) {
        itemname = this->item(index,0)->data(Qt::DisplayRole).toString();
        if(this->item(index, 1)->data(Qt::CheckStateRole).toBool()) {
            attrnames.append(itemname);
            attrtypes.append(DataBase::instance()->getAttributeType(itemname.toStdString()));
        }
    }

    //database->validateCategoricalAttributes(attrnames);

    return QPair<QStringList, QList<int>>(attrnames, attrtypes);
}

TargetsModel *DataBaseModel::getTargetsModel() const
{
    return targetsModel;
}

ProjectsModel *DataBaseModel::getProjectsModel() const
{
    return projectsModel;
}

AttributesModel *DataBaseModel::getAttributesModel() const
{
    return attributesModel;
}

DataBaseModel *DataBaseModel::instancePtr = nullptr;

DataBaseModel::DataBaseModel(DataBase *database) : QObject(database)
{
    attributesModel = new AttributesModel(database, database->getDb());
    projectsModel = new ProjectsModel(database, attributesModel, database->getDb());
    targetsModel = new TargetsModel(database, attributesModel, database->getDb());
    methodModel = new MethodModel(database);
    connect(database, SIGNAL(resetAttributesModelSignal()), methodModel, SLOT(clearMethods()));

    connect(database, SIGNAL(resetAttributesModelSignal()), this, SLOT(resetAttributesModel()));
    connect(database, SIGNAL(resetProjectsModelSignal()), this, SLOT(resetProjectsModel()));
    connect(database, SIGNAL(resetTargetsModelSignal()), this, SLOT(resetTargetsModel()));
    connect(database, SIGNAL(updateTablesSignal()), this, SLOT(update()));
}

DataBaseModel *DataBaseModel::instance()
{
	if (!instancePtr)
		instancePtr = new DataBaseModel(DataBase::instance());

	return instancePtr;
}

void DataBaseModel::update()
{
    attributesModel->select();
    while(attributesModel->canFetchMore())
        attributesModel->fetchMore();
    projectsModel->select();
    while(projectsModel->canFetchMore())
        projectsModel->fetchMore();
    targetsModel->select();
    while(targetsModel->canFetchMore())
        targetsModel->fetchMore();
}

void DataBaseModel::resetAttributesModel()
{
    attributesModel->reset();
}

void DataBaseModel::resetProjectsModel()
{
    projectsModel->reset();
}

void DataBaseModel::resetTargetsModel()
{
    targetsModel->reset();
}

void DataBaseModel::addTarget()
{

}




MethodModel *DataBaseModel::getMethodModel() const
{
    return methodModel;
}
