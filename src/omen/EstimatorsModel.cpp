#include "EstimatorsModel.hpp"

EstimatorsModel::EstimatorsModel(QObject *parent) : QStandardItemModel(parent){
    int index = 0;
    QStandardItem *nameitem = nullptr;
    QStandardItem *statusitem = nullptr;
    auto insertDstItem = [&] (std::pair<std::string, bool> &dstpair) {
        nameitem = new QStandardItem(dstpair.first.c_str());
        nameitem->setEditable(false);
        setItem(index,0, nameitem);
        statusitem = new QStandardItem(true);
        statusitem->setCheckable(true);
        statusitem->setCheckState(dstpair.second ? Qt::Checked : Qt::Unchecked);
        setItem(index,1, statusitem);
        index++;
    };
    std::for_each(Estimators.begin(), Estimators.end(), insertDstItem);

}

bool EstimatorsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    std::pair<std::string, bool> p = Estimators[index.row()];
    p.second = value.toBool();
    Estimators[index.row()] = p;
    return QStandardItemModel::setData(index, value, role);
}

QStringList EstimatorsModel::getSelectedEstimators() const
{
    QStringList selectedEst;

    auto filterSelectedEstimator = [&](const std::pair<std::string, bool> &p) {
        if(p.second) {
            selectedEst.append(p.first.c_str());
        }
    };

    std::for_each(Estimators.begin(), Estimators.end(), filterSelectedEstimator);
    return selectedEst;
}
