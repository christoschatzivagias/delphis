#include "MainWindow.hpp"
#include "TabArea.hpp"
#include "DataBaseModel.hpp"
#include "SimpleAnalogySearchDialog.hpp"

#include <QComboBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QTimer>
#include <QMdiArea>
#include <QMdiSubWindow>

TabArea::TabArea(MainWindow *parent) : QTabWidget(parent)
{
	setIconSize(QSize(50, 50));
    mp_projectsTab = new ProjectsTab(this);
    mp_attributesTab = new AttributesTab(this);
    mp_methodSearchTab = new BestParametersDialog(this);
    mp_calculationsParametersTab = new CalculationParametersTab(this);
    mp_chartsBar = new ChartsTab(this);
    addTab(mp_projectsTab, QIcon(":/sources/delphis_icons/omen__project_large_filled.svg"), "Projects");
    addTab(mp_attributesTab,QIcon(":/sources/delphis_icons/omen_project_attr_large_filled.svg"),"Attributes");
    addTab(mp_methodSearchTab,QIcon(":/sources/delphis_icons/omen_best_estimation_large_filled.svg"),"Method Search Configuration");

    addTab(mp_calculationsParametersTab,QIcon(":/sources/delphis_icons/omen_estimation_large_filled.svg"),"Calculation Parameters");
    addTab(mp_chartsBar, QIcon(), "Charts");
}


ChartsTab::ChartsTab(TabArea *parent) : QFrame(parent)
{
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mp_attributeComboBoxX = new QComboBox(this);

    mp_attributeComboBoxY = new QComboBox(this);


    QPushButton *applyRendering = new QPushButton("Plot",this);

    QHBoxLayout *topLayout = new QHBoxLayout();
    mainLayout->addLayout(topLayout);

    topLayout->addWidget(new QLabel("Attribute X:"));
    topLayout->addWidget(mp_attributeComboBoxX);
    topLayout->setStretchFactor(mp_attributeComboBoxX, 20);

    topLayout->addWidget(new QLabel("Attribute Y:"));
    topLayout->addWidget(mp_attributeComboBoxY);
    topLayout->setStretchFactor(mp_attributeComboBoxY, 20);
    topLayout->addWidget(applyRendering);

    topLayout->addSpacerItem(new QSpacerItem(1,1, QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));

    mp_mdiArea = new QMdiArea(this);
    mainLayout->addWidget(mp_mdiArea);


    connect(DataBase::instance(), &DataBase::attributesInserted, [&]() {
        mp_attributeComboBoxX->setModelColumn(1);

        mp_attributeComboBoxX->setModel(DataBaseModel::instance()->getAttributesModel());

        mp_attributeComboBoxY->setModelColumn(1);
        mp_attributeComboBoxY->setModel(DataBaseModel::instance()->getAttributesModel());
    });

    connect(applyRendering, &QPushButton::clicked, [&](){
        QStringList attrValuesXStr = DataBase::instance()->getActiveAttributeVector(mp_attributeComboBoxX->currentText());
        QStringList attrValuesYStr = DataBase::instance()->getActiveAttributeVector(mp_attributeComboBoxY->currentText());
        QStringList activePrjs = DataBase::instance()->getActiveProjectsVector();

        QVector<double> attrValuesXDbl,attrValuesYDbl;



        try {
            attrValuesXDbl = getDoubleVectorFromStringList(attrValuesXStr);
            attrValuesYDbl = getDoubleVectorFromStringList(attrValuesYStr);
        } catch(std::exception &e) {
            handleException(e);
            return;
        }


        DataList list;
        list.reserve(list.count());
        qDebug() << mp_attributeComboBoxX->currentText() << ":" << attrValuesXDbl;
        qDebug() << mp_attributeComboBoxY->currentText() << ":" << attrValuesYDbl;

        for(int index = 0; index < attrValuesXDbl.count(); index++) {
            list.push_back(Data(QPointF(attrValuesXDbl[index],attrValuesYDbl[index]), activePrjs[index]));
        }


        OmenChartView *chartView = new OmenChartView(createScatterChart(list, mp_attributeComboBoxX->currentText(), mp_attributeComboBoxY->currentText()), mp_mdiArea);

        mp_mdiArea->addSubWindow(chartView);
        chartView->showMaximized();

        mp_mdiArea->tileSubWindows();

        connect(DataBase::instance(), &DataBase::resetAttributesModelSignal, [&](){
            mp_mdiArea->closeAllSubWindows();
        });

       // chartsFrame->resize(chartsFrame->maximumSize());
    });

}

OmenChart *ChartsTab::createScatterChart(const DataList &list, const QString &attrX, const QString &attrY) const
{
    // scatter chart
    OmenChart *chart = new OmenChart();
    chart->setTitle(attrX + "-" + attrY);
    QString name("Series ");
    OmenScatterSeries *series = new OmenScatterSeries(chart);
    std::for_each(list.begin(), list.end(), [&](const Data &d) {
        series->appendLabeledPoint(d.first, d.second);
    });

    chart->addSeries(series);

    chart->createDefaultAxes();
    chart->axisX()->setTitleText(attrX);
    chart->axisY()->setTitleText(attrY);

    return chart;
}


