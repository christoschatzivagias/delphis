#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "ErrorEntryDialog.hpp"

ErrorEntryDialog::ErrorEntryDialog(QWidget *parent, Qt::WindowFlags f):QWidget(parent,f)
{
	MainLayout = new QVBoxLayout(this);
	ErrorMessage = new QLabel(this);
	RightEntry = new QLineEdit(this);
	SubmitButton = new QPushButton("Submit Value To Cell",this);
	MainLayout->addWidget(ErrorMessage);
	MainLayout->addWidget(RightEntry);
	MainLayout->addWidget(SubmitButton);
	connect(SubmitButton,SIGNAL(clicked()),this,SLOT(SendEntryToDataBase()));
}

void ErrorEntryDialog::SendEntryToDataBase()
{
	emit  SubmitValue(getValidEntry());
	close();
}

void ErrorEntryDialog::setErrorMessage(QString errorMessage)
{
	ErrorMessage->setText(errorMessage);
}

QString ErrorEntryDialog::getValidEntry()
{
	return RightEntry->text();
}
