#include <QVBoxLayout>
#include <QTableView>
#include <QMenu>
#include <QSqlRelationalDelegate>
#include "AttributesTab.hpp"
#include "DataBaseModel.hpp"
#include "MainWindow.hpp"
#include "TabArea.hpp"

AttributesTab::AttributesTab(TabArea *parent):QFrame(parent)
{
    new QVBoxLayout(this);
    mp_attributesTable = new QTableView(this);
    mp_attributesTable->setModel(DataBaseModel::instance()->getAttributesModel());
    mp_attributesTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    mp_attributesTable->setSelectionMode(QAbstractItemView::MultiSelection);
    mp_attributesTable->setEditTriggers(QTableView::NoEditTriggers);
    mp_attributesTable->resizeColumnsToContents();
    mp_attributesTable->resizeRowsToContents();
    mp_attributesTable->setItemDelegate(new QSqlRelationalDelegate(mp_attributesTable));
    mp_attributesTable->setContextMenuPolicy(Qt::CustomContextMenu);


    connect(mp_attributesTable, &QTableView::customContextMenuRequested, [&](QPoint pos){
         QMenu *menu=new QMenu(this);
         menu->addAction(ActionsContainer::instance()->getActionByName("ActivateAttributes"));
         menu->addAction(ActionsContainer::instance()->getActionByName("DeactivateAttributes"));
         menu->popup(mp_attributesTable->viewport()->mapToGlobal(pos));
    });

    connect(MainWindow::instance(), &MainWindow::ActivateSelectedAttributesSignal, [&](){
        DataBase::instance()->activateAttributes(mp_attributesTable->selectionModel()->selectedIndexes(), true);
    });

    connect(MainWindow::instance(), &MainWindow::DeactivateSelectedAttributesSignal, [&](){
        DataBase::instance()->activateAttributes(mp_attributesTable->selectionModel()->selectedIndexes(), false);
    });

    layout()->addWidget(mp_attributesTable);
}

