#include <QTableView>
#include <QTreeView>
#include <QVBoxLayout>
#include <QDockWidget>
#include <QPushButton>
#include <QMenu>
#include <QDir>
#include <QFileDialog>
#include <QSplitter>
#include <QToolBar>

#include <iostream>

#include "DataBaseModel.hpp"
#include "CalculationParametersTab.hpp"
#include "DataBase.hpp"
#include "Estimators.hpp"
#include "MainWindow.hpp"
#include "AnalogyMethod.hpp"
#include "ProjectsTableView.hpp"
#include "TabArea.hpp"

QStandardItemModel CalculationParametersTab::m_resultsModel;

CalculationParametersTab::CalculationParametersTab(TabArea *parent): QTabWidget(parent)
{
    setLayout(new QVBoxLayout());
    mp_mainSplitter = new QSplitter(Qt::Horizontal,this);

    mp_mainFrame->setLayout(new QVBoxLayout);
    mp_methodFrame = new QFrame(mp_mainFrame);
    mp_methodFrameLayout = new QVBoxLayout(mp_methodFrame);
    mp_toolbar = new QToolBar(mp_mainFrame);
	mp_toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    mp_toolbar->addAction(ActionsContainer::instance()->getActionByName("ImportMethod"));
    mp_toolbar->addAction(ActionsContainer::instance()->getActionByName("ImportCSVTargets"));
    mp_toolbar->addAction(ActionsContainer::instance()->getActionByName("AddTarget"));
    mp_toolbar->addAction(ActionsContainer::instance()->getActionByName("OutlierDetector"));
    mp_toolbar->addAction(ActionsContainer::instance()->getActionByName("ExportResults"));
    mp_mainFrame->layout()->addWidget(mp_toolbar);

    mp_methodsTable = new MethodProjectsTreeView(mp_methodFrame);
    mp_methodOptionsLayout = new QHBoxLayout();

    mp_methodFrameLayout->addWidget(mp_methodsTable);
    mp_methodFrameLayout->addLayout(mp_methodOptionsLayout);
    mp_methodsTable->setModel(DataBaseModel::instance()->getMethodModel());
    mp_methodsTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    mp_methodsTable->setEditTriggers(QTableView::NoEditTriggers);
    mp_methodsTable->setSelectionMode(QAbstractItemView::ExtendedSelection);

    mp_currentMethodEstimationAttributeTable = new QTreeView(mp_methodFrame);
    mp_currentMethodEstimationAttributeTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    mp_currentMethodEstimationAttributeTable->setEditTriggers(QTableView::NoEditTriggers);
    mp_currentMethodEstimationAttributeTable->setSelectionMode(QAbstractItemView::ExtendedSelection);

    mp_currentMethodProjectsTable = new QTreeView(mp_methodFrame);
    mp_currentMethodProjectsTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    mp_currentMethodProjectsTable->setEditTriggers(QTableView::NoEditTriggers);
    mp_currentMethodProjectsTable->setSelectionMode(QAbstractItemView::ExtendedSelection);

    mp_methodsTable->setCurrentMethodProjectsTable(mp_currentMethodProjectsTable);
    mp_methodsTable->setCurrentMethodEstimationAttributeTable(mp_currentMethodEstimationAttributeTable);

    mp_methodOptionsLayout->addWidget(mp_currentMethodEstimationAttributeTable,35);
    mp_methodOptionsLayout->addWidget(mp_currentMethodProjectsTable,65);



    mp_targetResultsFrame = new QFrame(this);
    mp_targetResultsFrame->setLayout(new QVBoxLayout);
    mp_targetProjectsTable = new ProjectsTableView(mp_targetResultsFrame);
    mp_targetProjectsTable->setModel(DataBaseModel::instance()->getTargetsModel());

    mp_targetProjectsTable->setEditTriggers(QTableView::EditKeyPressed);


    connect(mp_targetProjectsTable, &QTableView::customContextMenuRequested, [&](QPoint pos){
         QMenu *menu = new QMenu(this);
         menu->addAction(ActionsContainer::instance()->getActionByName("ActivateTargets"));
         menu->addAction(ActionsContainer::instance()->getActionByName("DeactivateTargets"));
         menu->popup(mp_targetProjectsTable->viewport()->mapToGlobal(pos));
    });


    connect(MainWindow::instance(), &MainWindow::ActivateSelectedTargetsSignal, [&](){
        DataBase::instance()->activateTargets(mp_targetProjectsTable->selectionModel()->selectedIndexes(), true);
    });

    connect(MainWindow::instance(), &MainWindow::DeactivateSelectedTargetsSignal, [&](){
        DataBase::instance()->activateTargets(mp_targetProjectsTable->selectionModel()->selectedIndexes(), false);
    });

    mp_targetResultsFrame->layout()->addWidget(mp_targetProjectsTable);

    mp_resultsTable = new QTreeView(mp_targetResultsFrame);
    mp_resultsTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    mp_resultsTable->setEditTriggers(QTableView::EditKeyPressed);
    mp_resultsTable->setSelectionMode(QAbstractItemView::ExtendedSelection);
    mp_targetResultsFrame->layout()->addWidget(mp_resultsTable);
    mp_resultsTable->setModel(&m_resultsModel);




    mp_mainSplitter->addWidget(mp_methodFrame);
    mp_mainSplitter->addWidget(mp_targetResultsFrame);
    mp_mainFrame->layout()->addWidget(mp_mainSplitter);

    this->addTab(mp_mainFrame, "Main");
    this->setTabPosition(QTabWidget::South);

    connect(DataBase::instance(), &DataBase::resetAttributesModelSignal, [&](){
        m_resultsModel.clear();
    });


    connect(mp_methodsTable, &QTableView::clicked, [&](const QModelIndex &index){
        m_resultsModel.clear();
        m_resultsModel.setHorizontalHeaderLabels(m_resultsModelColums);

        MethodModel *md = dynamic_cast<MethodModel *>(DataBaseModel::instance()->getMethodModel());
        MethodItem *methodItem = dynamic_cast<MethodItem *>(md->itemFromIndex(index));

        if(methodItem && methodItem->column() == 3){
            AnalogyMethodParameters method = std::get<0>(MethodModel::methods[methodItem->getId()]);
            QStandardItemModel *ProjectsModel = std::get<1>(MethodModel::methods[methodItem->getId()]);

            QStringList attrs = method.getAttributeNames();
            QList<int> attrtypes = DataBase::instance()->getTypeOfAttributes(attrs);
            QVector<QPair<int,QStringList> > prjsAttrVals;
            try {
                DataBase::instance()->getActiveAttributesVector(attrs, prjsAttrVals, Project::AS_TARGET);
            } catch (std::exception &e) {
                handleException(e, true, "Target project has invalid or null values!");
                return;
            }

            QList<int> activePrjs = getActiveProjectsIndices(ProjectsModel);

            const Distance *dstmetric = DistanceMetricsContainer::instance()->getDistanceMetricByName(method.getDistanceMetric());
            const Estimator *estim = EstimatorsContainer::instance()->getEstimatorByName(method.getStatistic());

            QVector<QStringList> prjsVecStr = method.getProjectsMatrix(activePrjs);
            QVector<double> weights = method.getSubsetVectorByAttributesSequence();
            QVector<double> estimVec = method.getEstimationAttributeVector(activePrjs);

            QStringList attributeTypesStr = method.getAttributeTypes();

            std::for_each(prjsAttrVals.begin(), prjsAttrVals.end(), [&](const QPair<int, QStringList> &vec){
                QList<QStandardItem *> resultRow;
                resultRow.append(new QStandardItem(QString::number(vec.first)));


                QVector<QVector<double> > prjsVec;


                try {
                    QVector<int> attributeTypes = getNumerifyIntVector(attributeTypesStr);
                    numerifyMatrix(prjsVecStr, attributeTypes.toList(), prjsVec);

                    prjsVec.push_back(getNumerifyVector(vec.second, attributeTypes.toList()));
                } catch(std::exception &e) {
                    QString extra_message = QString("Target with OMEN_ID:") + QString::number(vec.first) + " has invalid or null values!";
                    handleException(e, true, extra_message.toStdString());
                    return;
                }


                QVector<QVector<double> > stdPrjVec;
                QVector<QPair<int, QVector<double> > > indexedMatrix,indexedStdMatrix;
                DataBase::standardizeMatrix(prjsVec, attrtypes, stdPrjVec);
                QVector<double> stdVec = stdPrjVec.last();
                prjsVec.pop_back();
                stdPrjVec.pop_back();

                DataBase::createIndexMatrix(stdPrjVec, indexedStdMatrix);
                DataBase::createIndexMatrix(prjsVec, indexedMatrix);

                AnalogyMethod jack(method.subset(), dstmetric, estim, method.getAnalogies());
                double val = jack.estimate(indexedMatrix, indexedStdMatrix, stdVec, weights, estimVec);
                resultRow.append(new QStandardItem(DataBase::instance()->getTargetDescriptionById(vec.first)));

                resultRow.append(new QStandardItem(QString::number(val)));

                m_resultsModel.appendRow(resultRow);

            });
        }
    });
}

QList<int> CalculationParametersTab::getActiveProjectsIndices(QStandardItemModel *ProjectsModel)
{
    QList<int> activePrjs;

    for(int index = 0; index < ProjectsModel->rowCount(); index++) {
        QStandardItem *currentStatusItem = ProjectsModel->item(index,1);
        if(currentStatusItem->checkState() == Qt::Checked) {
            activePrjs << index;
        }
    }

    return activePrjs;
}

void CalculationParametersTab::calculate()
{
    if(mp_methodsTable->selectionModel()->hasSelection()) {
        m_resultsModel.clear();
        m_resultsModel.setHorizontalHeaderLabels(m_resultsModelColums);

        MethodModel *md = dynamic_cast<MethodModel *>(DataBaseModel::instance()->getMethodModel());
        MethodItem *methodItem = dynamic_cast<MethodItem *>(md->itemFromIndex(*mp_methodsTable->selectionModel()->selectedRows().begin()));

        AnalogyMethodParameters method = std::get<0>(MethodModel::methods[methodItem->getId()]);
        QStandardItemModel *ProjectsModel = std::get<1>(MethodModel::methods[methodItem->getId()]);

        QStringList attrs = method.getAttributeNames();
        QList<int> attrtypes = DataBase::instance()->getTypeOfAttributes(attrs);
        QVector<QPair<int,QStringList> > prjsAttrVals;
        try {
            DataBase::instance()->getActiveAttributesVector(attrs, prjsAttrVals, Project::AS_TARGET);
        } catch (std::exception &e) {
            handleException(e, true, "Target project has invalid or null values!");
            return;
        }

        QList<int> activePrjs = getActiveProjectsIndices(ProjectsModel);

        const Distance *dstmetric = DistanceMetricsContainer::instance()->getDistanceMetricByName(method.getDistanceMetric());
        const Estimator *estim = EstimatorsContainer::instance()->getEstimatorByName(method.getStatistic());

        QVector<QStringList> prjsVecStr = method.getProjectsMatrix(activePrjs);
        QVector<double> weights = method.getSubsetVectorByAttributesSequence();
        QVector<double> estimVec = method.getEstimationAttributeVector(activePrjs);

        QStringList attributeTypesStr = method.getAttributeTypes();

        std::for_each(prjsAttrVals.begin(), prjsAttrVals.end(), [&](const QPair<int, QStringList> &vec){
            QList<QStandardItem *> resultRow;
            resultRow.append(new QStandardItem(QString::number(vec.first)));


            QVector<QVector<double> > prjsVec;


            try {
                QVector<int> attributeTypes = getNumerifyIntVector(attributeTypesStr);
                numerifyMatrix(prjsVecStr, attributeTypes.toList(), prjsVec);

                prjsVec.push_back(getNumerifyVector(vec.second, attributeTypes.toList()));
            } catch(std::exception &e) {
                QString extra_message = QString("Target with OMEN_ID:") + QString::number(vec.first) + " has invalid or null values!";
                handleException(e, true, extra_message.toStdString());
				return;
            }


            QVector<QVector<double> > stdPrjVec;
            QVector<QPair<int, QVector<double> > > indexedMatrix,indexedStdMatrix;
            DataBase::standardizeMatrix(prjsVec, attrtypes, stdPrjVec);
            QVector<double> stdVec = stdPrjVec.last();
            prjsVec.pop_back();
            stdPrjVec.pop_back();

            DataBase::createIndexMatrix(stdPrjVec, indexedStdMatrix);
            DataBase::createIndexMatrix(prjsVec, indexedMatrix);

            AnalogyMethod jack(method.subset(), dstmetric, estim);

            double val = jack.estimate(indexedMatrix, indexedStdMatrix, stdVec, weights, estimVec);
            resultRow.append(new QStandardItem(DataBase::instance()->getTargetDescriptionById(vec.first)));

            resultRow.append(new QStandardItem(QString::number(val)));

            m_resultsModel.appendRow(resultRow);
        });
        //Take Targets with the corresponding attribute names
    }
}

void CalculationParametersTab::addTarget()
{
    if(!DataBase::instance()->hasAttributes())
        return;

    int id = DataBase::instance()->getDBTargets().size() + 1;
    DataBase::instance()->addDBTarget(Project(id,
                                              "User Target-" + std::to_string(id),
                                              1,
                                              DataBase::instance()->getDBAttributes(), Project::AS_TARGET));
}

void CalculationParametersTab::exportResults()
{
    QFileDialog resultsExportDialog;
    resultsExportDialog.setWindowModality(Qt::ApplicationModal);
    resultsExportDialog.setAcceptMode (QFileDialog::AcceptSave);
    resultsExportDialog.setNameFilter("csv Files (*.csv)");
    resultsExportDialog.setDefaultSuffix("csv");
    connect(&resultsExportDialog, &QFileDialog::fileSelected, [&](const QString filePath){
        ResultExporter rexp;
        rexp.exportResultModel(&m_resultsModel, filePath);
    });

    resultsExportDialog.show();
    resultsExportDialog.exec();
}

MethodProjectsTreeView::MethodProjectsTreeView(QWidget *parent) : QTreeView(parent) {

}

void MethodProjectsTreeView::setCurrentMethodProjectsTable(QTreeView *value)
{
    CurrentMethodProjectsTable = value;
}

void MethodProjectsTreeView::setCurrentMethodEstimationAttributeTable(QTreeView *value)
{
    CurrentMethodEstimationAttributeTable = value;
}


void MethodProjectsTreeView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    MethodModel *md = dynamic_cast<MethodModel *>(this->model());

    if(md) {
        MethodItem *cur_stitem = dynamic_cast<MethodItem *>(md->itemFromIndex(current));

        if(cur_stitem) {
            std::tuple<QStandardItemModel *, QStandardItemModel *> models = md->getMethodModels(cur_stitem->getId());
            CurrentMethodProjectsTable->setModel(std::get<0>(models));
            CurrentMethodEstimationAttributeTable->setModel(std::get<1>(models));
        }
    }
}

ResultExporter::ResultExporter() : QObject(QCoreApplication::instance()) {

}

void ResultExporter::exportResultModel(const QStandardItemModel *md, const QString &filePath)
{
    QFile file(filePath);
    if (file.open(QFile::WriteOnly)) {
        QTextStream stream(&file);
        stream << "OMEN_ID" << "," << "Target_Description" << "," <<  "Estimation\n";
        for (int i = 0; i < md->rowCount(); i++){
            stream << md->item(i,0)->text() << "," <<  md->item(i, 1)->text() << "," <<  md->item(i, 2)->text() << "\n";
        }
        file.close();
    }

}
