#include "CategoricalTransformWindow.hpp"
#include <QVBoxLayout>
#include <QTableView>
#include <QSqlQuery>
#include <QVector>
#include <QPair>
#include <QDialogButtonBox>
#include <QStatusBar>
#include <QAbstractButton>




CategoricalTransformerWindow::CategoricalTransformerWindow(QWidget *parent) : QDialog(parent)
{
    QVector<QPair<QString, QSet<QString> > > attributesValues = categoricalTransformer->getAttributesValues();
    setWindowTitle("Numeric Conversion");
    QMap<QString, QVector<int> > nullValues;
    mainLayout = new QVBoxLayout(this);
    table = new QTableView(this);
    table->setModel(&model);
    mainLayout->addWidget(table);
    model.setColumnCount(3);
    model.setHeaderData(0, Qt::Horizontal, "Attribute", Qt::DisplayRole);
    model.setHeaderData(1, Qt::Horizontal, "Value", Qt::DisplayRole);
    model.setHeaderData(2, Qt::Horizontal, "Transformed Value", Qt::DisplayRole);

    auto fillCategoricalAttributesModel = [&] (const QPair<QString, QSet<QString> > &attrTuple) {
        QString attrname = attrTuple.first;
        int index = -1;
        auto fillCategoricalAttributesValues = [&] (const QString &attrValue) {
            int value = index--;
            while(attrTuple.second.find(QString::number(value)) != attrTuple.second.end()) {
                value = index--;
            }
            QList<QStandardItem *> items;
            items << new QStandardItem(attrname);
            items << new QStandardItem(attrValue);
            items << new QStandardItem(QString::number(value));
            model.appendRow(items);
        };

        std::for_each(attrTuple.second.begin(), attrTuple.second.end(), fillCategoricalAttributesValues);
    };

    std::for_each(attributesValues.begin(), attributesValues.end(), fillCategoricalAttributesModel);


    dialogButtonBox = new QDialogButtonBox(QDialogButtonBox::Apply, Qt::Horizontal, this);
    statusbar = new QStatusBar(this);
    mainLayout->addWidget(statusbar);
    mainLayout->addWidget(dialogButtonBox);

    connect(dialogButtonBox, &QDialogButtonBox::clicked, this, &CategoricalTransformerWindow::transformCategoricalValues);
}

void CategoricalTransformerWindow::transformCategoricalValues()
{
    for(int index = 0; index < model.rowCount(); index++) {
        QString attrname = model.item(index, 0)->data(Qt::DisplayRole).toString();
        QString initvalue = model.item(index, 1)->data(Qt::DisplayRole).toString();
        QString tranvalue = model.item(index, 2)->data(Qt::DisplayRole).toString();
        categoricalTransformer->setTransformedValue(attrname, initvalue, tranvalue);

//        statusbar->showMessage(status);
    }
}
