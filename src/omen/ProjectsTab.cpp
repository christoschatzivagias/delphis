#include <QDockWidget>
#include <QMessageBox>
#include <QTableView>
#include <QVBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QToolBar>

#include <QLineEdit>
#include <QStringList>
#include <QCompleter>
#include <set>
#include "TabArea.hpp"
#include "ProjectsTab.hpp"
#include "DataBaseModel.hpp"
#include "MainWindow.hpp"
#include "ProjectsTableView.hpp"
#include <iostream>


Completer::Completer(QStringList stringList, QObject *parent)
    : QCompleter(stringList,parent), cursorPos_(-1)
{

}

ExpressionLineEdit::ExpressionLineEdit(QWidget* parent)
    : QLineEdit(parent)
{
    stringList << "minRoute" << "minPitch" << "minSpacing";
    completer_ = new Completer(stringList, this);
    setCompleter(completer_);

    QObject::connect(this, SIGNAL(textChanged(const QString&)),
                     completer_, SLOT(onLineEditTextChanged()));

    QObject::connect(this, SIGNAL(cursorPositionChanged(int, int)),
                     completer_, SLOT(onLineEditTextChanged()));
}

QString Completer::pathFromIndex(const QModelIndex &index) const
{
    QString newStr = index.data(Qt::EditRole).toString();
    ExpressionLineEdit *lineEdit = qobject_cast<ExpressionLineEdit*>(parent());
    QString str = lineEdit->text();
    int prevSpacePos = str.mid(0, lineEdit->cursorPosition()).lastIndexOf(' ');

    int curPos = lineEdit->cursorPosition();
    int nextSpacePos = str.indexOf(' ', curPos);
    if (nextSpacePos == -1) {
        nextSpacePos = str.size();
    }

    QString part1 = str.mid(0, prevSpacePos + 1);
    QString pre = str.mid(prevSpacePos + 1, curPos - prevSpacePos - 1);
    QString post = str.mid(curPos, nextSpacePos - curPos);
    QString part2 = str.mid(nextSpacePos);

    cursorPos_ = curPos + newStr.size() - pre.size();
    return part1 + newStr + part2;
}

void Completer::onLineEditTextChanged()
{
    qDebug() << "Completer::onLineEditTextChanged()" << cursorPos_;
    if (cursorPos_ != -1) {
        ExpressionLineEdit *lineEdit = qobject_cast<ExpressionLineEdit*>(parent());
        lineEdit->setCursorPosition(cursorPos_);
        cursorPos_ = -1;
    }
}

QStringList Completer::splitPath(const QString &path) const
{
     cursorPos_ = -1;
     ExpressionLineEdit *lineEdit = qobject_cast<ExpressionLineEdit*>(parent());
     QString text = lineEdit->text();
     QStringList stringList;
     QString str;
     int index = text.mid(0,lineEdit->cursorPosition()).lastIndexOf(' ');
     str = text.mid(index, lineEdit->cursorPosition()-index);
     str.trimmed();
     str.replace(" ", "");
     stringList << str;
     return stringList;
}


ProjectsTab::ProjectsTab(TabArea *parent): QFrame(parent)
{
    new QVBoxLayout(this);
    toolbar = new QToolBar(this);
    this->layout()->addWidget(toolbar);
    toolbar->addAction(ActionsContainer::instance()->getActionByName("ValueCompleter"));
    ProjectTable = new ProjectsTableView(this);

    ProjectTable->setModel(DataBaseModel::instance()->getProjectsModel());


    connect(ProjectTable, &QTableView::customContextMenuRequested, [&](QPoint pos){
         QMenu *menu=new QMenu(this);
         menu->addAction(ActionsContainer::instance()->getActionByName("CopyProjectsToTargets"));
         menu->addSeparator();
         menu->addAction(ActionsContainer::instance()->getActionByName("ActivateProjects"));
         menu->addAction(ActionsContainer::instance()->getActionByName("DeactivateProjects"));
         menu->popup(ProjectTable->viewport()->mapToGlobal(pos));
    });

    connect(MainWindow::instance(), &MainWindow::ActivateSelectedProjectsSignal, [&](){
        DataBase::instance()->activateProjects(ProjectTable->selectionModel()->selectedIndexes(), true);
    });

    connect(MainWindow::instance(), &MainWindow::DeactivateSelectedProjectsSignal, [&](){
        DataBase::instance()->activateProjects(ProjectTable->selectionModel()->selectedIndexes(), false);
    });

    connect(MainWindow::instance(), &MainWindow::CopySelectedProjectsToTargetsSignal, [&](){
            DataBase::instance()->copyProjectsToTargets(ProjectTable->selectionModel()->selectedIndexes());
    });
//#ifdef Q_OS_LINUX
//    QFrame *fr = new QFrame(this);
//    new QHBoxLayout(fr);
//    QLabel *lb = new QLabel("Filter Query:",this);
//    QCompleter *cm = new QCompleter();
//    kle = new ExpressionLineEdit(this);
//    connect(DataBase::instance(), &DataBase::resetAttributesModelSignal, [&]() {
//        KCompletion *comp = kle->completionObject();
//        if(comp) {
//            comp->clear();
//        }
//    });
//    connect(DataBase::instance(), &DataBase::attributesInserted, [&](const QStringList &attrNames){
//        Completer *cm = new  Completer( attrNames, kle);
//                cm->setFilterMode(Qt::MatchContains);
//       kle->setCompleter(cm);
//    });
//    fr->layout()->addWidget(lb);
//    fr->layout()->addWidget(kle);
//    layout()->addWidget(fr);
//#endif
    layout()->addWidget(ProjectTable);
}

ProjectsTab::~ProjectsTab()
{


}
