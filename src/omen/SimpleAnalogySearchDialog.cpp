#include <QVBoxLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QComboBox>
#include <QStatusBar>
#include <QLabel>
#include <QHeaderView>
#include <QSpinBox>
#include <QToolBar>
#include <QAction>
#include <QTableView>
#include <QComboBox>
#include <QDir>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>
#include <QStackedWidget>
#include "SpinBoxDelegate.hpp"
#include "SimpleAnalogySearchDialog.hpp"
#include "ConfigExporter.hpp"
#include "AnalogyOutlierDetector.hpp"
#include "TabArea.hpp"


void BestParametersDialog::updateAnalogiesBox()
{
    int analogies = DataBase::instance()->getActiveProjectsCount()-1;
    AnalogiesStartBox->setRange(1,analogies);
    AnalogiesEndBox->setRange(1,analogies);
}

BestParametersDialog::BestParametersDialog(TabArea *parent): QFrame(parent)
{

    this->setWindowTitle("Method Search Configuration");
    this->setWindowIcon(QIcon(":/sources/estimation1.png"));
    int analogies = DataBase::instance()->getActiveProjectsCount()-1;

    toolBar = new QToolBar(this);

    PlayAction = new QAction(QIcon(":/sources/playAlg.png"),"Play",toolBar);
    methodType = new QComboBox(toolBar);
    methodType->addItem("Heuristic");
    methodType->addItem("Brute Force");
    connect(methodType, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [](int index){

    });

    configFilesCounterSpinbox = new QSpinBox(this);
    configFilesCounterSpinbox->setValue(1);
    configFilesCounterSpinbox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    configFilesCounterLabel = new QLabel("Number of files");
    configFilesCounterLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    prefixLineEdit = new QLineEdit(this);
    prefixLineEdit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    prefixLineEdit->setPlaceholderText("File Prefix");

    directoryComboBox = createComboBox(QDir::toNativeSeparators(QDir::currentPath()));

    browseButton = new QPushButton("Browse", this);
    browseButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);


    connect(browseButton, &QAbstractButton::clicked, this, &BestParametersDialog::browse);


    toolBar->addAction(PlayAction);
    toolBar->addWidget(methodType);
    toolBar->addWidget(configFilesCounterLabel);
    toolBar->addWidget(configFilesCounterSpinbox);
    toolBar->addWidget(prefixLineEdit);

    toolBar->addWidget(directoryComboBox);
    toolBar->addWidget(browseButton);


    mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(toolBar);
    centralLayout = new QHBoxLayout();
    mainLayout->addLayout(centralLayout);
    leftLayout = new QVBoxLayout();
    rightLayout = new QVBoxLayout();

    centralLayout->addLayout(leftLayout);
    centralLayout->addLayout(rightLayout);

    AttrToBeEstimatedGroup = new QGroupBox("Attribute To Be Estimated",this);
    AttrToBeEstimatedGroup->setLayout(new QVBoxLayout(AttrToBeEstimatedGroup));
    AttrToBeEstimatedComboBox = new QComboBox(this);

    connect(DataBase::instance(), &DataBase::attributesInserted, [&]() {
        AttrToBeEstimatedComboBox->setModel(DataBaseModel::instance()->getAttributesModel());
        AttrToBeEstimatedComboBox->setModelColumn(1);
    });


    AttrToBeEstimatedGroup->layout()->addWidget(AttrToBeEstimatedComboBox);
    leftLayout->addWidget(AttrToBeEstimatedGroup);


    EvaluationsMetricGroup = new QGroupBox("Evaluation Metrics",this);
    MmreRadioButton = new QRadioButton("MMRE",this);
    MmreRadioButton->setChecked(true);
    MdmreRadioButton = new QRadioButton("MdMRE",this);
    PredRadioButton = new QRadioButton("Pred25",this);
    PredValueSpinBox = new QSpinBox(this);
    PredValueSpinBox->setValue(25);
    PredValueSpinBox->setRange (1,99);
    PredValueSpinBox->setSuffix("%");
    PredValueSpinBox->setDisabled(true);
    PredValueSpinBox->setVisible(false);



    EvaluationsMetricGroup->setLayout(new QHBoxLayout(EvaluationsMetricGroup));
    EvaluationsMetricGroup->layout()->addWidget(MmreRadioButton);
    EvaluationsMetricGroup->layout()->addWidget(MdmreRadioButton);
    EvaluationsMetricGroup->layout()->addWidget(PredRadioButton);
    EvaluationsMetricGroup->layout()->addWidget(PredValueSpinBox);

    leftLayout->addWidget(EvaluationsMetricGroup);


    AnalogiesGroup = new QGroupBox("Analogies",this);
    AnalogiesStartBox = new QSpinBox(this);
    AnalogiesStartBox->setRange(1,analogies);
    AnalogiesStartBox->setValue(1);
    AnalogiesEndBox = new QSpinBox(this);
    AnalogiesEndBox->setRange(1,analogies);
    AnalogiesEndBox->setValue(10);
    AnalogiesStartLabel = new QLabel("From",this);
    AnalogiesEndLabel = new QLabel("To",this);

    AnalogiesGroup->setLayout(new QHBoxLayout(AnalogiesGroup));
    AnalogiesGroup->layout()->addWidget (AnalogiesStartLabel);
    AnalogiesGroup->layout()->addWidget (AnalogiesStartBox);
    AnalogiesGroup->layout()->addWidget (AnalogiesEndLabel);
    AnalogiesGroup->layout()->addWidget (AnalogiesEndBox);

    leftLayout->addWidget(AnalogiesGroup);



    StatisticsGroup = new QGroupBox("Statistics",this);

    estimatorsView = new QTableView(StatisticsGroup);
    estimatorsView->verticalHeader()->setVisible(false);
    estimatorsView->horizontalHeader()->setVisible(false);
    estimatorsView->resizeColumnsToContents();

    estimatorsView->setModel(&estimatorModel);


    StatisticsGroup->setLayout(new QVBoxLayout(StatisticsGroup));
    StatisticsGroup->layout()->addWidget(estimatorsView);
    leftLayout->addWidget(StatisticsGroup);


    MinkowskiLamdaGroup = new QGroupBox("Minkowski Lamda",this);
    MinkowskiLamdaBox = new QSpinBox(this);
    MinkowskiLamdaBox->setRange(3,10);
    MinkowskiLamdaBox->setValue(3);
    MinkowskiLamdaGroup->setLayout(new QVBoxLayout(MinkowskiLamdaGroup));
    MinkowskiLamdaGroup->layout()->addWidget(MinkowskiLamdaBox);

    rightLayout->addWidget(MinkowskiLamdaGroup);

    DistanceMetricGroup = new QGroupBox("Distance Metrics",this);
    distanceMetricView = new QTableView(DistanceMetricGroup);
    distanceMetricView->verticalHeader()->setVisible(false);
    distanceMetricView->horizontalHeader()->setVisible(false);
    distanceMetricView->resizeColumnsToContents();

    distanceMetricView->setModel(&distanceMetricModel);


    DistanceMetricGroup->setLayout(new QVBoxLayout(DistanceMetricGroup));
    DistanceMetricGroup->layout()->addWidget(distanceMetricView);

    rightLayout->addWidget(DistanceMetricGroup);












    connect(PredRadioButton,SIGNAL(toggled(bool)),PredValueSpinBox,SLOT(setEnabled(bool)));
    connect(PlayAction,SIGNAL(triggered()),this,SLOT(exportConfiguration()));


    connect(DataBase::instance(), &DataBase::projectsInserted, [&]() {
        this->updateAnalogiesBox();
    });
}

BestParametersDialog::~BestParametersDialog()
{
}

int BestParametersDialog::getMinAnalogies() const
{
    return AnalogiesStartBox->text().toInt();
}

int BestParametersDialog::getMaxAnalogies() const
{
    return AnalogiesEndBox->text().toInt();
}

std::string BestParametersDialog::getSizeAdjustingAttribute() const
{
    return SizeAdjustingAttrComboBox->currentText().toStdString();
}

int BestParametersDialog::getMinkowskiLamda() const
{
    return MinkowskiLamdaBox->text().toInt();
}

QString BestParametersDialog::getEvaluationMetric() const
{
    if(MmreRadioButton->isChecked())
        return QString("MMRE");
    else if(MdmreRadioButton->isChecked())
        return QString("MDMRE");
    else if(PredRadioButton->isChecked())
        return QString("PRED25");

    return QString("INVALID");
}


int BestParametersDialog::getPredPercentage() const
{
    return PredValueSpinBox->text().toInt();
}

QString BestParametersDialog::getAttributeToBeEstimated() const
{
    return AttrToBeEstimatedComboBox->currentText();
}

QComboBox *BestParametersDialog::createComboBox(const QString &text)
{
    QComboBox *comboBox = new QComboBox;
    comboBox->setEditable(true);
    comboBox->addItem(text);
    comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    return comboBox;
}

void BestParametersDialog::updateComboBox(QComboBox *comboBox)
{
    if (comboBox->findText(comboBox->currentText()) == -1)
        comboBox->addItem(comboBox->currentText());
}

void BestParametersDialog::exportConfiguration() const
{
    if(DataBase::instance()->hasAttributes()) {
        try {
            Configuration exp;
            exp.exportConfig(directoryComboBox->currentText(), prefixLineEdit->text(), getEvaluationMetric(), getAttributeToBeEstimated(), "" /*getSizeAdjustingAttribute().c_str()*/, DataBase::instance()->getActiveAttributes(), getMinAnalogies(), getMaxAnalogies(),
                             distanceMetricModel.getSelectedDistanceMetrics(), estimatorModel.getSelectedEstimators(), methodType->currentIndex(), configFilesCounterSpinbox->value());
        } catch(std::exception &e) {
            handleException(e);
        }
    }
}

void BestParametersDialog::browse()
{
    QString directory =
            QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this, tr("Find Files"), QDir::currentPath()));

    if (!directory.isEmpty()) {
        if (directoryComboBox->findText(directory) == -1)
            directoryComboBox->addItem(directory);
        directoryComboBox->setCurrentIndex(directoryComboBox->findText(directory));
    }
}

