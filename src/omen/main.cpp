#include <QApplication>
#include <QCommandLineParser>
#include "MainWindow.hpp"
#include "DataBase.hpp"
#include "DataBaseModel.hpp"

#include <bitset>
#include <iostream>

int main(int argc , char **argv)
{
    QApplication OmenApp(argc,argv);

    QCoreApplication::setApplicationName("Omen");
    QCoreApplication::setApplicationVersion("1.0");
    QCommandLineParser parser;

    QCommandLineOption help( "h", "help");
    parser.addOption(help);

    QCommandLineOption version( "v", "version");
    parser.addOption(version);

    parser.process(OmenApp);

    if(parser.isSet(version)){
        std::cout << QCoreApplication::applicationName().toStdString() << " " << QCoreApplication::applicationVersion().toStdString() << std::endl;
        return 0;
    } else if(parser.isSet(help)){
        std::cout << "Options:\n";
        std::cout << "\t-v    Application Version\n";
        return 0;
    }


   // DBModel = new DataBaseModel(DataBase::instance());


    OmenApp.setActiveWindow(new MainWindow);
	OmenApp.activeWindow()->show();

    return OmenApp.exec();
}
