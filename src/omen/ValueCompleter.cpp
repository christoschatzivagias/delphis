#include "ValueCompleter.hpp"
#include <QHBoxLayout>
#include <QTableView>
#include <QStatusBar>

#include <QPainter>
#include <QPushButton>
#include <QStylePainter>
#include <QDialogButtonBox>
#include <sstream>

#include "Enums.h"
#include "CommonFunctionsContainer.hpp"

#include <iostream>

ComboBoxDelegate::ComboBoxDelegate(QStandardItemModel *model) : QItemDelegate(model)
{
    this->model = model;
}

QWidget *ComboBoxDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &index) const
{
    int row = index.row();
   QString attrname = model->item(row, 0)->text();

   int type = DataBase::instance()->getAttributeType(attrname.toStdString());

   QComboBox *editor = new QComboBox(parent);
   if(type == NUMERICAL) {
       editor->addItem("Zero");
       editor->addItem("Mean");
       editor->addItem("Median");
       editor->addItem("Mode");
   } else if(type == CATEGORICAL) {
       editor->addItem("Mode");
   }

   return editor;
}

void ComboBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
   QString value = index.model()->data(index, Qt::EditRole).toString();

   QComboBox *cBox = static_cast<QComboBox*>(editor);
   cBox->setCurrentIndex(cBox->findText(value));
}

void ComboBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
   QComboBox *cBox = static_cast<QComboBox*>(editor);
   QString value = cBox->currentText();

   model->setData(index, value, Qt::EditRole);
}

void ComboBoxDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
   editor->setGeometry(option.rect);
}

ValueCompleter::ValueCompleter(DataBase *db)
{
    this->db = db;
    findMissingValues(nullValues);
}

void ValueCompleter::findMissingValues(QMap<QString, QVector<int> > &nullValues)
{
    QStringList names = db->getActiveAttributes();
    auto constructWhereQuery = [&](const QString &name) {
        QSqlQuery qr(db->getDb());

        qr.exec("select * from projects where Status=1 AND `" + name + "`=''");

        while (qr.next()) {
            QMap<QString, QVector<int> >::iterator it = nullValues.find(name);
            if(it == nullValues.end()) {
                QVector<int> vec = {qr.value(0).toInt()};

                nullValues[name] = vec;
            } else {
                QVector<int> vec = it.value();
                vec.push_back(qr.value(0).toInt());
                nullValues[it.key()] = vec;
            }
        }
    };

    std::for_each(names.begin(), names.end(), constructWhereQuery);
}

QString ValueCompleter::fillAndUpdate(const QString &attrname, const QString &fillmethod)
{
    QString status = "Missing Values Filled";
    QString fillValue = "";

    QSqlQuery qr(db->getDb());
    QVector<int> vec = getNullValuesID(attrname);


    int type = db->getAttributeType(attrname.toStdString());
    if(type == CATEGORICAL) {
        QVector<QString> validValues;
        collectValidValues(attrname, validValues);
        if(fillmethod == "Mode") {
            fillValue = qModeValue(validValues);
        } else {
            status = "Unable to apply " + fillmethod + " method to a categorical attribute:" + attrname;
        }
    } else if(type == NUMERICAL) {
        QVector<double> validValues;

        collectValidValues(attrname, validValues);
        if(fillmethod == "Zero") {
            fillValue = "0";
        } else if(fillmethod == "Mean") {
            fillValue = QString().number(qMeanValue(validValues));
        } else if(fillmethod == "Median") {
            fillValue =  QString().number(qMedianValue(validValues));
        } else if(fillmethod == "Mode") {
            fillValue = qModeValue(validValues);
        }
    }

    auto fillNullValues = [&](int index) {
        QString query = QString("UPDATE projects SET '") + attrname + "'='" + fillValue + "' WHERE OMEN_ID=" + QString().number(index) + ";";
        qr.exec(query);
    };

    std::for_each(vec.begin(), vec.end(), fillNullValues);
    db->updateTables();
    return status;
}

void ValueCompleter::collectValidValues(const QString &attrname, QVector<double> &validValues)
{
    QVector<int> vec = getNullValuesID(attrname);
    QString queryToMissInvalidValuesIds = "OMEN_ID NOT IN(";
    for(int index = 0; index < vec.size(); index++) {
        queryToMissInvalidValuesIds += QString().number(vec[index]) + ", ";
    }

    queryToMissInvalidValuesIds = queryToMissInvalidValuesIds.left(queryToMissInvalidValuesIds.count() - 2) + ")";

    QSqlQuery qr(db->getDb());

    qr.exec(QString("select ") + attrname + " from projects where " + attrname + "<>'' AND " + queryToMissInvalidValuesIds);

    while (qr.next()) {
        validValues.push_back(qr.value(0).toDouble());
    }
}

void ValueCompleter::collectValidValues(const QString &attrname, QVector<QString> &validValues)
{
    QVector<int> vec = getNullValuesID(attrname);
    QString queryToMissInvalidValuesIds = "OMEN_ID NOT IN(";
    for(int index = 0; index < vec.size(); index++) {
        queryToMissInvalidValuesIds += QString().number(vec[index]) + ", ";
    }

    queryToMissInvalidValuesIds = queryToMissInvalidValuesIds.left(queryToMissInvalidValuesIds.count() - 2) + ")";

    QSqlQuery qr(db->getDb());

    qr.exec(QString("select ") + attrname + " from projects where " + attrname + "<>'' AND " + queryToMissInvalidValuesIds);

    while (qr.next()) {
        validValues.push_back(qr.value(0).toString());
    }
}

QVector<int> ValueCompleter::getNullValuesID(const QString &attrname) {
    QMap<QString, QVector<int> >::iterator it = nullValues.find(attrname);
    if(it != nullValues.end()) {
        return it.value();
    }
    
    return QVector<int>();
}


ValueCompleterWindow::ValueCompleterWindow(QWidget *parent) : QDialog(parent)
{
    setWindowTitle("Missing Values Filler");
    QMap<QString, QVector<int> > nullValues;
    valueCompleter->findMissingValues(nullValues);
    mainLayout = new QVBoxLayout(this);
    table = new QTableView(this);
    table->setModel(&model);
    mainLayout->addWidget(table);
    model.setColumnCount(2);
    model.setHeaderData(0, Qt::Horizontal, "Attribute", Qt::DisplayRole);
    model.setHeaderData(1, Qt::Horizontal, "Completion Method", Qt::DisplayRole);

    QList<QString> attrs = nullValues.keys();

    table->setItemDelegateForColumn(1, new ComboBoxDelegate(&model));


    auto fillAttrWithMissingValuesModel = [&] (const QString &attrname) {
        QList<QStandardItem *> items;
        int type = DataBase::instance()->getAttributeType(attrname.toStdString());
        items << new QStandardItem(attrname);

        if(type == NUMERICAL) {
            items << new QStandardItem("Zero");
        } else if(type == CATEGORICAL) {
            items << new QStandardItem("Mode");
        }
        model.appendRow(items);
    };

    std::for_each(attrs.begin(), attrs.end(), fillAttrWithMissingValuesModel);

    dialogButtonBox = new QDialogButtonBox(QDialogButtonBox::Apply, Qt::Horizontal, this);
    statusbar = new QStatusBar(this);
    mainLayout->addWidget(statusbar);
    mainLayout->addWidget(dialogButtonBox);

    connect(dialogButtonBox, &QDialogButtonBox::clicked, this, &ValueCompleterWindow::fillMissingValuesSlot);
}

void ValueCompleterWindow::fillMissingValuesSlot()
{
    for(int index = 0; index < model.rowCount(); index++) {
        QString attrname = model.item(index, 0)->data(Qt::DisplayRole).toString();
        QString fillmethod = model.item(index, 1)->data(Qt::DisplayRole).toString();

        QString status = valueCompleter->fillAndUpdate(attrname, fillmethod);
        statusbar->showMessage(status);
    }
}

