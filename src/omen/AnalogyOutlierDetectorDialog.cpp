#include <QtGlobal>
#include <QComboBox>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QToolBar>
#include <QStackedWidget>
#include <QFrame>
#include <QGroupBox>
#include <QGridLayout>
#include <QRadioButton>
#include <QTableView>
#include <QLabel>
#include <QCheckBox>
#include <QHeaderView>
#include <QStatusBar>
#include "DistanceMetricsModel.hpp"
#include "EstimatorsModel.hpp"
#include "AnalogyOutlierDetectorDialog.hpp"
#include "DataBase.hpp"
#include "AnalogyOutlierDetector.hpp"
#include "SpinBoxDelegate.hpp"

AnalogyOutlierDialog::AnalogyOutlierDialog(QWidget *parent) : QDialog(parent)
{
    attributesModel = new AttributesSelectionModel(DataBase::instance(),this);
    this->setWindowTitle("Outlier Research");
    this->setWindowIcon(QIcon(":/sources/estimation1.png"));
    int analogies = DataBase::instance()->getActiveProjectsCount()-1;

    toolBar = new QToolBar(this);

    PlayAction = new QAction(QIcon(":/sources/playAlg.png"),"Play",toolBar);



    methodsLabel = new QLabel("Methods", toolBar);
    methodsCombobox = new QComboBox(toolBar);
    methodsCombobox->setModel(DataBaseModel::instance()->getMethodModel());

    pagesComboBox = new QComboBox(toolBar);
    pagesComboBox->insertItems(0,QStringList() << "Parameters" << "Results");

    toolBar->addAction(PlayAction);
    toolBar->addSeparator();
    toolBar->addWidget(methodsLabel);
    toolBar->addWidget(methodsCombobox);
    toolBar->addSeparator();
    toolBar->addWidget(pagesComboBox);

    connect(methodsCombobox, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), [&] (int index) {
        AnalogyMethodParameters params = MethodModel::getMethodParameters(index);
        distanceMetricView->setCurrentText(params.getDistanceMetric());
        estimatorsView->setCurrentText(params.getStatistic());
        AnalogiesBox->setValue(params.getAnalogies());
        AttrToBeEstimatedComboBox->setCurrentText(params.getEstimationAttribute());
        attributesModel->setWeights(params.subset());

        if(params.getEvaluationMethod() == "MMRE")
            MmreRadioButton->setChecked(true);
        else if(params.getEvaluationMethod() == "MDMRE")
            MdmreRadioButton->setChecked(true);
        else if(params.getEvaluationMethod() == "PRED25")
            PredRadioButton->setChecked(true);
    });

    connect(PlayAction, &QAction::triggered, [&] () {
        AnalogyOutlierDetector s;

        QVector<QPair<QString, double > > attrWeights = attributesModel->getWeights();
        std::remove_if(attrWeights.begin(), attrWeights.end(), [&](const QPair<QString, double> &pair){
            if(pair.first == AttrToBeEstimatedComboBox->currentText())
                return true;
            return false;
        });


        QVector<QPair<double, QString >> results = s.search(getEvaluationMetric(), attrWeights, AttrToBeEstimatedComboBox->currentText(), distanceMetricView->currentText(), estimatorsView->currentText(),AnalogiesBox->value());

        m_outlierResultsModel.clear();
        m_outlierResultsModel.setColumnCount(2);
        m_outlierResultsModel.setHorizontalHeaderLabels(QStringList() << "Project Description" << "Magnitude Relative Difference");
        QVector<double> MRDvalues;
        MRDvalues.reserve(results.count());
        std::for_each(results.begin(), results.end(), [&](const QPair<double, QString > &pair){
            m_outlierResultsModel.appendRow(QList<QStandardItem *>() << new QStandardItem(pair.second) << new QStandardItem(QString::number(pair.first)));
            MRDvalues.push_back(pair.first);
        });

        double meanvalue = mean(MRDvalues.toStdVector());
        double variancevalue = variance(MRDvalues.toStdVector());

        QString info = QString("Mean of Magnitude Relative Difference " + QString::number(meanvalue) + " limits [χ+σ,χ+2σ,χ+3σ] [" +
                               QString::number(meanvalue + variancevalue) + "," +
                               QString::number(meanvalue + 2*variancevalue) + "," +
                               QString::number(meanvalue + 3*variancevalue) + "]");

        statisticsInfo->setText(info);

        pagesComboBox->setCurrentIndex(1);

    });


    mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(toolBar);
    stackWidget = new QStackedWidget(this);
    resultsFrame = new QFrame(stackWidget);
    resultsLayout = new QVBoxLayout(resultsFrame);
    mp_outlierResultView = new QTableView(resultsFrame);
    mp_outlierResultView->setModel(&m_outlierResultsModel);
    mp_outlierResultView->setSortingEnabled(true);
    mp_outlierResultView->sortByColumn(1, Qt::DescendingOrder);
    mp_outlierResultView->setEditTriggers(QTableView::NoEditTriggers);
    resultsLayout->addWidget(mp_outlierResultView);
    statisticsInfo = new QLabel(resultsFrame);
    resultsLayout->addWidget(statisticsInfo);
    mainLayout->addWidget(stackWidget);
    parametersWidget = new QFrame(stackWidget);
    stackWidget->addWidget(parametersWidget);
    stackWidget->addWidget(resultsFrame);
    centralLayout = new QHBoxLayout(parametersWidget);

    leftLayout = new QVBoxLayout(parametersWidget);
    middleLayout = new QVBoxLayout(parametersWidget);
    rightLayout = new QVBoxLayout(parametersWidget);
    connect(pagesComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int index){
       stackWidget->setCurrentIndex(index);
    });

    centralLayout->addLayout(leftLayout);
    centralLayout->addLayout(middleLayout);
    centralLayout->addLayout(rightLayout);


    DistanceMetricGroup = new QGroupBox("Distance Metrics",this);
    distanceMetricView = new QComboBox(DistanceMetricGroup);

    distanceMetricView->setModel(&distanceMetricModel);

    DistanceMetricGroupLayout = new QVBoxLayout(DistanceMetricGroup);

    DistanceMetricGroupLayout->addWidget(distanceMetricView);
    DistanceMetricGroup->setLayout(DistanceMetricGroupLayout);
    leftLayout->addWidget(DistanceMetricGroup);


    StatisticsGroup = new QGroupBox("Statistics",this);

    estimatorsView = new QComboBox(StatisticsGroup);


    estimatorsView->setModel(&estimatorModel);
    StatisticsGroupLayout = new QVBoxLayout(StatisticsGroup);

    StatisticsGroupLayout->addWidget(estimatorsView);

    StatisticsGroup->setLayout(StatisticsGroupLayout);
    leftLayout->addWidget(StatisticsGroup);


    AnalogiesGroup = new QGroupBox("Analogies",this);
    AnalogiesGroupLayout = new QGridLayout();
    AnalogiesBox = new QSpinBox(this);
    AnalogiesBox->setRange(1,analogies);
    AnalogiesBox->setValue(1);

    AnalogiesGroupLayout->addWidget(AnalogiesBox);
    AnalogiesGroup->setLayout(AnalogiesGroupLayout);

    middleLayout->addWidget(AnalogiesGroup);

    EvaluationsMetricGroup = new QGroupBox("Evaluation Metrics",this);
    EvaluationsMetricGroupLayout = new QVBoxLayout(EvaluationsMetricGroup);
    MmreRadioButton = new QRadioButton("MMRE",this);
    MmreRadioButton->setChecked(true);
    MdmreRadioButton = new QRadioButton("MdMRE",this);
    PredRadioButton = new QRadioButton("Pred25",this);
    PredValueSpinBox = new QSpinBox(this);
    PredValueSpinBox->setValue(25);
    PredValueSpinBox->setRange (1,99);
    PredValueSpinBox->setSuffix("%");
    PredValueSpinBox->setDisabled(true);
    PredValueSpinBox->setVisible(false);

    EvaluationsMetricGroupLayout->addWidget(MmreRadioButton);
    EvaluationsMetricGroupLayout->addWidget(MdmreRadioButton);
    EvaluationsMetricGroupLayout->addWidget(PredRadioButton);
    EvaluationsMetricGroupLayout->addWidget(PredValueSpinBox);

    middleLayout->addWidget(EvaluationsMetricGroup);

    ProjectAttributesGroup = new QGroupBox("Project Attributes",this);
    ProjectAttributesGroupLayout = new QVBoxLayout(ProjectAttributesGroup);


    AttrToBeEstimatedLabel = new QLabel("Attribute To Be Estimated",this);
    AttrToBeEstimatedComboBox = new QComboBox(this);
    AttrToBeEstimatedComboBox->setModel(attributesModel);


    ProjectAttributesGroupLayout->addWidget(AttrToBeEstimatedLabel);
    ProjectAttributesGroupLayout->addWidget(AttrToBeEstimatedComboBox);
    ProjectAttributesGroupBox = new QGroupBox("Attributes", this);
    AttributesLayout = new QVBoxLayout(ProjectAttributesGroupBox);
    mp_attributesView = new QTableView(ProjectAttributesGroupBox);
    mp_attributesView->setItemDelegateForColumn(2, new SpinBoxDelegate(mp_attributesView));
    mp_attributesView->verticalHeader()->setVisible(false);
    mp_attributesView->horizontalHeader()->setVisible(false);
    mp_attributesView->resizeColumnsToContents();

    mp_attributesView->setModel(attributesModel);
    AttributesLayout->addWidget(mp_attributesView);
    ProjectAttributesGroupLayout->addWidget(ProjectAttributesGroupBox);



    ProjectAttributesGroup->setLayout(ProjectAttributesGroupLayout);
    rightLayout->addWidget(ProjectAttributesGroup);

    StatusBar = new QStatusBar(this);
    StatusBar->setSizeGripEnabled(true);
    mainLayout->addWidget(StatusBar);

    setWindowModality(Qt::ApplicationModal);

}

QString AnalogyOutlierDialog::getEvaluationMetric() const
{
    if(MmreRadioButton->isChecked())
        return QString("MMRE");
    else if(MdmreRadioButton->isChecked())
        return QString("MDMRE");
    else if(PredRadioButton->isChecked())
        return QString("PRED25");

    return QString("INVALID");
}
