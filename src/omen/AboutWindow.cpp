#include <QHBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QTextEdit>
#include <QApplication>

#include "AboutWindow.hpp"

AboutWindow::AboutWindow(QWidget *parent) : QDialog(parent)
{
        init();
}

void AboutWindow::init()
{
    mp_mainLayout = new QHBoxLayout(this);
    mp_iconLabel = new QLabel(this);
    mp_textLabel = new QTextEdit(this);

    mp_iconLabel->setPixmap(QIcon(":/sources/money.png").pixmap(64, 64));

    mp_textLabel->setReadOnly(true);
    mp_textLabel->setHtml(m_aboutText);

    mp_mainLayout->addWidget(mp_iconLabel);
    mp_mainLayout->addWidget(mp_textLabel);
}
