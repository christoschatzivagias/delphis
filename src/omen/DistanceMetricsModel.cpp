#include "DistanceMetricsModel.hpp"


DistanceMetricsModel::DistanceMetricsModel(QObject *parent)
{
    int index = 0;
    QStandardItem *nameitem = nullptr;
    QStandardItem *statusitem = nullptr;
    auto insertDstItem = [&] (std::pair<std::string, bool> &dstpair) {
        nameitem = new QStandardItem(dstpair.first.c_str());
        nameitem->setEditable(false);
        setItem(index,0, nameitem);
        statusitem = new QStandardItem(true);
        statusitem->setCheckable(true);
        statusitem->setCheckState(dstpair.second ? Qt::Checked : Qt::Unchecked);
        setItem(index,1, statusitem);
        index++;
    };

    std::for_each(DistanceMetrics.begin(), DistanceMetrics.end(), insertDstItem);
}

bool DistanceMetricsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    std::pair<std::string, bool> p = DistanceMetrics[index.row()];
    p.second = value.toBool();
    DistanceMetrics[index.row()] = p;
    return QStandardItemModel::setData(index, value, role);
}

QStringList DistanceMetricsModel::getSelectedDistanceMetrics() const
{
    QStringList selectedDstMetrics;

    auto filterSelectedDstMetric = [&](const std::pair<std::string, bool> &p) {
      if(p.second) {
          selectedDstMetrics.append(p.first.c_str());
      }
    };

    std::for_each(DistanceMetrics.begin(), DistanceMetrics.end(), filterSelectedDstMetric);
    return selectedDstMetrics;
}
