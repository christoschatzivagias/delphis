#include <QApplication>
#include <QFontDialog>
#include "MainToolBar.hpp"
#include "MainWindow.hpp"
#include <QAction>

MainToolBar::MainToolBar(MainWindow *parent):QToolBar(parent)
{
    addAction(ActionsContainer::instance()->getActionByName("Load"));
    addAction(ActionsContainer::instance()->getActionByName("Save"));
    addSeparator();
    addAction(ActionsContainer::instance()->getActionByName("ImportCSV"));
    addAction(ActionsContainer::instance()->getActionByName("ImportARFF"));

    addSeparator();
    addAction(ActionsContainer::instance()->getActionByName("Font"));
}
