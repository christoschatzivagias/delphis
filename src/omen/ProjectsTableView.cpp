#include "ProjectsTableView.hpp"

ProjectsTableView::ProjectsTableView(QWidget *parent) : QTableView(parent)
{
    setSortingEnabled(true);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setEditTriggers(QTableView::NoEditTriggers);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    resizeColumnsToContents();
    resizeRowsToContents();
    setContextMenuPolicy(Qt::CustomContextMenu);
}

void ProjectsTableView::setModel(ProjectsModel *tmd)
{
    mp_sortFilterProxy->setSourceModel(tmd);
    QTableView::setModel(mp_sortFilterProxy);
    sortByColumn(1, Qt::AscendingOrder);
}

void ProjectsTableView::setModel(TargetsModel *tmd)
{
    mp_sortFilterProxy->setSourceModel(tmd);
    QTableView::setModel(mp_sortFilterProxy);
    sortByColumn(1, Qt::AscendingOrder);
}
