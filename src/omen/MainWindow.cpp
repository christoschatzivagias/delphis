#include <QMessageBox>
#include <QComboBox>
#include <QTableView>
#include <QStatusBar>
#include <QProgressDialog>
#include <QProgressBar>
#include <QtConcurrent/QtConcurrent>
#include <QMutex>
#include <QApplication>
#include <QFileDialog>
#include <QFontDialog>
#include <QFileDialog>

#include <exception>
#include <limits.h>

#include "MainWindow.hpp"
#include "ThreadManager.hpp"
#include "CommonFunctionsContainer.hpp"
#include "ValueCompleter.hpp"
#include "CategoricalTransformWindow.hpp"
#include "Estimators.hpp"
#include "Exception.hpp"
#include "arff_parser.h"
#include "Enums.h"
#include "Timer.hpp"
#include "MainMenuBar.hpp"
#include "MainToolBar.hpp"
#include "TabArea.hpp"
#include "SimpleAnalogySearchDialog.hpp"
#include "AnalogyOutlierDetectorDialog.hpp"
#include "AboutWindow.hpp"

MainWindow *MainWindow::instancePtr = nullptr;
ActionsContainer *ActionsContainer::instancePtr = nullptr;



MainWindow::MainWindow(QWidget * parent, Qt::WindowFlags WFlags):QMainWindow(parent,WFlags)
{
    MainWindow::instancePtr = this;
    this->setWindowIcon(QIcon(":/sources/money.png"));
    mp_mainMenuBar = new MainMenuBar(this);
    mp_mainToolBar = new MainToolBar(this);
    mp_tabWidget = new TabArea(this);


    setMenuBar(mp_mainMenuBar);
    addToolBar(mp_mainToolBar);
    setCentralWidget(mp_tabWidget);
    mp_statusBar = new QStatusBar(this);
    mp_progressBar = new QProgressBar(mp_statusBar);

    mp_statusBar->addPermanentWidget(mp_progressBar);

    this->setStatusBar(mp_statusBar);

    resize(1200,1400);

    QObject::connect(ThreadManager::instance(), &ThreadManager::progressValueChanged, mp_progressBar, &QProgressBar::setValue);
    QObject::connect(ThreadManager::instance(), &ThreadManager::messageChanged, mp_statusBar, &QStatusBar::showMessage);
    QObject::connect(ThreadManager::instance(), &ThreadManager::resetBusySignal, mp_progressBar, [&] () {
        mp_progressBar->setMinimum(0);
        mp_progressBar->setMaximum(100);
        qApp->processEvents();
    });

    QObject::connect(ThreadManager::instance(), &ThreadManager::setBusySignal, mp_progressBar, [&] () {
        mp_progressBar->setMinimum(0);
        mp_progressBar->setMaximum(0);
        qApp->processEvents();
    });

    m_fileSaved = false;
}

MainWindow *MainWindow::instance()
{
	if (!instancePtr)
		instancePtr = new MainWindow;

    return instancePtr;
}

void MainWindow::setProgressValue(int value)
{
    mp_progressBar->setValue(value);
}

void MainWindow::CloseSlot()
{
    disconnect(qApp,SIGNAL(aboutToQuit()),this,SLOT(CloseSlot()));
    NewDataBaseSlot();
    qApp->quit();
}

MainWindow::~MainWindow()
{
}

void MainWindow::MissingValueScanSlot()
{
    if(DataBase::instance()->hasAttributes()) {
        ValueCompleterWindow *vcwin = new ValueCompleterWindow(this);
        vcwin->show();
    }
}

void MainWindow::CreateFileChooserDialogSlot()
{
    mp_chooseFileDialog = new QFileDialog(this);
    mp_chooseFileDialog->setWindowModality(Qt::ApplicationModal);
    mp_chooseFileDialog->setAcceptMode (QFileDialog::AcceptOpen);
    mp_chooseFileDialog->setNameFilter("ini Files (*.ini)");
    mp_chooseFileDialog->setDefaultSuffix("ini");
    connect(mp_chooseFileDialog,SIGNAL(fileSelected(QString)),this,SLOT(LoadAttrsAndProjectsAscii(QString)));
    mp_chooseFileDialog->show();
}

void MainWindow::CreateARFFImportFileDialogSlot()
{
    QFileDialog importFileDialog(this);
    importFileDialog.setWindowModality(Qt::ApplicationModal);
    importFileDialog.setAcceptMode (QFileDialog::AcceptOpen);
    importFileDialog.setNameFilter("Attribute Relation File Format(*.arff)");
    importFileDialog.setDefaultSuffix("arff");
    connect(&importFileDialog, &QFileDialog::fileSelected, [&] (const QString &name) {
        LoadAttrsAndProjectsArff(name);
    });
    importFileDialog.show();
    importFileDialog.exec();
}

void MainWindow::CreateFileSaverDialogSlot()
{
    QFileDialog saveFileDialog(this);
    saveFileDialog.setWindowModality(Qt::ApplicationModal);
    saveFileDialog.setAcceptMode (QFileDialog::AcceptSave);
    saveFileDialog.setNameFilter("ini Files (*.ini)");
    saveFileDialog.setDefaultSuffix("ini");
    connect(&saveFileDialog, &QFileDialog::fileSelected,[&](const QString &filePath) {
        mp_asciiFMan->saveAsciiFile(filePath.toStdString());
    });
    saveFileDialog.show();
    saveFileDialog.exec();
}

void MainWindow::CreateARFFExportFileDialogSlot()
{
    QFileDialog importFileDialog(this);
    importFileDialog.setWindowModality(Qt::ApplicationModal);
    importFileDialog.setAcceptMode (QFileDialog::AcceptSave);
    importFileDialog.setNameFilter("Attribute Relation File Format(*.arff)");
    importFileDialog.setDefaultSuffix("arff");
    connect(&importFileDialog, &QFileDialog::fileSelected,[&](const QString &filePath) {
        mp_asciiFMan->saveArffFile(filePath.toStdString());
    });
    importFileDialog.show();
    importFileDialog.exec();
}


void MainWindow::CreateCSVImportFileDialogSlot()
{
    QFileDialog importFileDialog(this);
    importFileDialog.setWindowModality(Qt::ApplicationModal);
    importFileDialog.setAcceptMode (QFileDialog::AcceptOpen);
    importFileDialog.setNameFilter("Comma Separated File Format(*.csv)");
    importFileDialog.setDefaultSuffix("csv");
    connect(&importFileDialog, &QFileDialog::fileSelected,[&](const QString &filePath) {
        LoadAttrsAndProjectsCSV(filePath);
    });
    importFileDialog.show();
    importFileDialog.exec();
}

void MainWindow::CreateCSVTargetsImportFileDialogSlot()
{
    QFileDialog importFileDialog(this);
    importFileDialog.setWindowModality(Qt::ApplicationModal);
    importFileDialog.setAcceptMode (QFileDialog::AcceptOpen);
    importFileDialog.setNameFilter("Comma Separated File Format(*.csv)");
    importFileDialog.setDefaultSuffix("csv");
    connect(&importFileDialog, &QFileDialog::fileSelected,[&](const QString &filePath) {
        LoadAttrsAndTargetsCSV(filePath);
    });
    importFileDialog.show();
    importFileDialog.exec();
}


void MainWindow::CreateMethodImporterDialogSlot()
{
    if(!DataBase::instance()->hasAttributes()) {
        return;
    }
    QFileDialog methodImporterDialog(this);
    methodImporterDialog.setWindowModality(Qt::ApplicationModal);
    methodImporterDialog.setAcceptMode (QFileDialog::AcceptOpen);
    methodImporterDialog.setNameFilter("ini Files (*.ini)");
    methodImporterDialog.setDefaultSuffix("ini");
    methodImporterDialog.setFileMode(QFileDialog::ExistingFiles);
    connect(&methodImporterDialog,SIGNAL(filesSelected(QStringList)),this,SLOT(ImportMethodIni(QStringList)));
    methodImporterDialog.show();
    methodImporterDialog.exec();
}

void MainWindow::LoadAttrsAndProjectsAscii(const QString &File)
{
    int option;
    option = NewDataBaseSlot();
    if(option == 1) return;

    int fileReadResult = NoError;
    //AsciiFMan = new AsciiFileManager();
    mp_asciiFMan->readAsciiFile(File.toStdString());

    if(fileReadResult == DuplicateAttributeName)
    {
        mp_chooseFileDialog->hide();
        QMessageBox::critical( this, "Read Error", "There are 2 attributes with the same name the file has corrupted data", QMessageBox::Ok);
        return;
    }
    else if(fileReadResult == DuplicateProjectName)
    {
        mp_chooseFileDialog->hide();
        QMessageBox::critical( this, "Read Error", "There are 2 projects with the same name the file has corrupted data", QMessageBox::Ok);
        return;
    }
    else if(fileReadResult == InvalidValueForNumericAttribute)
    {
        mp_chooseFileDialog->hide();
        QMessageBox::critical( this, "Read Error", "Numeric Attribute has invalid value.", QMessageBox::Ok);
        return;
    }
    else if(fileReadResult == SyntaxError)
    {
        mp_chooseFileDialog->hide();
        QMessageBox::critical( this, "Read Error", "Syntax error at ini file.", QMessageBox::Ok);
        return;
    }


    try {
        DataBase::instance()->setDBAttributes(mp_asciiFMan->getDBAttributes());
        DataBase::instance()->setDBProjects(mp_asciiFMan->getDBProjects());
        DataBase::instance()->setDBTargets(mp_asciiFMan->getDBTargets());
    } catch(std::exception &e) {
        handleException(e);
    }
}

void MainWindow::ImportMethodIni(const QStringList &Files)
{

    std::for_each(Files.begin(), Files.end(), [&](const QString &File) {
        AnalogyMethodParameters params;
        try {
            params.importFile(File);
            QStringList methodAttrs = params.getAttributeNames();
            QStringList methodTypes = params.getAttributeTypes();


            for(int index = 0; index < methodAttrs.count(); index++) {
                if(!DataBase::instance()->hasAttribute(methodAttrs[index]))
                    throw IncompatibleMethodError();

                else {
                    if(DataBase::instance()->getAttributeType(methodAttrs[index].toStdString()) != methodTypes[index].toInt())
                        throw IncompatibleMethodError();
                }
            }
        } catch (std::exception &e) {
            handleException(e);
            return;
        }

        DataBase::instance()->addMethod(params);
        DataBaseModel::instance()->getMethodModel()->addMethod(params);
    });
}

void MainWindow::LoadAttrsAndProjectsArff(const QString &File)
{
    int option = NewDataBaseSlot();
    if(option == 1)
        return;

    mp_asciiFMan->readArffFile(File.toStdString());

    try {
        DataBase::instance()->setDBAttributes(mp_asciiFMan->getDBAttributes());
        DataBase::instance()->setDBProjects(mp_asciiFMan->getDBProjects());
        DataBase::instance()->setDBTargets(mp_asciiFMan->getDBTargets());

    } catch(std::exception &e) {
        handleException(e);
    }
}

int MainWindow::NewDataBaseSlot()
{
    if(DataBase::instance()->hasAttributes()) {
        int option = QMessageBox::warning(0, "Warning", "This will cause the delete of database \n Are you sure you want to Proceed?", QMessageBox::Ok | QMessageBox::Cancel);

        if(option == QMessageBox::Cancel) {
            setDisabled(false);
            return 1;
        }
    }

    m_loadFileName = "";
    DataBase::instance()->clear();

    return 0;
}

void MainWindow::LoadAttrsAndProjectsCSV(const QString &file)
{
    int option = NewDataBaseSlot();
    if(option == 1)
        return;

    mp_asciiFMan->readCSVFile(file.toStdString());

    try {
        DataBase::instance()->setDBAttributes(mp_asciiFMan->getDBAttributes());
        DataBase::instance()->setDBProjects(mp_asciiFMan->getDBProjects());
        DataBase::instance()->setDBTargets(mp_asciiFMan->getDBTargets());

    } catch(std::exception &e) {
        handleException(e);
    }
}

void MainWindow::LoadAttrsAndTargetsCSV(const QString &file)
{
    mp_asciiFMan->importCSVTargets(file.toStdString());

    try {
        for(int index = 0; index < mp_asciiFMan->getDBAttributes().size(); index++) {
            if(!DataBase::instance()->hasAttribute(mp_asciiFMan->getDBAttributes()[index].getDescription().c_str()))
                throw IncompatibleMethodError();

            else {
               // if(DataBase::instance()->getAttributeType(attribute_list[index].getDescription()) != attribute_types_list[index])
               //     throw IncompatibleMethodError();
            }

        }
        DataBase::instance()->setDBTargets(mp_asciiFMan->getDBTargets());
    } catch(std::exception &e) {
        handleException(e);
    }
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    if(!m_fileSaved) {
        if(this->NewDataBaseSlot()==1)
            event->ignore();
    }
}

ActionsContainer::ActionsContainer() : QObject(QCoreApplication::instance())
{
    connect(getActionByName("Load"), &QAction::triggered, MainWindow::instance(), &MainWindow::CreateFileChooserDialogSlot);
    connect(getActionByName("Save"), &QAction::triggered, MainWindow::instance(), &MainWindow::CreateFileSaverDialogSlot);
    connect(getActionByName("ImportCSV"), &QAction::triggered, MainWindow::instance(), &MainWindow::CreateCSVImportFileDialogSlot);

    connect(getActionByName("ImportARFF"), &QAction::triggered, MainWindow::instance(), &MainWindow::CreateARFFImportFileDialogSlot);

    connect(getActionByName("ImportCSVTargets"), &QAction::triggered, MainWindow::instance(), &MainWindow::CreateCSVTargetsImportFileDialogSlot);
    connect(getActionByName("ImportMethod"), &QAction::triggered, MainWindow::instance(), &MainWindow::CreateMethodImporterDialogSlot);
    connect(getActionByName("CopyProjectsToTargets"), &QAction::triggered, MainWindow::instance(),  &MainWindow::CopySelectedProjectsToTargetsSignal);
    connect(getActionByName("ActivateProjects"), &QAction::triggered, MainWindow::instance(),  &MainWindow::ActivateSelectedProjectsSignal);
    connect(getActionByName("DeactivateProjects"), &QAction::triggered, MainWindow::instance(),  &MainWindow::DeactivateSelectedProjectsSignal);
    connect(getActionByName("ActivateTargets"), &QAction::triggered, MainWindow::instance(),  &MainWindow::ActivateSelectedTargetsSignal);
    connect(getActionByName("DeactivateTargets"), &QAction::triggered, MainWindow::instance(),  &MainWindow::DeactivateSelectedTargetsSignal);
    connect(getActionByName("ActivateAttributes"), &QAction::triggered, MainWindow::instance(),  &MainWindow::ActivateSelectedAttributesSignal);
    connect(getActionByName("DeactivateAttributes"), &QAction::triggered, MainWindow::instance(),  &MainWindow::DeactivateSelectedAttributesSignal);

    connect(getActionByName("Font"), &QAction::triggered, [=](){
        qApp->setFont(QFontDialog::getFont(0, qApp->font()));
    });

    connect(getActionByName("Exit"), &QAction::triggered, MainWindow::instance(), &MainWindow::CloseSlot);
    //connect(getActionByName("BestParameterEstimation"), &QAction::triggered, MainWindow::instance(),  &MainWindow::CreateBestParametersDialogSlot);
    connect(getActionByName("ValueCompleter"), &QAction::triggered, MainWindow::instance(),  &MainWindow::MissingValueScanSlot);
    connect(getActionByName("OutlierDetector"), &QAction::triggered, [&](){
        if(DataBase::instance()->hasAttributes())
            (new AnalogyOutlierDialog(MainWindow::instance()))->show();
    });


    connect(getActionByName("About"), &QAction::triggered, [&](){
        AboutWindow *abw = new AboutWindow(MainWindow::instance());
        abw->show();
    });

    connect(getActionByName("Contents"), &QAction::triggered, [](){
        QDesktopServices::openUrl(QUrl(tr("https://gitlab.com/chatzich/testreadme#omen")));
    });

    connect(getActionByName("AddTarget"), &QAction::triggered, [](){
        if(!DataBase::instance()->hasAttributes())
            return;

        int id = DataBase::instance()->getDBTargets().size() + 1;
        DataBase::instance()->addDBTarget(Project(id,
                                                  "User Target-" + std::to_string(id),
                                                  1,
                                                  DataBase::instance()->getDBAttributes(), Project::AS_TARGET));
    });

    connect(getActionByName("ExportResults"), &QAction::triggered, [](){
        CalculationParametersTab::exportResults();
    });
}
