#include "MethodModel.hpp"

QVector<std::tuple<AnalogyMethodParameters, QStandardItemModel *, QStandardItemModel *> > MethodModel::methods;
MethodModel::MethodModel(QObject *parent) : QStandardItemModel(parent)
{
    setColumnCount(columns.count());
    this->setHorizontalHeaderLabels(columns);
}


void MethodModel::addMethod(const AnalogyMethodParameters &params)
{
    QList<QStandardItem *> ll;
    MethodItem *methodlb = new MethodItem(QString("Method_") + QString::number(rowCount() + 1) );
    ll << methodlb;

    MethodItem *methodtype = new MethodItem("Analogy");
    ll << methodtype;

    MethodItem *methodeval = new MethodItem(QString::number(params.getBestEvaluation()));
    ll << methodeval;

    MethodItem *methodapply = new MethodItem(QString::null);
    methodapply->setIcon(QIcon(":/sources/playAlg.png"));
    ll << methodapply;

    appendRow(ll);

    QList<QStandardItem *> distanceMetricItemList;
    QStandardItem *distmetriclb = new QStandardItem("DistanceMetric");
    distmetriclb->setSelectable(false);

    QStandardItem *distmetricdt = new QStandardItem(params.getDistanceMetric());
    distmetricdt->setSelectable(false);

    distanceMetricItemList << distmetriclb;
    distanceMetricItemList << distmetricdt;
    ll[0]->appendRow(distanceMetricItemList);

    QList<QStandardItem *> estimatorItemList;
    QStandardItem *estimatorlb = new QStandardItem("Estimator");
    estimatorlb->setSelectable(false);

    QStandardItem *estimatordt = new QStandardItem(params.getStatistic());
    estimatordt->setSelectable(false);


    estimatorItemList << estimatorlb;
    estimatorItemList << estimatordt;
    ll[0]->appendRow(estimatorItemList);

    QList<QStandardItem *> analogiesItemList;
    QStandardItem *analogieslb = new QStandardItem("Analogies");
    analogieslb->setSelectable(false);

    QStandardItem *analogiesdt = new QStandardItem(QString::number(params.getAnalogies()));
    analogiesdt->setSelectable(false);

    analogiesItemList << analogieslb;
    analogiesItemList << analogiesdt;
    ll[0]->appendRow(analogiesItemList);


    QList<QStandardItem *> evalItemList;
    QStandardItem *evallb = new QStandardItem("Evalutation Type");
    evallb->setSelectable(false);

    QStandardItem *evaldt = new QStandardItem(params.getEvaluationMethod());
    evaldt->setSelectable(false);

    evalItemList << evallb;
    evalItemList << evaldt;
    ll[0]->appendRow(evalItemList);


    QVector<QPair<QString, double> > subset = params.subset();
    std::for_each(subset.begin(), subset.end(), [&](const QPair<QString, double> &p){
        QList<QStandardItem *> attributeWeightListItem;
        QStandardItem *weightslb = new QStandardItem(p.first);
        weightslb->setSelectable(false);

        QStandardItem *weightsdt = new QStandardItem(QString::number(p.second));
        weightsdt->setSelectable(false);

        attributeWeightListItem << weightslb;
        attributeWeightListItem << weightsdt;

        ll[0]->appendRow(attributeWeightListItem);
    });


    QStandardItemModel *md = addProjectMatrixModel(params.getAttributeNames(), params.getProjectsMatrix());
    QStandardItemModel *mdd = addEstimationAttrModel(params.getEstimationAttribute(), params.getEstimationAttributeVector());

    methods.append(std::tuple<AnalogyMethodParameters, QStandardItemModel *, QStandardItemModel *>(params, md, mdd));
}

void MethodModel::clearMethods()
{
    methods.clear();
    this->clear();
    this->setHorizontalHeaderLabels(columns);
}


std::tuple<QStandardItemModel *, QStandardItemModel *> MethodModel::getMethodModels(unsigned int id)
{
    if(id < (unsigned int)methods.count())
        return std::tuple<QStandardItemModel *, QStandardItemModel *> (std::get<1>(methods[id]), std::get<2>(methods[id]));
    return std::tuple<QStandardItemModel *, QStandardItemModel *>();
}

AnalogyMethodParameters MethodModel::getMethodParameters(unsigned int index) {
    if(index < methods.count()) {
        return std::get<0>(methods.at(index));
    }

    return AnalogyMethodParameters();
}


QStandardItemModel *MethodModel::addProjectMatrixModel(const QStringList &attrNames, const QVector<QStringList > &projectMatrix) { //must change and follow factory pattern
    QStandardItemModel *md = new QStandardItemModel();
    QStringList newAttrNames = attrNames;
    newAttrNames.prepend("Status");

    newAttrNames.prepend("ID");
    md->setHorizontalHeaderLabels(newAttrNames);
    unsigned int id = 1;

    connect(md, &QStandardItemModel::itemChanged, [&](QStandardItem *it) {
        if(it->model()) {
            //it->model()->blockSignals(true);

            if(it->column() == 1) {
                int row = it->row();
                it->setText(it->checkState() == Qt::Checked ? QString("Active") : QString("Inactive"));
                for(int col = 0; col < it->model()->columnCount(); col++) {
                    it->model()->item(row, col)->setBackground(it->checkState() == Qt::Checked ? QBrush(Qt::white) : QBrush(Qt::gray));
                }
            }
           // it->model()->blockSignals(false);

        }

    });

    std::for_each(projectMatrix.begin(), projectMatrix.end(), [&](const QStringList &rowvec){
        QList<QStandardItem *> projectItem;
        QStandardItem *statusItem = new QStandardItem("Active");
        statusItem->setCheckable(true);
        statusItem->setCheckState(Qt::Checked);

        projectItem << new QStandardItem(QString("Project-") + QString::number(id++));
        projectItem << statusItem;
        std::for_each(rowvec.begin(), rowvec.end(), [&](const QString &value){
            projectItem << new QStandardItem(value);
        });

        md->appendRow(projectItem);
    });

    return md;
}


QStandardItemModel *MethodModel::addEstimationAttrModel(const QString &estimAttr,const QVector<double> &estimAttrVec) { //must change and follow factory pattern
    QStandardItemModel *md = new QStandardItemModel();
    QStringList attrNames(estimAttr);
    attrNames.prepend("ID");
    md->setHorizontalHeaderLabels(attrNames);

    unsigned int id = 1;

    std::for_each(estimAttrVec.begin(), estimAttrVec.end(), [&](double value){
        QList<QStandardItem *> projectItem;
        projectItem.append(new QStandardItem(QString("Project-") + QString::number(id++)));
        projectItem.append(new QStandardItem(QString::number(value)));
        md->appendRow(projectItem);
    });

    return md;
}


MethodItem::MethodItem(const QString &text) : QStandardItem(text)
{
    id = MethodModel::methods.count();
    setToolTip("Press Apply button to apply the method");
}
unsigned int MethodItem::getId() const
{
    return id;
}

