#include <QApplication>
#include <QDesktopServices>
#include <QUrl>
#include "MainMenuBar.hpp"
#include "MainWindow.hpp"


MainMenuBar::MainMenuBar(MainWindow *parent):QMenuBar(parent)
{
    FileMenu = new QMenu("&File" , this);
    FileMenu->addAction(ActionsContainer::instance()->getActionByName("Load"));
    FileMenu->addAction(ActionsContainer::instance()->getActionByName("Save"));
    FileMenu->addSeparator();
    FileMenu->addAction(ActionsContainer::instance()->getActionByName("ImportCSV"));
    FileMenu->addAction(ActionsContainer::instance()->getActionByName("ImportARFF"));

    FileMenu->addSeparator();
    FileMenu->addAction(ActionsContainer::instance()->getActionByName("Exit"));
    FontMenu = new QMenu("F&ont" , this);
    FontMenu->addAction(ActionsContainer::instance()->getActionByName("Font"));

    HelpMenu = new QMenu("&Help" , this);
    HelpMenu->addAction(ActionsContainer::instance()->getActionByName("Contents"));
    HelpMenu->addAction(ActionsContainer::instance()->getActionByName("About"));

    addMenu(FileMenu);
    addMenu(FontMenu);
    addMenu(HelpMenu);
}
