#include <QSqlError>
#include <QSqlQuery>
#include <QSqlResult>
#include <QString>
#include <QSqlField>
#include <QtConcurrent/QtConcurrent>
#include <QCoreApplication>
#include <QtWidgets/QMessageBox>

#include <vector>
#include <map>
#include <limits>

#include "DataBase.hpp"
#include "CommonFunctionsContainer.hpp"
#include "Enums.h"
#include "Timer.hpp"
#include "Exception.hpp"
#include "ThreadManager.hpp"
DataBase *DataBase::instancePtr = nullptr;

DataBase::DataBase(QObject *parent ):QObject(parent)
{
    addConnection();
    updateTimer.setSingleShot(true);
    QObject::connect(&updateTimer, SIGNAL(timeout()), this, SLOT(updateTablesSlot()));
}

void DataBase::addConnection()
{
    QSqlDatabase::database("in_mem_db", false).close();
    QSqlDatabase::removeDatabase("in_mem_db");
    db = QSqlDatabase::addDatabase("QSQLITE", "in_mem_db");
    db.setDatabaseName(":memory:");
    if (!db.open())
        qDebug() << "Unable to open database";
    query = std::make_unique<QSqlQuery>("", db);
}

QSqlDatabase DataBase::getDb() const
{
    return db;
}

void DataBase::addMethod(const AnalogyMethodParameters &analogyParams)
{
    DBMethods.push_back(analogyParams);
}

QString DataBase::getAttributeDescriptionById(unsigned int id) const
{
	QSqlQuery qr(db);
	qr.exec("select Description from Attributes where OMEN_ID=" + QString::number(id));
	qr.first();
	return qr.value(0).toString();
}

QString DataBase::getProjectDescriptionById(unsigned int id) const
{
    QSqlQuery qr(db);
    qr.exec("select Description from Projects where OMEN_ID=" + QString::number(id));
    qr.first();
    return qr.value(0).toString();
}

QString DataBase::getTargetDescriptionById(unsigned int id) const
{
    QSqlQuery qr(db);
    qr.exec("select Description from Targets where OMEN_ID=" + QString::number(id));
    qr.first();
    return qr.value(0).toString();
}

void DataBase::setAttributeStatus(unsigned int index, int status)
{
    if (index <= DBAttributes.size()) {
		DBAttributes[index].setStatus(status);
        QString query = QString("UPDATE Attributes SET Status=") + QString::number(status) + " WHERE OMEN_ID=" + QString::number(index) + ";";
		exec(query.toStdString());
	}
}

void DataBase::setProjectStatus(unsigned int index, int status)
{
    if (index <= DBProjects.size()) {
		DBProjects[index].setStatus(status);
        QString query = QString("UPDATE Projects SET Status=") + QString::number(status) + " WHERE OMEN_ID=" + QString::number(index) + ";";
		exec(query.toStdString());
	}
}

void DataBase::setTargetStatus(unsigned int index, int status)
{
    if (index <= DBTargets.size()) {
		DBTargets[index].setStatus(status);
        QString query = QString("UPDATE Targets SET Status=") + QString::number(status) + " WHERE OMEN_ID=" + QString::number(index) + ";";
        exec(query.toStdString());
	}
}

DataBase *DataBase::instance()
{
    if(!instancePtr)
        instancePtr = new DataBase;

    return instancePtr;
}

void DataBase::updateTables()
{
    if(QThread::currentThread() == QCoreApplication::instance()->thread())
        updateTimer.start(10);
}

void DataBase::clearQuery()
{
    query->finish();
    query->clear();
}

void DataBase::updateTablesSlot()
{
    emit updateTablesSignal();
}

void DataBase::exec(const std::string &querystr)
{
    if(querystr.empty())
        return;
    QSqlQuery qr(db);
        qr.exec(querystr.c_str());
        //qr.exec();
        if(qr.lastError().text() != " ")
            std::cout << querystr << std::endl << "Error ----> " << qr.lastError().text().toUtf8().constData() << std::endl;
        updateTables();
        qr.finish();
}

void DataBase::setDBAttributes(const std::vector<Attribute> &DBAttrs)
{
    //attributesModel->reset();
    emit resetAttributesModelSignal();
    this->DBAttributes = DBAttrs;
    QStringList atrrNamesList;
	auto insertAttributeRecordToDatabase = [&](Attribute &attr) {
        exec(attr.sqlQuery().c_str());
        atrrNamesList << attr.getDescription().c_str();
	};


    auto it = std::find_if(DBAttributes.begin(), DBAttributes.end(), [&](const Attribute &attr) -> bool {
        return !attr.validate();
    });

    db.transaction();
    std::for_each(DBAttributes.begin(), DBAttributes.end(), insertAttributeRecordToDatabase);
    db.commit();
    emit attributesInserted();
}

void DataBase::setDBProjects(const std::vector<Project> &DBPrjs)
{
    emit resetProjectsModelSignal();
    this->DBProjects = DBPrjs;
    auto insertProjectRecordToDatabase = [&](const Project &prj) {
        exec(prj.sqlQuery(Project::AS_PROJECT).c_str());
    };

    db.transaction();
    std::for_each(DBProjects.begin(), DBProjects.end(), insertProjectRecordToDatabase);
    db.commit();
    emit projectsInserted();
}

void DataBase::setDBTargets(const std::vector<Project> &DBTargets)
{   
    emit resetTargetsModelSignal();
    this->DBTargets = DBTargets;
    auto insertTargetRecordToDatabase = [&](const Project &trg) {
        exec(trg.sqlQuery(Project::AS_TARGET).c_str());
    };

    db.transaction();
    std::for_each(DBTargets.begin(), DBTargets.end(), insertTargetRecordToDatabase);
    db.commit();
}

void DataBase::addDBTarget(const Project &trg)
{
    this->DBTargets.push_back(trg);
    db.transaction();
    exec(trg.sqlQuery(Project::AS_TARGET).c_str());

    db.commit();
}

void DataBase::addDBTargets(const std::vector<Project> &trgs)
{
    db.transaction();

    std::for_each(trgs.begin(), trgs.end(), [&](const Project &trg){
        exec(trg.sqlQuery(Project::AS_TARGET).c_str());
    });
    std::copy(trgs.begin(), trgs.end(), std::back_inserter(this->DBTargets));

    db.commit();
}


const std::vector<Attribute> &DataBase::getDBAttributes() const
{
    return DBAttributes;
}

std::vector<std::string>  DataBase::getDBAttributesNames()
{
    std::vector<std::string> attrNames;
    for(int counter = 0; counter < DBAttributes.size(); counter++){
        if(DBAttributes[counter].getStatus() == 1)
            attrNames.push_back(DBAttributes[counter].getDescription());
    }
    return attrNames;
}

int DataBase::getActiveProjectsCount()
{
    QSqlQuery qr(db);
    qr.prepare("select Description from projects where Status=1");
    qr.exec();
    qr.last();

    return qr.at() + 1;
}

int DataBase::getAttributeType(const std::string &attrname) const
{

    foreach (const Attribute &attr, DBAttributes) {
        if(attr.getDescription() == attrname)
            return attr.getType();
    }

    return WRONGTYPE;
}

QStringList DataBase::getProjectValuesByDescription(const QString &desc, const QStringList &attrs) {
    int index = -1;
    bool validValue = false;
    QStringList attrsVals;
    QVector<int> attrsIds;
    QSqlQuery qr(db);

    QString selectQuery = "select * from Projects where Description='" + desc + "'";

    qr.exec(selectQuery);

    qr.last();
    int qrat = qr.at();
    if (qr.at() + 1 <= 0) {
        return attrsVals;
    }

    qr.first();

    QSqlRecord rec = qr.record();
    foreach (const QString &it, attrs) {
        index = rec.indexOf(it);
        if(index == -1) {
            throw InvalidProjectValue();
        }
        attrsIds << rec.indexOf(it);
    }


    attrsVals.clear();
    std::for_each (attrsIds.begin(), attrsIds.end(), [&] (int index) {
        validateValue(qr.value(1).toString(), "", qr.value(index).toString());
        attrsVals << qr.value(index).toString();
    });

    return attrsVals;
}

const std::vector<Project> &DataBase::getDBProjects() const
{
    return DBProjects;
}

const std::vector<Project> &DataBase::getDBTargets() const
{
    return DBTargets;
}

void DataBase::clear()
{
    this->DBProjects.clear();
    this->DBTargets.clear();
    this->setDBAttributes(std::vector<Attribute>());
    //this->DBAttributes.clear();

    emit resetAttributesModelSignal();
    emit resetProjectsModelSignal();
    emit resetTargetsModelSignal();

}

void DataBase::createIndexMatrix(const QVector<QVector<double>> &matrix, QVector<QPair<int, QVector<double> > > &indexMatrix)
{
    indexMatrix.reserve(matrix.count());
    int index = 0;
    std::for_each(matrix.begin(), matrix.end(), [&](const QVector<double> &row) {
        indexMatrix.push_back(QPair<int, QVector<double> >(index, row));
        index++;
    });
}

void DataBase::convertSubsetWeightsToWeights(const QVector<QPair<QString, double> > &subsetWeights, QVector<double> &weights)
{
    weights.reserve(subsetWeights.count());
    std::for_each(subsetWeights.begin(), subsetWeights.end(), [&](const QPair<QString, double> &weight) {
        weights.push_back(weight.second);
    } );
}

void DataBase::standardizeMatrix(const QVector<QVector<double>> &matrix, const QList<int> &attrtypes, QVector<QVector<double>> &stdMatrix)
{
    QVector<double> minVec,maxVec;
    bool validvalue = false;
    getMinMaxVectors(matrix, minVec, maxVec);

    stdMatrix.reserve(matrix.count());

    std::for_each(matrix.begin(), matrix.end(), [&](const QVector<double> &vec){
        QVector<double> newVec;
        newVec.reserve(vec.size());

        for(int index2 = 0; index2 < vec.count(); index2++) {
            double val = vec[index2];
            if(attrtypes[index2] == CATEGORICAL) {
                if(val >= 0)
                    throw InvalidProjectValue();

                newVec.push_back(val);
            } else newVec.push_back((val - minVec[index2])/(maxVec[index2] - minVec[index2]));
        }

        stdMatrix.push_back(newVec);
    });
}

QList<int> DataBase::getTypeOfAttributes(const QStringList &attrNames) const
{
    QList<int> ret;

    std::for_each(attrNames.begin(), attrNames.end(), [&](const QString &attrname){
       ret.append(this->getAttributeType(attrname.toStdString()));
    });

    return ret;
}


void DataBase::validateCategoricalAttributes(const QStringList &attrs) const
{
    QSqlQuery qr(db);
    qr.exec("select Description from Attributes where Type=2");
    qr.first();

    do {
        QSqlQuery qr1(db);
        qr1.exec("select " + qr.value(0).toString() + " from Projects;");
        QSet<QString> ss;
        while(qr1.next()) {
            bool ok = false;
            int num = qr1.value(0).toString().toInt(&ok);
            if(!ok || (ok && num >= 0))
                throw InvalidProjectValue("",qr1.value(0).toString().toStdString());
            ss.insert(qr1.value(0).toString());
        }
    } while(qr.next());
}

const QStringList &DataBase::getActiveAttributes() const
{
    QSqlQuery qr(db);
    qr.prepare("select Description from Attributes where Status=1");
    qr.exec();
    qr.last();
    activeAttrs.clear();
    qDebug() << "qr at:" << qr.at();
    activeAttrs.reserve(qr.at() + 2);
    qr.first();
    do {
        activeAttrs.push_back(qr.value(0).toString());
    } while(qr.next());

    return activeAttrs;
}


inline double validateValue(const QString &prjDesc, const QString &attr, const QString &value)
{
    bool validValue = true;

    if(value.isNull() || value.isEmpty()) {
        throw InvalidProjectValue(prjDesc.toStdString(), attr.toStdString(), value.toStdString());
        return std::numeric_limits<double>::quiet_NaN();
    }


    double dblv = value.toDouble(&validValue);

    if(!validValue) {
        dblv = -hash(value.toStdString());
    }

    return dblv;
}

void DataBase::getActiveAttributesVector(const QStringList &attrs, QVector<QPair<int,QStringList>> &prjsAttrVals, Project::Type type)
{
    int index = -1;
    bool validValue = false;
    QStringList attrsVals;
    QVector<int> attrsIds;
    QSqlQuery qr(db);

    QString selectQuery = type == Project::AS_PROJECT ? "select * from Projects where Status=1" : "select * from Targets where Status=1";

    qr.exec(selectQuery);

    qr.last();
	int qrat = qr.at();
	if (qr.at() + 1 <= 0) {
		return;
	}
    prjsAttrVals.reserve(qr.at() + 1);

    qr.first();

    QSqlRecord rec = qr.record();
    foreach (const QString &it, attrs) {
        index = rec.indexOf(it);
        if(index == -1) {
            throw InvalidProjectValue();
        }
        attrsIds << rec.indexOf(it);
    }
	
    do {
        validValue = false;
        attrsVals.clear();
        std::for_each (attrsIds.begin(), attrsIds.end(), [&] (int index) {
            validateValue(qr.value(1).toString(), "", qr.value(index).toString());
            attrsVals << qr.value(index).toString();
        });

        prjsAttrVals.push_back(QPair<int, QStringList>(qr.value(0).toInt(), attrsVals));
    } while(qr.next());

    //TODO CHECK IF attrs size is equal to prjsAttrVals size otherwise
}

QStringList DataBase::getActiveAttributeVector(const QString &attr)
{
    QStringList vals;
    QSqlQuery qr(db);
    qr.exec("select * from Projects where Status=1");

    int index = qr.record().indexOf(attr);

    while (qr.next()) vals << qr.value(index).toString();

    return vals;
}

QStringList DataBase::getActiveProjectsVector() const
{
    QString selectQuery = "select * from Projects where Status=1";
    QStringList activeProjects;

    QSqlQuery qr(db);
    qr.exec(selectQuery);

    while (qr.next()) activeProjects << qr.value(1).toString();


    return activeProjects;
}

bool DataBase::hasAttribute(const QString &attr)
{
    QString query = "SELECT * from Attributes where Description='" + attr +"';";

    QSqlQuery qr(db);
    qr.exec(query);

    if(qr.first())
        return true;
    return false;
}


bool DataBase::hasAttributes(const QStringList &attrs)
{
    return (std::find_if(attrs.begin(), attrs.end(), [&](const QString &name) -> bool {
        return !hasAttribute(name);
    }) != attrs.end());
}

void DataBase::copyProjectsToTargets(const QModelIndexList &prjIds) {
    std::for_each(prjIds.begin(), prjIds.end(), [&](const QModelIndex &index) {
        if(index.column() == 0)
           this->addDBTarget(Project(this->DBTargets.size() + 1, this->DBProjects[index.row()].getDescription(), 1, this->DBProjects[index.row()].getAttributes()));
    });

}

void DataBase::activateAttributes(const QModelIndexList &attrIds, bool active)
{
    std::for_each(attrIds.begin(), attrIds.end(), [&](const QModelIndex &index) {
        if(index.column() == 0)
            this->setAttributeStatus(index.data().toUInt(), active);
    });
}


void DataBase::activateProjects(const QModelIndexList &prjIds, bool active)
{
    std::for_each(prjIds.begin(), prjIds.end(), [&](const QModelIndex &index) {
        if(index.column() == 0)
            this->setProjectStatus(index.data().toUInt(), active);
    });
}

void DataBase::activateTargets(const QModelIndexList &prjIds, bool active)
{
    std::for_each(prjIds.begin(), prjIds.end(), [&](const QModelIndex &index) {
        if(index.column() == 0)
            this->setTargetStatus(index.data().toUInt(), active);
    });
}

bool DataBase::hasAttributes()
{
    QString query = "SELECT Description from Attributes";
    QSqlQuery qr(db);
    qr.exec(query);

    if(qr.first())
        return true;
    return false;
}
