#include "Attribute.hpp"
#include "DataBase.hpp"
#include "Exception.hpp"

static std::string sanitizeAttributeName(const std::string &attrname)
{
    QString name(attrname.c_str());
    name = name.replace(' ', '_').replace('.',"_");
    return name.toStdString();
}

Attribute::Attribute(int Nr, const std::string &Description, int Type, int Status, const std::string &Value, double WeightFactor)
{

    this->Nr = Nr;
    if(!Description.empty())
        this->Description = sanitizeAttributeName(Description);
    else  this->Description = sanitizeAttributeName(std::string("ATTR_") + QString::number(Nr).toStdString());
    this->Description.erase(std::remove(this->Description.begin(), this->Description.end(), '"'), this->Description.end());
    this->Description.erase(std::remove(this->Description.begin(), this->Description.end(), '\''), this->Description.end());

    this->Type = Type;
    this->Status = Status;
    this->WeightFactor = WeightFactor;
    this->Value = Value.c_str();
}

int Attribute::getNr() const
{
    return Nr;
}

std::string Attribute::getDescription() const
{
    return Description;
}

int Attribute::getType() const
{
    return Type;
}

int Attribute::getStatus() const
{
    return Status;
}

double Attribute::getWeightFactor() const
{
    return WeightFactor;
}

QString Attribute::getValue() const
{
    return Value.toString();
}

void Attribute::setType(int Type)
{
    this->Type = Type;
}

void Attribute::setWeightFactor(double WeightFactor)
{
    this->WeightFactor = WeightFactor;
}

void Attribute::setValue(const QString &Value)
{
    this->Value = Value;
}

void Attribute::setStatus(int status)
{
	Status = status;
}

std::string Attribute::sqlQuery() const
{
    std::string query = "insert into Attributes values (";
	query += std::to_string(Nr);
	query += ",";
    query += "'" + Description + "'";
	query += ",";
	query += std::to_string(Status);
	query += ",";
	query += std::to_string(Type);
	query += ")";
    return query;
}

bool Attribute::validate() const
{
    bool isDescriptionNumber = false;

    QString::fromStdString(Description).toDouble(&isDescriptionNumber);

    if(isDescriptionNumber) {
        throw InvalidAttributeName();
        return false;
    }

    return true;
}
