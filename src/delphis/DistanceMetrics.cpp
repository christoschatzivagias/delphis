#include <vector>
#include <map>
#include <cstdlib>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <QCoreApplication>
#include "DistanceMetrics.hpp"
#include "CommonFunctionsContainer.hpp"


DistanceMetricsContainer *DistanceMetricsContainer::instancePtr = nullptr;


const std::vector<const Distance *> &DistanceMetricsContainer::getCurrentDistances() const
{
    return CurrentDistances;
}

const Distance *DistanceMetricsContainer::getDistanceMetricByName(const QString &name) const
{
    auto itret = find_if(Distances.cbegin(), Distances.cend(), [&] (const Distance *dist) {
            return name == dist->getName().c_str();
    });
    return *itret;
}

DistanceMetricsContainer *DistanceMetricsContainer::instance()
{
    if(!instancePtr)
        instancePtr = new DistanceMetricsContainer;

    return instancePtr;
}

DistanceMetricsContainer::DistanceMetricsContainer(const QStringList &distMetics) : QObject(QCoreApplication::instance())
{

    Distances = {
        new Euclidean(this),
        new Manhattan(this),
        new Canberra(this),
        new Chebychev(this),
        new Czekanowski(this),
        new Minkowski(this),
        new KaufmanRousseeuw(this)
    };

    auto checkAndAddDistanceMetric = [&] (const QString &name) {
        auto insertDistanceWithName = [&] (const Distance *dst) {
            if(name.toStdString() == dst->getName())
                CurrentDistances.push_back(dst);
        };

        std::for_each(Distances.begin(), Distances.end(), insertDistanceWithName);
    };

	std::for_each(distMetics.begin(), distMetics.end(), checkAndAddDistanceMetric);
}



Euclidean::Euclidean(QObject *parent) : Distance(parent)
{
    name = "Euclidean";
    typeId = DistanceMetricsContainer::EUCLIDEAN;
}

double Euclidean::calculate(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights) const
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    double v1 = 0, v2 = 0;

    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        v1 = point1[counter1];
        v2 = point2[counter1];
        if(v1 < 0 && v2 < 0) {
            if(v1 != v2) sum += weights[counter1];
        } else sum += weights[counter1]*pow(v1 - v2, 2);
    }

    return sqrt(sum);
}

Manhattan::Manhattan(QObject *parent) : Distance(parent)
{
    name = "Manhattan";
    typeId = DistanceMetricsContainer::MANHATTAN;
}

double Manhattan::calculate(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights) const
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    double v1 = 0, v2 = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        v1 = point1[counter1];
        v2 = point2[counter1];
        if(v1 < 0 && v2 < 0) {
            if(v1 != v2) sum += weights[counter1];
        } else sum += weights[counter1]*fabs(v1 - v2);
    }
    return sum;
}

Canberra::Canberra(QObject *parent) : Distance(parent)
{
    name = "Canberra";
    typeId = DistanceMetricsContainer::CANBERRA;
}

double Canberra::calculate(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights) const
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    double v1 = 0, v2 = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        v1 = point1[counter1];
        v2 = point2[counter1];
        if(v1 < 0 && v2 < 0) {
            if(v1 != v2) sum += weights[counter1];
        } else sum += weights[counter1]*fabs(v1 - v2)/(fabs(v1) + fabs(v2));
    }

    return sum;
}

Chebychev::Chebychev(QObject *parent) : Distance(parent)
{
    name = "Chebychev";
    typeId = DistanceMetricsContainer::CHEBYCHEV;
}

double Chebychev::calculate(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights) const
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    double temp = 0;
    double v1 = 0, v2 = 0;

    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        v1 = point1[counter1];
        v2 = point2[counter1];
        if(v1 < 0 && v2 < 0) {
            if(v1 != v2) sum += weights[counter1];
        } else {
            temp = weights[counter1]*fabs(v1 - v2);
            if(temp > sum)
                sum = temp;
        }
    }

    return sum;
}

Czekanowski::Czekanowski(QObject *parent) : Distance(parent)
{
    name = "Czekanowski";
    typeId = DistanceMetricsContainer::CZEKANOWSKI;
}

double Czekanowski::calculate(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights) const
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum1 = 0, sum2 = 0;
    double v1 = 0, v2 = 0;

    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        v1 = point1[counter1];
        v2 = point2[counter1];
        if(v1 < 0 && v2 < 0) {
            if(v1 != v2) {
                v1 = 1;
                v2 = 0;
            } else {
                v1 = 1;
                v2 = 1;
            }
        }
        if(v1 > v2)
            sum1 += weights[counter1]*v2;
        else sum1 += weights[counter1]*v1;
        sum2 += (v2 + v1);
    }

    return 1 - ((2*sum1)/sum2);
}

Minkowski::Minkowski(QObject *parent, int lamda) : Distance(parent)
{
    name = "Minkowski";
    typeId = DistanceMetricsContainer::MINKOWSKI;
    this->lamda = lamda;
}

double Minkowski::calculate(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights) const
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    double v1 = 0, v2 = 0;

    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        v1 = point1[counter1];
        v2 = point2[counter1];
        if(v1 < 0 && v2 < 0) {
            if(v1 != v2) sum += weights[counter1];
        } else sum += weights[counter1]*pow(fabs(v1 - v2), lamda);
    }

    return pow(sum, 1/(double)lamda);
}

KaufmanRousseeuw::KaufmanRousseeuw(QObject *parent) : Distance(parent)
{
    name = "Kaufmanrousseeuw";
    typeId = DistanceMetricsContainer::KAUFMANROUSSEEUW;
}

double KaufmanRousseeuw::calculate(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights) const
{
    return 0;
#if 0
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum1 = 0, sum2 = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum1 += weights[counter1]*fabs(point1[counter1] - point2[counter1])/ranges[counter1];
    }

    return sum1/ranges.size();
#endif
}

std::string Distance::getName() const
{
    return name;
}

bool Distance::getActive() const
{
    return active;
}

void Distance::setActive(bool value)
{
    active = value;
}
