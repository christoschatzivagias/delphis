#include <QCoreApplication>
#include <QFutureWatcher>
#include <QTimer>

#include "ThreadManager.hpp"


ThreadManager *ThreadManager::instancePtr = nullptr;

ThreadManager::ThreadManager() : QObject(QCoreApplication::instance())
{
	//connect(&futureWatcher, &QFutureWatcher<void>::progressValueChanged, this, &ThreadManager::progressValueChanged, Qt::DirectConnection);
}

ThreadManager *ThreadManager::instance()
{
	if (!instancePtr)
		instancePtr = new ThreadManager;
	return instancePtr;
}

void ThreadManager::process(const QFuture<void> &ft, const QString &str)
{
	emit progressValueChanged(0);

	futureWatcher.setFuture(ft);
	int modulo = ft.progressMaximum() / 100.0f + 1;

	while (!futureWatcher.isFinished()) {
		if(futureWatcher.progressValue() % modulo == 0) {
			emit progressValueChanged(100*futureWatcher.progressValue()/(double)ft.progressMaximum());
            emit messageChanged(str);
		}
		QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
	}

	emit progressValueChanged(100);

    QTimer::singleShot(1000, [&](){
        emit resetBusy();
        emit progressValueChanged(0);
        emit messageChanged(QString());
    });
}

