#include <QStringList>
#include <QString>
#include <QFile>

#include <QtConcurrent>
#include "Project.hpp"
#include "AsciiFileManager.hpp"
#include "Enums.h"
#include "DataBase.hpp"
#include "ThreadManager.hpp"

#include "CommonFunctionsContainer.hpp"
#include "Exception.hpp"
#include "arff_parser.h"
#include "CSVReader.h"

#include <fstream>

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <fstream>




AsciiFileManager::AsciiFileManager()
{
}

void AsciiFileManager::readAttributes(const std::string &DataFileName)
{
    QStringList ls;
    int type = 0;
    int status = 0;
    unsigned int AttrCounter = 0;
    unsigned int counter1 = 0;
    AscDBAttributes.clear();

    QFile file(DataFileName.c_str());

    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream textStream(&file);
        QStringList lines = textStream.readAll().split('\n');
        QStringList::iterator it;
        for(it = lines.begin(); QString::compare(*it, "[params]", Qt::CaseInsensitive); ++it);
        ++it;

        AttrCounter =  (*it).section('=', 1).toUInt();
        ++it;

        AscDBAttributes.reserve(AttrCounter);
        ThreadManager::instance()->setBusy();
        ThreadManager::instance()->process(QtConcurrent::run([&]() {

            std::for_each(it, it + AttrCounter, [&](const QString &line) {
                ls = line.section('=', 1).split(',');

                if(ls.count() < 2) {
                    file.close();
                    throw InvalidIniFileException();
                    return;
                }

                if(!QString::compare(ls[1].toUtf8().constData(), "NUMERIC", Qt::CaseInsensitive))
                    type = NUMERICAL;
                else if(!QString::compare(ls[1].toUtf8().constData(), "CATEGORICAL", Qt::CaseInsensitive))
                    type = CATEGORICAL;

                status = 1;

                if(ls.count() > 2 && !QString::compare(ls[2].toUtf8().constData(), "OFF", Qt::CaseInsensitive))
                    status = 0;

                AscDBAttributes.push_back(Attribute(counter1++ + 1, ls[0].toUtf8().constData(), type, status));
            });
        }), "Reading Attributes...");

        file.close();
    } else throw InvalidIniFileException();

}

void AsciiFileManager::readProjects(const std::string &DataFileName)
{
    size_t AttrCounter = 0;
    unsigned int PrjCounter = 0;
    AscDBProjects.clear();
    QFile file(DataFileName.c_str());


    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream textStream(&file);
        QStringList lines = textStream.readAll().split('\n');
        QStringList::iterator it;
        for(it = lines.begin(); QString::compare(*it, "[projects]", Qt::CaseInsensitive); ++it);
        ++it;

        PrjCounter = (*it).section('=',1).toInt();
        ++it;

        AscDBProjects.resize(PrjCounter);
        QVector<QPair<int, QString> > indexedlines;
        indexedlines.reserve(PrjCounter);

        for(int index = 0; index < PrjCounter; index++) {
            indexedlines.push_back(QPair<int, QString> (index, *(it + index)));
        }

        ThreadManager::instance()->process(QtConcurrent::map(indexedlines.begin(), indexedlines.end(), [&](const QPair<int,QString> &indexedLine) {
            std::vector<Attribute> AscDBAttrs = AscDBAttributes;

            QStringList ls;
            int status = 1;

            ls = convertCommaInsideApostrophs(indexedLine.second.section('=', 1)).split(',');

            AttrCounter = AscDBAttrs.size();
            if(ls.count() < AttrCounter + 1 || ls.count() > AttrCounter + 2) {
                file.close();
                throw InvalidIniFileException();
                return;
            }

            for(int counter2 = 0; counter2 < AttrCounter; counter2++ )
                AscDBAttrs[counter2].setValue(ls[counter2 + 1].replace("&COMMA&", ","));

            if(ls.count() == AttrCounter + 2 && !QString::compare(ls[AttrCounter + 1], "OFF", Qt::CaseInsensitive))
                status = 0;
            ThreadManager::instance()->mutex.lock();
            AscDBProjects.insert(AscDBProjects.begin() + indexedLine.first, Project(indexedLine.first + 1, ls[0].toStdString(), status, AscDBAttrs, Project::AS_PROJECT));
            ThreadManager::instance()->mutex.unlock();
        }), "Reading Projects...");

        file.close();
    } else throw InvalidIniFileException();
}

void AsciiFileManager::readTargets(const std::string &DataFileName)
{
    size_t AttrCounter = 0;
    unsigned int TrgCounter = 0;
    AscDBTargets.clear();
    QFile file(DataFileName.c_str());

    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream textStream(&file);
        QStringList lines = textStream.readAll().split('\n');
        QStringList::iterator it;
        for(it = lines.begin(); QString::compare(*it, "[targets]", Qt::CaseInsensitive); ++it);
        ++it;

        TrgCounter = (*it).section('=',1).toInt();
        ++it;

        AscDBTargets.resize(TrgCounter);


        QVector<QPair<int, QString> > indexedlines;
        indexedlines.reserve(TrgCounter);

        for(int index = 0; index < TrgCounter; index++) {
            indexedlines.push_back(QPair<int, QString> (index, *(it + index)));
        }

        ThreadManager::instance()->process(QtConcurrent::map(indexedlines.begin(), indexedlines.end(), [&](const QPair<int,QString> &indexedLine) {
            std::vector<Attribute> AscDBAttrs = AscDBAttributes;
            QStringList ls;
            int status = 1;

            ls = convertCommaInsideApostrophs(indexedLine.second.section('=', 1)).split(',');

            AttrCounter = AscDBAttrs.size();

            if(ls.count() < AttrCounter + 1 || ls.count() > AttrCounter + 2) {
                file.close();
                throw InvalidIniFileException();
                return;
            }

            for(unsigned int counter2 = 0; counter2 < AttrCounter; counter2++ )
                AscDBAttrs[counter2].setValue(ls[counter2 + 1].replace("&COMMA&", ","));

            if(ls.count() == AttrCounter + 2 && !QString::compare(ls[AttrCounter + 1], "OFF", Qt::CaseInsensitive))
                status = 0;
            ThreadManager::instance()->mutex.lock();
            AscDBTargets.insert(AscDBTargets.begin() + indexedLine.first, Project(indexedLine.first + 1, ls[0].toStdString(), status, AscDBAttrs, Project::AS_TARGET));
            ThreadManager::instance()->mutex.unlock();
       }), "Reading Targets...");


        file.close();

    } else throw InvalidIniFileException();
}

void AsciiFileManager::readAsciiFile(const std::string &File)
{
    try {
        readAttributes(File);
        readProjects(File);
        readTargets(File);
    } catch(std::exception &e) {
        handleException(e);
    }
}

const std::vector<Attribute> &AsciiFileManager::getDBAttributes()
{
    return AscDBAttributes;
}

const std::vector<Project> &AsciiFileManager::getDBProjects()
{
    return AscDBProjects;
}

const std::vector<Project> &AsciiFileManager::getDBTargets()
{
    return AscDBTargets;
}

void AsciiFileManager::tmpArffFileWithoutComments(const std::string &file) const
{
    QFile tmpFile(QString(file.c_str()) + "XXXXX");
    QFile arffFile(file.c_str());
    if (!arffFile.open(QIODevice::ReadOnly)) {
        handleException(std::exception(),true, std::string("Cannot open file ") + file);
        return;
    }

    if (!tmpFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        handleException(std::exception(),true, std::string("Cannot open file ") + file);
        return;
    }

    QTextStream tmpArffFileStream(&tmpFile);
    QTextStream arffFileStream(&arffFile);

    while (!arffFileStream.atEnd()) {
        QString line = arffFileStream.readLine();
        if ( line.contains("%") ) {
            if ( !line.startsWith("%") ) {
                QStringList ll = line.split("%");
                if (ll.count() >= 2) {
                    line = ll[0];
                }
            } else line = "";
        }
        if(!line.isEmpty())
            tmpArffFileStream << line << "\n";
        QStringList fields = line.split(",");
    }

    arffFile.close();
    tmpFile.close();


}

void AsciiFileManager::readArffFile(const std::string &file)
{
    std::vector<Attribute> AttrsPrjTmp;
    ArffAttr *tmpattr = nullptr;
    ArffInstance *tmpprj = nullptr;
    ArffValue *tmpvalue = nullptr;
    QString tempStr;

    tmpArffFileWithoutComments(file);

    ArffParser arffparser(file + "XXXXX");
    ArffData* data = nullptr;
    try {
        data = arffparser.parse();
    } catch (std::exception &e) {
        handleException(e, true, " Unable to parse file " + file);
		return;
    }

    QFile::remove(QString(file.c_str()) + "XXXXX");

    int32 attrnum = data->num_attributes();
    int32 prjnum = data->num_instances();
    int type = 0;
    bool isNumerical = false;
    bool isCategorical = false;
    AscDBAttributes.clear();
    AscDBProjects.clear();
    AscDBTargets.clear();

    for(int counter1 = 0; counter1 < attrnum; counter1++) {
        tmpattr = data->get_attr(counter1);

        isNumerical = (tmpattr->type() == INTEGER ||
                       tmpattr->type() == ARFF_FLOAT ||
                       tmpattr->type() == NUMERIC);

        isCategorical = (tmpattr->type() == STRING ||
                         tmpattr->type() == NOMINAL);



        if(isNumerical) {
            type = NUMERICAL;
        } else if(isCategorical) {
            type = CATEGORICAL;
        }

        AscDBAttributes.push_back(Attribute(counter1 + 1, tmpattr->name(), type, 1));
    }


    for(int counter1 = 0; counter1 < prjnum; counter1++) {
        AttrsPrjTmp.clear();
        tmpprj = data->get_instance(counter1);
        attrnum = tmpprj->size();
        tempStr = "Project " + QString().setNum(counter1 + 1);
        AttrsPrjTmp.reserve(attrnum);

        for(int counter2 = 0; counter2 < attrnum; counter2++) {
            tmpvalue = tmpprj->get(counter2);
            QString qstr;
            switch(tmpvalue->type())
            {
            case INTEGER:
            case ARFF_FLOAT:
                qstr.setNum(tmpvalue->getFloat());
                break;
            case STRING:
            case NUMERIC:
            case NOMINAL:
                qstr = tmpvalue->getString().c_str();
                break;
            }
            AttrsPrjTmp.push_back(Attribute(AscDBAttributes[counter2].getNr(), AscDBAttributes[counter2].getDescription(), AscDBAttributes[counter2].getType(), AscDBAttributes[counter2].getStatus(), qstr.toStdString()));
        }

        AscDBProjects.push_back(Project(counter1 + 1, tempStr.toStdString(), 1, AttrsPrjTmp));
    }
}

void AsciiFileManager::readCSVFile(const std::string &file)
{
    CSVReader csvreader;
    QVector<QStringList> lines;
    AscDBAttributes.clear();
    AscDBProjects.clear();
    AscDBTargets.clear();

    csvreader.ReadCSV(file.c_str(), lines);
    try {
        if(lines.count() == 0)
            throw CorruptedCSVException();
    } catch(CorruptedCSVException& e) {
        handleException(e);
        return;
    }
    QStringList first = lines[0];
    AscDBAttributes.reserve(first.count());
    std::vector<enum AttributeType> attribute_types_list(first.count(), NUMERICAL);

    long int attribute_index = 0;
    long int project_index = 0;

    auto checkProjectsAttributesType = [&](const QStringList &line) {
        bool IsNumeric = true;
        long int attribute_index_c = 0;
        foreach (const QString &attrvalue, line) {
            IsNumeric = true;
            attrvalue.toDouble(&IsNumeric);

            ThreadManager::instance()->mutex.lock();
            if(!IsNumeric && !attrvalue.isEmpty()) {
                attribute_types_list[attribute_index_c] = CATEGORICAL;
            }
            ThreadManager::instance()->mutex.unlock();

            attribute_index_c++;

        }
    };

    lines.removeFirst();
    lines.removeLast();
    ThreadManager::instance()->process(QtConcurrent::map(lines.begin(), lines.end(), checkProjectsAttributesType), "Checking Projet Attributes...");

    attribute_index = 0;
    auto addAttribute = [&](QString &attrname) {
        AscDBAttributes.push_back(Attribute(attribute_index + 1, attrname.toUtf8().constData(), attribute_types_list[attribute_index]));
        attribute_index++;
    };

    std::for_each(first.begin(), first.end(), addAttribute);

    AscDBProjects.reserve(lines.count());

    project_index = 0;

    auto readProject = [&](const QStringList &line) {
        std::vector<Attribute> attribute_list_copy;// = attribute_list;
        attribute_list_copy.reserve(AscDBAttributes.size());


        long int attribute_index_c = 0;

        try {
            if(line.count() > AscDBAttributes.size())
                throw BadCSVLineException();
        } catch(BadCSVLineException& e) {
            handleException(e);
            return;
        }

        std::for_each(line.begin(), line.end(), [&] (const QString &attrvalue) {
            attribute_list_copy.push_back(Attribute(AscDBAttributes[attribute_index_c].getNr(),
                                                    AscDBAttributes[attribute_index_c].getDescription(),
                                                    AscDBAttributes[attribute_index_c].getType(),
                                                    AscDBAttributes[attribute_index_c].getStatus(),
                                                    attrvalue.toUtf8().constData()));
            attribute_index_c++;
        });

        ThreadManager::instance()->mutex.lock();
        AscDBProjects.push_back(Project(project_index + 1,
                                       "Project-" + std::to_string(project_index),
                                       1,
                                       attribute_list_copy, Project::AS_PROJECT));
        project_index++;
        ThreadManager::instance()->mutex.unlock();
    };


    ThreadManager::instance()->process(QtConcurrent::map(lines.begin(), lines.end(), readProject), "Reading Projects...");
}

void AsciiFileManager::importCSVTargets(const std::string &file)
{
    CSVReader csvreader;
    QVector<QStringList> lines;
    AscDBAttributes.clear();
    AscDBProjects.clear();
    AscDBTargets.clear();
    csvreader.ReadCSV(file.c_str(), lines);

    QStringList first = lines[0];
    AscDBAttributes.reserve(first.count());
    std::vector<enum AttributeType> attribute_types_list(first.count(), NUMERICAL);

    long int attribute_index = 0;
    long int project_index = 0;

    auto checkProjectsAttributesType = [&](QStringList &line) {
        bool IsNumeric = true;
        long int attribute_index_c = 0;
        foreach (const QString &attrvalue, line) {
            IsNumeric = true;
            attrvalue.toDouble(&IsNumeric);

            ThreadManager::instance()->mutex.lock();
            if(!IsNumeric && !attrvalue.isEmpty()) {
                attribute_types_list[attribute_index_c] = CATEGORICAL;
            }
            ThreadManager::instance()->mutex.unlock();

            attribute_index_c++;

        }
    };

    lines.removeFirst();
    lines.removeLast();

    ThreadManager::instance()->process(QtConcurrent::map(lines.begin(), lines.end(), checkProjectsAttributesType), "Checking Project Attributes...");


    attribute_index = 0;
    auto addAttribute = [&](const QString &attrname) {
        AscDBAttributes.push_back(Attribute(attribute_index + 1, attrname.toUtf8().constData(), attribute_types_list[attribute_index]));
        attribute_index++;
    };

    std::for_each(first.begin(), first.end(), addAttribute);

    AscDBTargets.reserve(lines.count());

    project_index = 0;

    auto readProject = [&](const QStringList &line) {
        std::vector<Attribute> attribute_list_copy;// = attribute_list;
        attribute_list_copy.reserve(AscDBAttributes.size());


        long int attribute_index_c = 0;

        try {
            if(line.count() > AscDBAttributes.size())
                throw BadCSVLineException();
        } catch(BadCSVLineException& e) {
            handleException(e);
            return;
        }

        QHash<QString, QString> attrNameValuePairVec;
        attrNameValuePairVec.reserve(line.count());


        int lineValueIndex = 0;
        std::for_each(line.begin(), line.end(), [&](const QString &value){
            attrNameValuePairVec.insert(first[lineValueIndex], value);
            lineValueIndex++;
        });

        std::vector<Attribute> attributesVec = DataBase::instance()->getDBAttributes();

        std::for_each(attributesVec.begin(), attributesVec.end(), [&](const Attribute &attr){
            QHash<QString, QString>::iterator it = attrNameValuePairVec.find(attr.getDescription().c_str());

            attribute_list_copy.push_back(Attribute(attr.getNr(),
                                                    attr.getDescription(),
                                                    attr.getType(),
                                                    attr.getStatus(),
                                                    (it != attrNameValuePairVec.end()) ? it.value().toUtf8().constData() : std::string()) );
            attribute_index_c++;
        });

        ThreadManager::instance()->mutex.lock();
        AscDBTargets.push_back(Project(project_index + 1,
                                       "Target-" + std::to_string(project_index),
                                       1,
                                       attribute_list_copy, Project::AS_TARGET));
        project_index++;
        ThreadManager::instance()->mutex.unlock();
    };


    ThreadManager::instance()->process(QtConcurrent::map(lines.begin(), lines.end(), readProject), "Reading Targets...");
}

void AsciiFileManager::saveAsciiFile(const std::string &File)
{
    const std::vector<Attribute> &Attrs = DataBase::instance()->getDBAttributes();
    const std::vector<Project> &Prjs = DataBase::instance()->getDBProjects();
    const std::vector<Project> &Trgs = DataBase::instance()->getDBTargets();
    unsigned int counter1 = 0,counter2 = 0;
    std::vector<Attribute> tmpAttrs;
    std::vector<std::string> fileParts;
    std::string FileContents;
    char tmpStr[128];

    FileContents = "";
    FileContents = "[params]\n";
    fileParts.push_back(FileContents);
    sprintf(tmpStr,"%d",(int)Attrs.size());
    FileContents = "Count=";
    FileContents = FileContents + tmpStr + "\n";
    fileParts.push_back(FileContents);
    for(counter1 = 0;counter1 < Attrs.size();counter1++) {
        sprintf(tmpStr,"%d",Attrs[counter1].getNr());
        FileContents = "PARAM";
        FileContents = FileContents + tmpStr + "=" + Attrs[counter1].getDescription() + ",";
        fileParts.push_back(FileContents);

        if(Attrs[counter1].getType() == 1)
            FileContents = "NUMERIC";
        else FileContents = "CATEGORICAL";
        fileParts.push_back(FileContents);

        if(Attrs[counter1].getStatus() == 0) {
            FileContents = ",OFF";
            fileParts.push_back(FileContents);
        }

        FileContents = "\n";
        fileParts.push_back(FileContents);
    }

    FileContents = "\n";
    fileParts.push_back(FileContents);

    FileContents = "[PROJECTS]\n";
    fileParts.push_back(FileContents);
    sprintf(tmpStr,"%d",(int)Prjs.size());
    FileContents = "Count=";
    FileContents = FileContents + tmpStr + "\n";
    fileParts.push_back(FileContents);
    for(counter1 = 0;counter1 < Prjs.size();counter1++) {
        sprintf(tmpStr,"%d",Prjs[counter1].getNr());
        FileContents = "Project";
        FileContents = FileContents + tmpStr + "=" + Prjs[counter1].getDescription() ;
        fileParts.push_back(FileContents);

        tmpAttrs = Prjs[counter1].getAttributes();
        for(counter2 = 0;counter2 < tmpAttrs.size();counter2++){
            FileContents = std::string(",") + tmpAttrs[counter2].getValue().toStdString() ;
            fileParts.push_back(FileContents);
        }
        if(Prjs[counter1].getStatus() == 0)
            FileContents = ",OFF";
        fileParts.push_back(FileContents);

        FileContents = "\n";
        fileParts.push_back(FileContents);
    }

    FileContents = "\n";
    fileParts.push_back(FileContents);


    FileContents = "[TARGETS]\n";
    fileParts.push_back(FileContents);
    sprintf(tmpStr,"%d",(int)Trgs.size());
    FileContents = "Count=";
    FileContents = FileContents + tmpStr + "\n";
    fileParts.push_back(FileContents);
    for(counter1 = 0;counter1 < Trgs.size();counter1++) {
        sprintf(tmpStr,"%d",Trgs[counter1].getNr());
        FileContents = "Target";
        FileContents = FileContents + tmpStr + "=" + Trgs[counter1].getDescription();
        fileParts.push_back(FileContents);

        tmpAttrs = Trgs[counter1].getAttributes();
        for(counter2 = 0;counter2 < tmpAttrs.size();counter2++){
            FileContents = std::string(",") + tmpAttrs[counter2].getValue().toStdString();
            fileParts.push_back(FileContents);
        }
        if(Trgs[counter1].getStatus() == 0)
            FileContents = ",OFF";
        fileParts.push_back(FileContents);

        FileContents = "\n";
        fileParts.push_back(FileContents);
    }

    FileContents = "\n";
    fileParts.push_back(FileContents);
    std::ofstream out(File.c_str(),std::ios::out);
    for(int counter1 = 0; counter1 < fileParts.size(); counter1++) {
        out<<fileParts[counter1];
    }
    out<<"\n";
    out.close();
}

void AsciiFileManager::saveArffFile(const std::string &File)
{
    const std::vector<Attribute> &Attrs = DataBase::instance()->getDBAttributes();
    const std::vector<Project> &Prjs = DataBase::instance()->getDBProjects();
    const std::vector<Project> &Trgs = DataBase::instance()->getDBTargets();
    unsigned int counter1 = 0,counter2 = 0;
    std::vector<Attribute> tmpAttrs;
    std::vector<std::string> fileParts;
    std::string FileContents;
    QString attrName;

    FileContents = "";
    FileContents = "@relation " + File + "\n";
    fileParts.push_back(FileContents);
    for(counter1 = 0;counter1 < Attrs.size();counter1++) {
        FileContents = "@attribute";
        attrName = Attrs[counter1].getDescription().c_str();
        attrName.replace(" ", "_");
        FileContents = FileContents + " ";
        fileParts.push_back(FileContents);
        FileContents = (const char *)attrName.toUtf8();
        FileContents += " ";
        fileParts.push_back(FileContents);

        if(Attrs[counter1].getType() == 1)
            FileContents = "numeric";
        else FileContents = "nominal";
        fileParts.push_back(FileContents);

        FileContents = "\n";
        fileParts.push_back(FileContents);
    }

    FileContents = "\n";
    fileParts.push_back(FileContents);

    FileContents = "@data\n";
    fileParts.push_back(FileContents);
    for(counter1 = 0;counter1 < Prjs.size();counter1++) {
        tmpAttrs = Prjs[counter1].getAttributes();
        for(counter2 = 0;counter2 < tmpAttrs.size() - 1; counter2++){
            FileContents = tmpAttrs[counter2].getValue().toStdString() + std::string(",");
            if(FileContents == ",")
                FileContents = "?,";
            fileParts.push_back(FileContents);
        }
        FileContents = tmpAttrs[tmpAttrs.size() - 1].getValue().toStdString();
        fileParts.push_back(FileContents);
        FileContents = "\n";
        fileParts.push_back(FileContents);
    }

    FileContents = "\n";
    fileParts.push_back(FileContents);


    std::ofstream out(File.c_str(),std::ios::out);
    for(int counter1 = 0; counter1 < fileParts.size(); counter1++) {
        out<<fileParts[counter1];
    }
    out<<"\n";
    out.close();
}

