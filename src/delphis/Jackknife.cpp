#include "Jackknife.hpp"
#include "AnalogyMethod.hpp"
#include "DataBase.hpp"

Jackknife::Jackknife(const AnalogyMethod &analogy, const QVector<QPair<int, QVector<double> > > &indexedMatrix, const QVector<QPair<int, QVector<double> > > &indexedStdMatrix, const QVector<double> &realVals, const QVector<QPair<QString, double> > &weightsPair)
{
    m_analogy = &analogy;
    m_indexedMatrix = &indexedMatrix;
    m_indexedStdMatrix = &indexedStdMatrix;
    m_realVals = &realVals;
    m_weightsPair = &weightsPair;
}

void Jackknife::perform() const
{
    QVector<double> weights;
    DataBase::convertSubsetWeightsToWeights(*m_weightsPair, weights);
    QVector<QPair<int, QVector<double> > > matrix = *m_indexedStdMatrix;
    m_estimates.reserve(matrix.count());
    for(int index = 0; index < matrix.count(); index++) {
        QPair<int, QVector<double> > row = matrix[index];
        matrix.remove(index);
        double estimate = m_analogy->estimate(*m_indexedMatrix, matrix, row.second, weights, *m_realVals);
        m_estimates.push_back(estimate);
        matrix.insert(index, row);
    }
}

const QVector<double> &Jackknife::getEstimates() const
{
    return m_estimates;
}

