#include "Project.hpp"
#include <QSqlRecord>
#include <QSqlField>

#include <algorithm>
#include <iostream>



Project::Project(int Nr, const std::string &Desc, int Status, const std::vector<Attribute> &Attrs, const Type t)
{
    this->Nr = Nr;
    this->Description = Desc;
    this->Status = Status;
    this->Attributes = Attrs;
    if(Nr != -1)
		sqlQuery(t);
}

int Project::getNr() const
{
    return Nr;
}

std::string Project::getDescription() const
{
    return Description.c_str();
}

int Project::getStatus() const
{
    return Status;
}

std::vector<Attribute> Project::getAttributes() const
{
	return Attributes;
}

void Project::setNr(int Nr)
{
    this->Nr = Nr;
}

void Project::setDescription(const std::string &Description)
{
    this->Description = Description;
}

void Project::setStatus(int Status)
{
    this->Status = Status;
}

void Project::setAttributes(const std::vector<Attribute> &Attrs)
{
	this->Attributes = Attrs;
}

void Project::addAttribute(const Attribute &Attr)
{
    this->Attributes.push_back(Attr);
}

const std::string &Project::sqlQuery(Type t) const
{
    if(Nr == -1) {
		query = "";
        return query;
    }
    if(query.size() > 0)
        return query;
    query = (t == Project::AS_PROJECT) ? "insert into Projects values (" : "insert into Targets values (";

    auto constructSqlValuesStr = [&](const Attribute &attr) {

        query += std::string(", \"") + attr.getValue().replace('"',"'").toStdString() + std::string("\"");
    };

    query += std::to_string(Nr);
    query += ",";
    query += "'" + Description + "'";
    query += ",";
    query += std::to_string(Status);
    std::for_each(Attributes.begin(), Attributes.end(),constructSqlValuesStr);
    query += ")";
    return query;
}
