#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <stdio.h>
#include <cmath>
#include <string.h>
#include <QString>
#include <QVector>
#include <QVariant>
#include <QDebug>
#include <QFutureWatcher>
#include <QtConcurrent>
#include <QCoreApplication>

#include <QtWidgets/QMessageBox>
#include "CommonFunctionsContainer.hpp"
#include "ThreadManager.hpp"
#include "Enums.h"
#include "Exception.hpp"
#include <bitset>
#include <stdio.h>
#include <stddef.h>

#ifdef _WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif

static std::set<std::string> sqlite_keywords = {"ABORT",
"ACTION",
"ADD",
"AFTER",
"ALL",
"ALTER",
"ANALYZE",
"AND",
"AS",
"ASC",
"ATTACH",
"AUTOINCREMENT",
"BEFORE",
"BEGIN",
"BETWEEN",
"BY",
"CASCADE",
"CASE",
"CAST",
"CHECK",
"COLLATE",
"COLUMN",
"COMMIT",
"CONFLICT",
"CONSTRAINT",
"CREATE",
"CROSS",
"CURRENT_DATE",
"CURRENT_TIME",
"CURRENT_TIMESTAMP",
"DATABASE",
"DEFAULT",
"DEFERRABLE",
"DEFERRED",
"DELETE",
"DESC",
"DETACH",
"DISTINCT",
"DROP",
"EACH",
"ELSE",
"END",
"ESCAPE",
"EXCEPT",
"EXCLUSIVE",
"EXISTS",
"EXPLAIN",
"FAIL",
"FOR",
"FOREIGN",
"FROM",
"FULL",
"GLOB",
"GROUP",
"HAVING",
"IF",
"IGNORE",
"IMMEDIATE",
"IN",
"INDEX",
"INDEXED",
"INITIALLY",
"INNER",
"INSERT",
"INSTEAD",
"INTERSECT",
"INTO",
"IS",
"ISNULL",
"JOIN",
"KEY",
"LEFT",
"LIKE",
"LIMIT",
"MATCH",
"NATURAL",
"NO",
"NOT",
"NOTNULL",
"NULL",
"OF",
"OFFSET",
"ON",
"OR",
"ORDER",
"OUTER",
"PLAN",
"PRAGMA",
"PRIMARY",
"QUERY",
"RAISE",
"RECURSIVE",
"REFERENCES",
"REGEXP",
"REINDEX",
"RELEASE",
"RENAME",
"REPLACE",
"RESTRICT",
"RIGHT",
"ROLLBACK",
"ROW",
"SAVEPOINT",
"SELECT",
"SET",
"TABLE",
"TEMP",
"TEMPORARY",
"THEN",
"TO",
"TRANSACTION",
"TRIGGER",
"UNION",
"UNIQUE",
"UPDATE",
"USING",
"VACUUM",
"VALUES",
"VIEW",
"VIRTUAL",
"WHEN",
"WHERE",
"WITH",
"WITHOUT" };


bool isSqlKeyword(const QString &keyword)
{
    auto result = std::find(sqlite_keywords.begin(), sqlite_keywords.end(), keyword.toUtf8().constData());

    return result != std::end(sqlite_keywords);
}

int factorial(int n)
{
    int k;
    if(n==0)
        return(1);
    else k=n*factorial(n-1);
    return k;
}

int findSubsetCombinationsCount(int n)
{
    int count = 0;
    for(int counter = 1; counter < n; counter++)
    {
        count += factorial(n)/(factorial(counter)*factorial(n-counter));
    }
    return count;
}

double stod_(const std::string &strInput)
{
    /* STOD: String to double */
    /* Written by Mathias Van Malderen */
    double dbl_one = 0;
    double dbl_two = 0;
    double dbl_final = 0;
    int strlen;
    bool dec_pt = false;
    int nums_before_dec = 0;
    int nums_after_dec = 0;

    strlen = strInput.length();


    /* Check whether the string can be transformed into a number */

    if(strInput[0] == '0' && strInput[1] == '0')
    {
        // invalid number !
        return 0;
    }

    for(int i = 0; i < strlen; i++)
    {
        if(strInput[i] == '0' || strInput[i] == '1' || strInput[i] == '2' || strInput[i] == '3' || strInput[i] == '4' || strInput[i] == '5' || strInput[i] == '6' || strInput[i] == '7' || strInput[i] == '8' || strInput[i] == '9')
        {
            // valid number
        }
        else if(strInput[i] == '.')
        {
            if(dec_pt)
            {
                // there was already a decimal point counted
                // invalid number !
                return 0;
            }
            else
            {
                dec_pt = true; // increment by one
            }
        }
        else
        {
            // invalid number !
            return 0;
        }
    }

    /* Convert the number */
    // STEP 1: Calculate the amount of numbers before/after the decimal point (if there's one)
    if(dec_pt) // if there's a decimal point in the number
    {
        for(int i = 0; i < strlen; i++)
        {
            if(strInput[i+1] != '.')
            {
                nums_before_dec++;
            }
            else
            {
                nums_before_dec++;
                break;
            }
        }
        nums_after_dec = strlen-nums_before_dec;
        nums_after_dec -= 1;
    }
    else
    {
        // This piece of code was added later as a BUGFIX !
        // Now the STOD-function is working 100% perfect !!!
        nums_after_dec = 0;
        nums_before_dec = strlen;
    }

    // STEP 2: Convert the string to a real number
    for(int i = 0; i < nums_before_dec; i++)
    {
        switch(strInput[i])
        {
        case '0':
            dbl_one += 0 * apow(10, (nums_before_dec - i));
            break;
        case '1':
            dbl_one += 1 * apow(10, (nums_before_dec - i));
            break;
        case '2':
            dbl_one += 2 * apow(10, (nums_before_dec - i));
            break;
        case '3':
            dbl_one += 3 * apow(10, (nums_before_dec - i));
            break;
        case '4':
            dbl_one += 4 * apow(10, (nums_before_dec - i));
            break;
        case '5':
            dbl_one += 5 * apow(10, (nums_before_dec - i));
            break;
        case '6':
            dbl_one += 6 * apow(10, (nums_before_dec - i));
            break;
        case '7':
            dbl_one += 7 * apow(10, (nums_before_dec - i));
            break;
        case '8':
            dbl_one += 8 * apow(10, (nums_before_dec - i));
            break;
        case '9':
            dbl_one += 9 * apow(10, (nums_before_dec - i));
            break;
        default:
            // invalid number !
            return 0;
        }
    }

    dbl_one = dbl_one / 10; // little fix


    for(int i = 0; i < nums_after_dec; i++)
    {
        switch(strInput[i + nums_before_dec + 1])
        {
        case '0':
            dbl_two += (0 / apow(10, i+1));
            break;
        case '1':
            dbl_two += (1 / apow(10, i+1));
            break;
        case '2':
            dbl_two += (2 / apow(10, i+1));
            break;
        case '3':
            dbl_two += (3 / apow(10, i+1));
            break;
        case '4':
            dbl_two += (4 / apow(10, i+1));
            break;
        case '5':
            dbl_two += (5 / apow(10, i+1));
            break;
        case '6':
            dbl_two += (6 / apow(10, i+1));
            break;
        case '7':
            dbl_two += (7 / apow(10, i+1));
            break;
        case '8':
            dbl_two += (8 / apow(10, i+1));
            break;
        case '9':
            dbl_two += (9 / apow(10, i+1));
            break;
        default:
            // invalid number !
            return 0;
        }
    }

    // STEP 3: Return the converted string as a double:
    dbl_final = dbl_one + dbl_two;
    return dbl_final;
}
/* This function 'apow' raises x to the power of y, it's a dependency of 'stod' */
double apow(const float &x, int y)
{
    double result = 1;
    if(y == 0)
        return result;

    if(y < 0)
    {
        y = -y;
        for(int i = 0; i < y; i++)
            result = result * x;
        return 1/result;
    }

    for(int i = 0; i < y; i++)
        result = result * x;

    return result;
}

#if 0
bool caseInsensitiveStringCompare(const string& str1, const string& str2)
{
    if (str1.size() != str2.size())
    {
        return false;
    }
    for (string::const_iterator c1 = str1.begin(), c2 = str2.begin(); c1 != str1.end(); ++c1, ++c2)
    {
        if (tolower(*c1) != tolower(*c2))
        {
            return false;
        }
    }
    return true;
}
#endif

//given a set and a particular combination of set elements, return the next combination (in lexicographic order)
//  for example: the order of combinations for the set {a,b,c} is: {},{a},{b},{c},{a,b},{a,c},{b,c},{a,b,c}
//
bool next_combination(const std::vector<std::string>& set, std::vector<std::string>& combo)
{
    //if set is empty, combo has more elements than set, or combo has same number of elements as set (i.e. last combination)
    //then exit (i.e. the next combination is not defined)
    if (set.empty() || combo.size() >= set.size())
        return false;

    //if current combination is the empty set then the next combination is the first non-trivial one
    if (combo.empty())
    {
        combo.push_back(set[0]);
        return true;
    }

    //declare and define a membership indication vector
    std::vector<bool> membership = subset_tally(set,combo);

    //increment the membership indication vector (via special algorithm)
    battle_transform(membership);

    //given the incremented membership indication vector, generate the next combination
    combo = generate_subset(set,membership);

    //the next combination has been found
    return true;
}

//return a boolean membership indication vector, testing each element in set for membership in subset:
//value of 1 indicates membership, value of 0 indicates non-membership (tally is right justified)
//
//  for example: if  set = { a a b b b c d e }  and  subset = { a b b c e }
//           then member = { 0 1 0 1 1 1 0 1 )  <-- tallies which set elements are members of subset
//
std::vector<bool> subset_tally(const std::vector<std::string>& set, const std::vector<std::string>& subset)
{
    std::vector<std::string>::const_iterator _set = set.end();
    std::vector<std::string>::const_iterator _subset = subset.end();
    --_subset;
    --_set;

    std::vector<bool> member(set.size());
    std::vector<bool>::iterator _member = member.end();

    while (_member != member.begin())
    {
        --_member;

        //if set element is a member of the given subset, tally respective member element
        if(*_subset==*_set)
        {
            *_member = true;
            //then move to the next element of the subset (if possible)
            if(_subset != subset.begin())
                --_subset;
            else	break;
        }
        //else set element is not a member of the given subset
        else	*_member = false;

        //move to next element of the set to test for subset membership (if possible)
        if(_set == set.begin())
            break;
        else	--_set;
    };

    return member;
}

//generate respective subset from boolean membership indication vector
//
//for example: if  set = { a a b b b c d e }  and
//              member = { 0 1 0 1 1 1 0 1 )
//         then subset = { a b b c e }        <-- subset represented by tallies
//
std::vector<std::string> generate_subset(const std::vector<std::string>& set, const std::vector<bool>& member)
{
    std::vector<std::string> subset;
    std::vector<std::string>::const_iterator _set = set.begin();
    std::vector<bool>::const_iterator _member = member.begin();

    while(_member != member.end())
    {
        if (*_member++ == true)
            subset.push_back(*_set++);
        else	++_set;
    };

    return subset;
}

//increment the boolean bitfield via the following specific transform (a.k.a. the bits march into battle):
//
//imagine that every bitfield is a battlefield, where the ones at the far right are involved in the fiercest of combat,
//zeroes to the left of the rightmost ones are in the demilitarized zone, and reinforcements are any ones to their left;
//the battle is going poorly, reinforcements are needed, so the closest one to the battle moves one step closer to combat,
//and when he does all of the righmost ones that were in battle rush all the way to the left to greet this brave little bit;
//if all the available ones are already in battle (i.e. all ones are already all the way to the right) then RETREAT (!)
//and try to approaching battle once more (i.e. starting all the way to the left), this time with an extra one to help out;
//if all possible bits were already engaged, then the battle is won, victory is yours, the last combination has been reached.
//
//  for example: bitfield = 01011101-->01011011-->01010111-->01001111-->00111110-->00111101-->00111011--> ... -->11111111
//
//and that is how to find the next transform of the boolean battlefield (i.e. next combination, next partition, etc.)
//seriously, it's a very useful combinatoric transform; and how else could this particular algorithm possibly be explained?!
//
bool battle_transform(std::vector<bool>& bitfield)
{
    std::vector<bool>::iterator _bitfield = bitfield.end();
    _bitfield--;

    bool zero_found = false;
    bool bit_found = false;
    int bits_in_battle = 0;

    //read bits from right to left, surveying the battlefield (i.e. bitfield)
    while(_bitfield+1 != bitfield.begin())
    {
        //if still in the combat zone (i.e. haven't encountered a zero yet)
        if(!zero_found)
            //then count number of bits engaged in battle (i.e. number of rightmost ones)
            (*_bitfield == true ) ? ++bits_in_battle : zero_found=true;
        //else made it out of the combat zone (i.e. a zero has been encountered)
        else
            //if the next recruit is found (i.e. first one to the left of zeroes)
            if(*_bitfield == true)
            {
                bit_found=true;
                //then he moves one step to the right (moving ever closer to battle)
                *_bitfield++ = false;
                *_bitfield++ = true;
                //and his fellow combatants rush all the way to the left to greet him
                while(_bitfield!=bitfield.end())if(bits_in_battle-->0)*_bitfield++=true;
                    else *_bitfield++=false;
                //transform has been found
                return true;
            }
        //if possible, move one step to the left on the battlefield (i.e. bitfield)
        if(_bitfield != bitfield.begin())
            --_bitfield;
        else	break;
    };

    //if all bits have already been engaged (i.e. all ones are already all the way to the right)
    if(zero_found && !bit_found)
    {
        //then retreat and regroup (adding another one to the battlefield)
        _bitfield=bitfield.begin();
        ++bits_in_battle;
        //move all ones to the left, so that they may march into battle once more
        while(_bitfield != bitfield.end()) bits_in_battle-->0 ? *_bitfield++=1 : *_bitfield++=0;
        //next bit-sequence has been found
        return true;
    }

    //it must have been a completely non-zero bitfield (i.e. the last bit-sequence)
    return false;
}

std::ostream& operator<<(std::ostream& out, const std::vector<std::string>& v)
{
    for(std::vector<std::string>::const_iterator i=v.begin(); i!=v.end(); ++i)
    {
        out<<*i;
    }
    return out;
}

double manhattan(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> & weights)
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += weights[counter1]*fabs(point1[counter1] - point2[counter1]);
    }
    return sum;
}

double euklidean(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights)
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += weights[counter1]*pow(point1[counter1] - point2[counter1], 2);
    }

    return sqrt(sum);
}

double canberra(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> & weights)
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += weights[counter1]*fabs(point1[counter1] - point2[counter1])/(fabs(point1[counter1]) + fabs(point2[counter1]));
    }

    return sum;
}

double chebychev(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights)
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;
    double temp = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        temp = weights[counter1]*fabs(point1[counter1] - point2[counter1]);
        if(temp > sum)
            sum = temp;
    }

    return sum;
}

double minkowski(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> & weights, int lamda)
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum = 0;

    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += weights[counter1]*pow(fabs(point1[counter1] - point2[counter1]), lamda);
    }

    return pow(sum, 1/(double)lamda);
}

double czekanowski(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights)
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum1 = 0, sum2 = 0;

    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        if(point1[counter1] > point2[counter1])
            sum1 += weights[counter1]*point2[counter1];
        else sum1 += weights[counter1]*point1[counter1];
        sum2 += weights[counter1]*(point2[counter1] + point1[counter1]);
    }

    return 1 - ((2*sum1)/sum2);
}

double kaufman_rousseeuw(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights, const std::vector<double> &ranges)
{
    if(point1.size() != point2.size())
        return -1;
    if(point1.size() != weights.size())
        return -1;

    double sum1 = 0, sum2 = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum1 += weights[counter1]*fabs(point1[counter1] - point2[counter1])/ranges[counter1];
    }

    return sum1/ranges.size();
}


double manhattan(const std::vector<double> & point1, const std::vector<double> & point2)
{
    if(point1.size() != point2.size())
        return -1;

    double sum = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += fabs(point1[counter1] - point2[counter1]);
    }

    return sum;
}

double euklidean(const std::vector<double> &point1, const std::vector<double> &point2)
{
    if(point1.size() != point2.size())
        return -1;

    double sum = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += pow(point1[counter1] - point2[counter1], 2);
    }

    return sqrt(sum);
}

double canberra(const std::vector<double> & point1, const std::vector<double> & point2)
{
    if(point1.size() != point2.size())
        return -1;

    double sum = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += fabs(point1[counter1] - point2[counter1])/(fabs(point1[counter1]) + fabs(point2[counter1]));
    }

    return sum;
}

double chebychev(const std::vector<double> & point1, const std::vector<double> & point2)
{
    if(point1.size() != point2.size())
        return -1;

    double sum = 0;
    double temp = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        temp = fabs(point1[counter1] - point2[counter1]);
        if(temp > sum)
            sum = temp;
    }
    return sum;
}

double minkowski(const std::vector<double> &point1, const std::vector<double> & point2, int lamda)
{
    if(point1.size() != point2.size())
        return -1;

    double sum = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum += pow(fabs(point1[counter1] - point2[counter1]), lamda);
    }

    return pow(sum, 1/(double)lamda);
}

double czekanowski(const std::vector<double> &point1, const std::vector<double> &point2)
{
    if(point1.size() != point2.size())
        return -1;

    double sum1 = 0, sum2 = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        if(point1[counter1] > point2[counter1])
            sum1 += point2[counter1];
        else sum1 += point1[counter1];
        sum2 += point2[counter1] + point1[counter1];
    }

    return 1 - ((2*sum1)/sum2);
}

double kaufman_rousseeuw(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &ranges)
{
    if(point1.size() != point2.size())
        return -1;

    double sum1 = 0, sum2 = 0;
    for(int counter1 = 0; counter1 < point1.size(); counter1++)
    {
        sum1 += fabs(point1[counter1] - point2[counter1])/ranges[counter1];
    }

    return sum1/ranges.size();
}

static char *integerToBinaryString(unsigned int val, char *buff, int sz) {
    char *pbuff = buff;

    /* Must be able to store one character at least. */
    if (sz < 1) return NULL;

    /* Special case for zero to ensure some output. */
    if (val == 0) {
        *pbuff++ = '0';
        *pbuff = '\0';
        return buff;
    }

    /* Work from the end of the buffer back. */
    pbuff += sz;
    *pbuff-- = '\0';

    /* For each bit (going backwards) store character. */
    while (val != 0) {
        if (sz-- == 0) return NULL;
        *pbuff-- = ((val & 1) == 1) ? '1' : '0';

        /* Get next bit. */
        val >>= 1;
    }

    return pbuff+1;
}

int binaryStringToInteger(const char *str)
{
    int sum = 0;
    int len = strlen(str);
    for(int counter1 = len - 1; counter1 >= 0; counter1--) {
        if(str[counter1] == '1') {
            sum += pow(2, counter1 - len + 1);
        }
    }

    return sum;
}

std::string toBinary(unsigned int n, unsigned int length)
{
    std::string r;
    while(n!=0) {r=(n%2==0 ?"0":"1")+r; n/=2;}

    if(r.size() < length) {
        std::string fillstr(length - r.size(), '0');
        r = fillstr + r;
    }
    return r;
}


void getAllWeightSubsets(unsigned int length, QVector<QString> &rvalue)
{
    long long unsigned int combinations = std::pow(2, length);
    std::vector<int> combinationVec;
    combinationVec.reserve(combinations);
	rvalue.reserve(combinations);

    for(int iter = 1; iter < combinations; iter++) {
        combinationVec.push_back(iter);
    }

    auto findSubset = [&](int iter) {
        if(iter == 0) {
            return;
        }
        QString weights = toBinary(iter, length).c_str() ;
		weights += "\n";
		ThreadManager::instance()->mutex.lock();
        rvalue.push_back(weights);
		ThreadManager::instance()->mutex.unlock();
    };

    ThreadManager::instance()->process(QtConcurrent::map(combinationVec.begin(), combinationVec.end(), findSubset));
}

QVector<QVector<QString>> getAllSubsets(const QVector<QString> &set)
{
    QVector<QVector<QString>> subset;
    subset.push_back(QVector<QString>());

    for (int i = 0; i < set.size(); i++) {
        QVector<QVector<QString>> subsetTemp = subset;

        for (int j = 0; j < subsetTemp.size(); j++)
            subsetTemp[j].push_back( set[i] );

        for (int j = 0; j < subsetTemp.size(); j++)
            subset.push_back( subsetTemp[j] );
    }

    subset.erase(subset.begin());
    return subset;
}

void transpose(const QVector<QVector<double> > data, QVector<QVector<double> > &result)
{
    // this assumes that all inner vectors have the same size and
    // allocates space for the complete result in advance
    result = QVector<QVector<double> >(data[0].size(), QVector<double>(data.size()));
    for (QVector<double>::size_type i = 0; i < data[0].size(); i++)
        for (QVector<double>::size_type j = 0; j < data.size(); j++) {
            result[i][j] = data[j][i];
        }
}

void standardizeVector(const QVector<double> &inputVec, QVector<double> &outputVec)
{
    double min = (*std::min_element(inputVec.begin(), inputVec.end()));
    double max = (*std::max_element(inputVec.begin(), inputVec.end()));
    double difference = max - min;

    outputVec.reserve(inputVec.size());

    auto standardizeValue = [&](double value) { outputVec.push_back((value - min)/difference); };

    std::for_each(inputVec.begin(), inputVec.end(), standardizeValue);
}

uint64_t hash_str_uint64(const std::string& str)
{
    uint64_t hash = 0xcbf29ce484222325;
    uint64_t prime = 0x100000001b3;

    for(int i = 0; i < str.size(); ++i) {
        uint16_t value = str[i];
        hash = hash ^ value;
        hash *= prime;
    }

    return hash;

}

unsigned long long hash(const std::string& str)
{
    unsigned long long hash = 5381;
    for (size_t i = 0; i < str.size(); ++i)
        hash = 33 * hash + (unsigned char)str[i];
    return hash;
}


void numerifyMatrix(const QVector<QStringList> &matrix, const QList<int> &attributeTypes, QVector<QVector<double> > &num_matrix)
{
    num_matrix.reserve(matrix.count());
    std::for_each(matrix.begin(), matrix.end(), [&](const QStringList &strl){
        QVector<double> rowvec;
        rowvec.reserve(strl.count());
        int index = 0;
        std::for_each(strl.begin(), strl.end(), [&](const QString &str){
            bool valid = false;
            double value = str.toDouble(&valid);
            if(valid && attributeTypes[index] == NUMERICAL) {
                rowvec.push_back(value);
            } else {
                value = hash(str.toStdString());
                rowvec.push_back(-value);
            }
            index++;
        });
        num_matrix.push_back(rowvec);
    });
}

void numerifyVector(const QStringList &vec, const QList<int> &attributeTypes, QVector<double> &num_vec)
{
    num_vec.reserve(vec.count());
    int index = 0;
    std::for_each(vec.begin(), vec.end(), [&](const QString &str){
        bool valid = false;
        double value = str.toDouble(&valid);
        if(valid && attributeTypes[index] == NUMERICAL) {
            num_vec.push_back(value);
        } else {
            value = hash(str.toStdString());
            num_vec.push_back(-value);
        }
        index++;
    });
}

QVector<double> getNumerifyVector(const QStringList &vec, const QList<int> &attributeTypes)
{
    QVector<double> num_vec;
    num_vec.reserve(vec.count());
    int index = 0;
    std::for_each(vec.begin(), vec.end(), [&](const QString &str){
        bool valid = false;
        double value = str.toDouble(&valid);
        if(valid && attributeTypes[index] == NUMERICAL) {
            num_vec.push_back(value);
        } else {
            value = hash(str.toStdString());
            num_vec.push_back(-value);
        }
        index++;
    });
    return num_vec;
}

QStringList getStringifyIntList(const QList<int> &lst) {
    QStringList strl;
    strl.reserve(lst.count());

    std::for_each(lst.begin(), lst.end(), [&] (int value){ strl.push_back(QString::number(value)); });

    return strl;
}

void numerifyIntVector(const QStringList &invec, QVector<int> &outvec)
{
        outvec.reserve(invec.count());
        std::for_each(invec.begin(), invec.end(), [&](const QString &str){
            bool valid = false;
            double value = str.toInt(&valid);
            if(valid) {
                outvec.push_back(value);
            } else {
                value = hash(str.toStdString());
                outvec.push_back(-value);
            }
        });
}

QVector<int> getNumerifyIntVector(const QStringList &invec)
{
    QVector<int> outvec;
        outvec.reserve(invec.count());
        std::for_each(invec.begin(), invec.end(), [&](const QString &str){
            bool valid = false;
            double value = str.toInt(&valid);
            if(valid) {
                outvec.push_back(value);
            } else {
                value = hash(str.toStdString());
                outvec.push_back(-value);
            }
        });
       return outvec;
}


void getMinMaxVectors(const QVector<QVector<double> > &matrix, QVector<double> &minVec, QVector<double> &maxVec)
{
    QVector<QVector<double>> transMatrix;

    transpose(matrix, transMatrix);

    minVec.reserve(transMatrix.count());
    maxVec.reserve(transMatrix.count());

    auto iterateMatrix = [&](const QVector<double> &vec) {
        auto min = std::min_element(vec.begin(), vec.end());
        auto max = std::max_element(vec.begin(), vec.end());
        minVec.push_back(*min);
        maxVec.push_back(*max);
    };

    std::for_each(transMatrix.begin(), transMatrix.end(), iterateMatrix);
}

void handleException(const std::exception &e, bool gui, const std::string &extra_message)
{
    if(gui)
        QMessageBox::critical(0, "Critical", QString(e.what()) + QString("\n") + extra_message.c_str());
    else std::cout << std::string(e.what()) + std::string("\n") + extra_message;
}

double qMeanValue(const QVector<double> &vec)
{
    return std::accumulate(vec.begin(), vec.end(), .0) / vec.size();
}

double qMedianValue(QVector<double> vec)
{
    std::sort(vec.begin(), vec.end());

    return vec.size() % 2
            ? vec[vec.size() / 2]
            :
            ((double)vec[vec.size() / 2 - 1] + vec[vec.size() / 2]) * .5;
}

QString qModeValue(const QVector<QString> &vec)
{
    QMap<QString, int> districts;

    std::for_each(vec.begin(), vec.end(), [&](const QString &value) {
       districts.insert(value, 0);
    });

    std::for_each(vec.begin(), vec.end(), [&](const QString &value) {
       QMap<QString, int>::iterator it = districts.find(value);
       it.value()++;
    });

    int max = -1;
    QString ret;
    std::map<QString, int> nm = districts.toStdMap();
    std::for_each(nm.begin(), nm.end(), [&](const std::pair<QString, int> &p){
        if(max < p.second) {
            max = p.second;
            ret = p.first;
        }
    });

    return ret;
}

QString qModeValue(const QVector<double> &vec)
{
    const auto mode = [&]{
            size_t bestC = 0, tmpC = 0;
            int    bestV = 0, tmpV = 0;
            auto apply = [&](int next){
                if(tmpC > bestC)
                    bestC = tmpC, bestV = tmpV;
                tmpC = 1, tmpV = next;
            };
            for(int x : vec)
                if(x == tmpV)
                    tmpC++;
                else
                    apply(x);
            apply(0);
            return bestC ? QString::number(bestV) : "NULL";
        }();

    return mode;
}

QVector<int> fibonacciGenerator(int count)
{
    QVector<int> fiboVec = {0, 1};
    fiboVec.reserve(count);
    for(int index = 2; index < count; index++) {
        fiboVec.push_back(fiboVec[index - 2] + fiboVec[index - 1]);
    }

    return fiboVec;
}

void convertDoubleVectorToStringList(const QVector<double> &inputVec, QStringList &outList)
{
    outList.reserve(inputVec.size());

    std::for_each(inputVec.begin(), inputVec.end(), [&](double value) {
       outList << QString::number(value);
    });
}

QStringList getStringListFromDoubleVector(const QVector<double> &inputVec)
{
    QStringList outList;
    convertDoubleVectorToStringList(inputVec, outList);
    return outList;
}

const QVector<double> getDoubleVectorFromStringList(const QStringList &inputVec)
{
    QVector<double> ret;
    convertStringListToDoubleVector(inputVec, ret);
    return ret;
}


void convertStringListToDoubleVector(const QStringList &inputVec, QVector<double> &outList)
{
    outList.reserve(inputVec.size());

    std::for_each(inputVec.begin(), inputVec.end(), [&](const QString &value) {
       bool valid = false;
       double dblvalue= value.toDouble(&valid);
       if(!valid)
           throw InvalidProjectValue("", value.toStdString());
       outList.push_back(dblvalue);
    });

}

void mywait(size_t secs)
{
    #ifdef _WIN32
        Sleep(1000 * secs);
    #else
        sleep(secs);
    #endif
}

void printProgress(const std::string &label, int step, int total )
{
    //progress width
    const int pwidth = 72;

    //minus label len
    int width = pwidth - strlen( label.c_str() );
    int pos = ( step * width ) / total ;


    int percent = ( step * 100 ) / total;

    //set green text color, only on Windows
    //SetConsoleTextAttribute(  GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_GREEN );
    printf( "%s[", label.c_str() );

    //fill progress bar with =
    for ( int i = 0; i < pos; i++ )  printf( "%c", '=' );

    //fill progress bar with spaces
    printf( "% *c", width - pos + 1, ']' );
    printf( " %3d%%\r", percent );

    //reset text color, only on Windows
    //SetConsoleTextAttribute(  GetStdHandle( STD_OUTPUT_HANDLE ), 0x08 );
}


QString convertCommaInsideApostrophs(const QString &line)
{
    QString newLine;
    bool apostrophOpen = false;
    for(int index = 0; index < line.count(); index++) {
        if(line[index] == '"') {
            apostrophOpen = !apostrophOpen;
            continue;
        }


        if(apostrophOpen && line[index] == ',') {
            newLine += "&COMMA&";
        } else {
            newLine.push_back(line[index]);
        }
    }
    return newLine;
}

double mean(const std::vector<double> &v)
{
    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    return sum / v.size();
}

double median(const std::vector<double> &v)
{
    if(!v.empty()) {
        std::vector<double> svalues = v;
        std::sort(svalues.begin(), svalues.end());
        int size = svalues.size();
        if(size % 2 == 0)
            return (svalues[size/2 - 1] + svalues[size/2]) / 2;
        else
            return svalues[size/2];
    }

    return 0;
}

double variance(const std::vector<double> &v)
{
    double m = mean(v);
    double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
    return sq_sum / v.size() - m * m;
}

double standardDeviation(const std::vector<double> &v)
{
    return std::sqrt(variance(v));
}
