#include <algorithm>

#include <QFile>
#include <QSettings>
#include <QVector>
#include <QtConcurrent>

#include "ConfigExporter.hpp"
#include "ThreadManager.hpp"
#include "Exception.hpp"


Configuration::Configuration()
{

}

void Configuration::importConfig(const QString &file)
{
    QSettings set(file,QSettings::IniFormat);


    if(set.value("Type").toString() != "AnalogyMethodSimpleSearchConfiguration" ||
            set.value("Version") != "1.0") {
        throw ImportConfigurationError();
        return;
    }

    evaluationMethod = set.value("Parameters/EvaluationMethod").toString();
    minAnalogies = set.value("Parameters/MinAnalogies").value<int>();
    maxAnalogies = set.value("Parameters/MaxAnalogies").value<int>();
    selectedEstimators = set.value("Parameters/Estimators").value<QStringList>();
    selectedDistanceMetrics = set.value("Parameters/DistanceMetrics").value<QStringList>();
    estimAttrName = set.value("Parameters/EstimAttrName").toString();

    estimAttrValsStr = set.value("Parameters/EstimAttrVector").value<QStringList>();
    estimAttrVals.clear();
    convertStringListToDoubleVector(estimAttrValsStr, estimAttrVals);


    sizeAdjustedAttrValsStr = set.value("Parameters/SizeAdjustedAttrVector").value<QStringList>();
    sizeAdjustedAttrVals.clear();
 //   convertStringListToDoubleVector(sizeAdjustedAttrValsStr, sizeAdjustedAttrVals);


    subsets.clear();
    QStringList strl = set.value("Subsets/Raw").value<QString>().split("\n");
    
    attributeNames = set.value("Projects/AttributeNames").value<QStringList>();
    attributeTypes = set.value("Projects/AttributeTypes").value<QStringList>();
    projectDescriptions = set.value("Projects/ProjectDescriptions").value<QStringList>();

    subsetWeights.clear();
    subsetWeights.reserve(strl.count());


    auto convertSubsetsToWeights = [&](const QString &str) {
        if(!str.isEmpty()) {
            QVector<QPair<QString, double> > row;
            row.reserve(str.count());
            for(int index = 0; index < str.count(); index++) {
                double weight = QString(str[index]).toDouble();
                row.append(QPair<QString, double> (attributeNames.at(index), weight));
            }

            subsetWeights.append(row);
        }
    };

    std::for_each(strl.begin(), strl.end(), convertSubsetsToWeights);


    projectsMatrix.clear();
    projectsNum = set.value("Projects/ProjectsNum").value<int>();
    projectsMatrix.reserve(projectsNum);
    for(int index = 0; index < projectsNum; index++) {
        QStringList strl = set.value(QString("Projects/Project") + QString().setNum(index)).value<QStringList>();

        projectsMatrix.push_back(strl);
    }
}

void Configuration::exportConfig(const QString &path, const QString &prefix, const QString &evaluationMethod, const QString &estimAttr, const QString &sizeAdjustedAttr, const QStringList &activeAttributes, int minAnalogies, int maxAnalogies, const QStringList &selectedDistanceMetrics, const QStringList &selectedEstimators, int methodType, unsigned int fileNum)
{
    if(activeAttributes.count() > 32 && methodType == 1) {
        throw TooLongConfiguration();
    }

    QVector<QString> attributesVec = activeAttributes.toVector();
    QList<int> attributesTypeVec = DataBase::instance()->getTypeOfAttributes(activeAttributes);
    int indexOfEstimAttr = attributesVec.indexOf(estimAttr);
    if(indexOfEstimAttr != -1)
        attributesVec.removeAt(indexOfEstimAttr);
    attributesTypeVec.removeAt(indexOfEstimAttr);

    QVector<QPair<int,QStringList> > attrsValDbl;
    //DataBase::instance()->createStandardizedMatrix(attributes, c);
    try {
        DataBase::instance()->getActiveAttributesVector(attributesVec.toList(), attrsValDbl);
    } catch (std::exception &e) {
        handleException(e);
        return;
    }
    QStringList projectsDesc;
    QStringList estimVals = DataBase::instance()->getActiveAttributeVector(estimAttr);
    QStringList sizeAdjusteVals = DataBase::instance()->getActiveAttributeVector(sizeAdjustedAttr);



    if(methodType == 1) {
        QVector<QString> subsets;
        getAllWeightSubsets(attributesVec.length(), subsets);
        const unsigned int sizeOfSubset = subsets.size()/fileNum + subsets.size() % fileNum;
        const unsigned int modulo = subsets.size() % sizeOfSubset;
        unsigned int pivot = 0;
        for(unsigned int counter = 1; counter <= fileNum; counter++) {

            QVector<QString> subsubsets;

            if(pivot + sizeOfSubset <= (unsigned int)subsets.count()) {
                subsubsets.resize(sizeOfSubset);
                qCopy(subsets.begin() + pivot, subsets.begin() + pivot + sizeOfSubset, subsubsets.begin());

            }
            else {
                subsubsets.resize(modulo);
                qCopy(subsets.begin() + pivot, subsets.end(), subsubsets.begin());
            }

            pivot += sizeOfSubset;

            QString filePath = path + "/" + prefix + "Configuration" + QString::number(counter) + ".ini";
            QFile f(filePath);
            f.remove();
            QSettings set(filePath,QSettings::IniFormat);
            set.setValue("Type","AnalogyMethodSimpleSearchConfiguration");
            set.setValue("Version","1.0");
            set.beginGroup("Parameters");
            set.setValue("EvaluationMethod", evaluationMethod);
            set.setValue("MinAnalogies", minAnalogies);
            set.setValue("MaxAnalogies", maxAnalogies);
            set.setValue("Estimators",selectedEstimators);
            set.setValue("DistanceMetrics",selectedDistanceMetrics);
            set.setValue("EstimAttrName",estimAttr);
            set.setValue("EstimAttrVector",estimVals);
            set.setValue("SizeAdjustedAttrVector",sizeAdjusteVals);
            set.endGroup();

            set.beginGroup("Subsets");

            QString subsubsetsToWrite;
            subsubsetsToWrite.reserve(subsubsets.size()*attributesVec.size());

            std::for_each(subsubsets.begin(), subsubsets.end(), [&](const QString &v) {
                subsubsetsToWrite += v;
            });

            set.setValue("Raw", subsubsetsToWrite);


            set.endGroup();


            set.beginGroup("Projects");
            set.setValue("AttributeNames",QStringList(attributesVec.toList()));
            attributeTypes.clear();
            std::for_each(attributesTypeVec.begin(), attributesTypeVec.end(), [&](int value) {
                attributeTypes << QString::number(value);
            });
            set.setValue("AttributeTypes",attributeTypes);

            set.setValue("ProjectsNum",attrsValDbl.count());

            int index = 0;
            auto iterateProjects = [&](const QPair<int,QStringList> &project) {
                projectsDesc << DataBase::instance()->getProjectDescriptionById(project.first);
                set.setValue(QString("Project") + QString().setNum(index), project.second);
                index++;
            };
            std::for_each(attrsValDbl.begin(), attrsValDbl.end(), iterateProjects);

            set.setValue("ProjectDescriptions", projectsDesc);
            set.endGroup();
        }

    } else {
        QString filePath = path + "/" + prefix + "Configuration" + ".ini";
        QFile f(filePath);
        f.remove();
        QSettings set(filePath,QSettings::IniFormat);
        set.setValue("Type","AnalogyMethodSimpleSearchConfiguration");
        set.setValue("Version","1.0");
        set.beginGroup("Parameters");
        set.setValue("EvaluationMethod", evaluationMethod);
        set.setValue("MinAnalogies", minAnalogies);
        set.setValue("MaxAnalogies", maxAnalogies);
        set.setValue("Estimators",selectedEstimators);
        set.setValue("DistanceMetrics",selectedDistanceMetrics);
        set.setValue("EstimAttrName",estimAttr);
        set.setValue("EstimAttrVector",estimVals);
        set.setValue("SizeAdjustedAttrVector",sizeAdjusteVals);
        set.endGroup();


        set.beginGroup("Projects");
        set.setValue("AttributeNames",QStringList(attributesVec.toList()));
        attributeTypes.clear();
        std::for_each(attributesTypeVec.begin(), attributesTypeVec.end(), [&](int value) {
            attributeTypes << QString::number(value);
        });
        set.setValue("AttributeTypes",attributeTypes);

        set.setValue("ProjectsNum",attrsValDbl.count());

        int index = 0;
        auto iterateProjects = [&](const QPair<int,QStringList> &project) {
            projectsDesc << DataBase::instance()->getProjectDescriptionById(project.first);
            set.setValue(QString("Project") + QString().setNum(index), project.second);
            index++;
        };
        std::for_each(attrsValDbl.begin(), attrsValDbl.end(), iterateProjects);

        set.setValue("ProjectDescriptions", projectsDesc);
        set.endGroup();
    }

}

void Configuration::exportConfig(const QString &path, const QString &prefix, const QString &evalMethod, const QString &estimAttr, const QString &sizeAdjustedAttr,  const QStringList &activeAttributes, unsigned int fileNum)
{
    exportConfig(path, prefix, evalMethod, estimAttr, sizeAdjustedAttr, activeAttributes, minAnalogies, maxAnalogies, selectedDistanceMetrics, selectedEstimators);
}

int Configuration::getMinAnalogies() const
{
    return minAnalogies;
}

void Configuration::setMinAnalogies(int value)
{
    minAnalogies = value;
}

int Configuration::getMaxAnalogies() const
{
    return maxAnalogies;
}

void Configuration::setMaxAnalogies(int value)
{
    maxAnalogies = value;
}

QStringList Configuration::getSelectedEstimators() const
{
    return selectedEstimators;
}

void Configuration::setSelectedEstimators(const QStringList &value)
{
    selectedEstimators = value;
}

QStringList Configuration::getSelectedDistanceMetrics() const
{
    return selectedDistanceMetrics;
}

void Configuration::setSelectedDistanceMetrics(const QStringList &value)
{
    selectedDistanceMetrics = value;
}

QStringList Configuration::getEstimAttrValsStr() const
{
    return estimAttrValsStr;
}

void Configuration::setEstimAttrValsStr(const QStringList &value)
{
    estimAttrValsStr = value;
}

QVector<QStringList> Configuration::getSubsets() const
{
    return subsets;
}

void Configuration::setSubsets(const QVector<QStringList> &value)
{
    subsets = value;
}

QVector<double> Configuration::getEstimAttrVals() const
{
    return estimAttrVals;
}

void Configuration::setEstimAttrVals(const QVector<double> &value)
{
    estimAttrVals = value;
}


int Configuration::getProjectsNum() const
{
    return projectsNum;
}

int Configuration::getAttrsNum() const
{
    return attrsNum;
}

QVector<QStringList> Configuration::getProjectsMatrix() const
{
    return projectsMatrix;
}

QVector<QVector<QPair<QString, double> > > Configuration::getSubsetWeights() const
{
    return subsetWeights;
}

QVector<double> Configuration::getSizeAdjustedAttrVals() const
{
    return sizeAdjustedAttrVals;
}

QString Configuration::getEvaluationMethod() const
{
    return evaluationMethod;
}

QStringList Configuration::getAttributeNames() const
{
    return attributeNames;
}
QString Configuration::getEstimAttrName() const
{
    return estimAttrName;
}

QStringList Configuration::getAttributeTypes() const
{
    return attributeTypes;
}

QStringList Configuration::getProjectDescriptions() const
{
    return projectDescriptions;
}


