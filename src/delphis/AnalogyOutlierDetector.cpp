#include <QVector>
#include <QRunnable>
#include <QMutex>
#include <QThreadPool>
#include <QtConcurrent>

#include "AnalogyOutlierDetector.hpp"
#include "DataBase.hpp"
#include "AnalogyMethod.hpp"
#include "Estimators.hpp"
#include "ThreadManager.hpp"
class OutlierDetectWorker : public QRunnable
{
public:
    static QMutex m_outlierMutex;
    static QVector<QPair<double, QString >> m_mreMmrePrjPair;
    OutlierDetectWorker(double initValue = -1,
                        int startIndex = 0, int endIndex = 0,
                        const QList<int> &attributeTypes = QList<int>(),
                        const QVector<QVector<double> > &prjVec = QVector<QVector<double> >(),
                        const QVector<double> &realValues = QVector<double>(),
                        const QVector<QPair<QString, double> > &weightsPair = QVector<QPair<QString, double> >(),
                        const AnalogyMethod &analogy = AnalogyMethod(),
                        const EvaluationMethodFactory &evalMethod = EvaluationMethodFactory(),
                        const QStringList &activePrjs = QStringList()) {
        m_initValue = initValue;
        m_range = QPair<int, int>(startIndex, endIndex);
        m_attributeTypes = attributeTypes;
        m_prjsVec = prjVec;
        m_realValues = realValues;
        m_weightsPair = weightsPair;
        m_analogy = analogy;
        m_evalutation = evalMethod;
        m_activeProjects = activePrjs;

    }


    virtual void run() {

        for(int index = m_range.first; index <= m_range.second; index++) {
            QVector<double> row = m_prjsVec[index];
            m_prjsVec.remove(index);
            double realval = m_realValues[index];
            m_realValues.remove(index);

            QVector<QVector<double> > stdPrjVec;
            QVector<QPair<int, QVector<double> > > indexedMatrix,indexedStdMatrix;
            DataBase::standardizeMatrix(m_prjsVec, m_attributeTypes, stdPrjVec);
            DataBase::createIndexMatrix(stdPrjVec, indexedStdMatrix);
            DataBase::createIndexMatrix(m_prjsVec, indexedMatrix);

            Jackknife jack(m_analogy, indexedMatrix, indexedStdMatrix, m_realValues, m_weightsPair);
            jack.perform();

            double evalvalue = m_evalutation.evaluate(jack.getEstimates(), m_realValues, m_analogy);

            m_outlierMutex.lock();
            m_mreMmrePrjPair[index] = QPair<double, QString>(std::abs(evalvalue - m_initValue)/m_initValue, m_activeProjects[index]);
            m_outlierMutex.unlock();

            m_prjsVec.insert(index, row);

            m_realValues.insert(index, realval);
        }
    }

private:
    QPair<int, int> m_range;
    QList<int> m_attributeTypes;
    QVector<QVector<double> > m_prjsVec;
    QVector<double> m_realValues;
    QVector<QPair<QString, double> > m_weightsPair;
    AnalogyMethod m_analogy;
    EvaluationMethodFactory m_evalutation;
    QStringList m_activeProjects;
    double m_initValue = -1;
};


QVector<QPair<double, QString >> OutlierDetectWorker::m_mreMmrePrjPair;
QMutex OutlierDetectWorker::m_outlierMutex;


AnalogyOutlierDetector::AnalogyOutlierDetector()
{

}

QVector<QPair<double, QString> > AnalogyOutlierDetector::search(const QString &evaluationMethod, const QVector<QPair<QString, double> > &weightsPair, const QString &estimattr, const QString &distanceMetric, const QString &estimator, int analogies)
{
    QStringList activeprjs = DataBase::instance()->getActiveProjectsVector();

    QVector<QPair<int,QStringList> > prjsAttrVals;
    QVector<QStringList> prjsAttrValsVec;
    prjsAttrValsVec.reserve(prjsAttrVals.count());

    QStringList attrs;
    attrs.reserve(weightsPair.count());
    std::for_each(weightsPair.begin(), weightsPair.end(), [&] (const QPair<QString, double> &weightPair){
        attrs.push_back(weightPair.first);
    });

    try {
        DataBase::instance()->getActiveAttributesVector(attrs, prjsAttrVals, Project::AS_PROJECT);
    } catch (std::exception &e) {
        handleException(e, true, "Target project has invalid or null values!");
        return QVector<QPair<double, QString >>();
    }

    std::for_each(prjsAttrVals.begin(), prjsAttrVals.end(), [&](const QPair<int,QStringList> &strlst){
        prjsAttrValsVec.push_back(strlst.second);
    });


    AnalogyMethod analogy(weightsPair, DistanceMetricsContainer::instance()->getDistanceMetricByName(distanceMetric),
                          EstimatorsContainer::instance()->getEstimatorByName(estimator), analogies);

    double initvalue = 0;
    QVector<QVector<double> > stdPrjVec;
    QVector<QVector<double> > prjsVec;
    QList<int> attributeTypes = DataBase::instance()->getTypeOfAttributes(attrs);

    numerifyMatrix(prjsAttrValsVec, attributeTypes, prjsVec);


    QVector<QPair<int, QVector<double> > > indexedMatrix,indexedStdMatrix;
    DataBase::standardizeMatrix(prjsVec, attributeTypes, stdPrjVec);
    DataBase::createIndexMatrix(stdPrjVec, indexedStdMatrix);
    DataBase::createIndexMatrix(prjsVec, indexedMatrix);
    QStringList vals = DataBase::instance()->getActiveAttributeVector(estimattr);
    QVector<double> realVals = getDoubleVectorFromStringList(vals);
    EvaluationMethodFactory eval(evaluationMethod, estimattr, activeprjs, prjsAttrValsVec, realVals, attrs, getStringifyIntList(attributeTypes));

    Jackknife jack(analogy, indexedMatrix, indexedStdMatrix, realVals, weightsPair);


    ThreadManager::instance()->setBusy();
    ThreadManager::instance()->process(QtConcurrent::run([&]() {
        jack.perform();
        initvalue = eval.evaluate(jack.getEstimates(), realVals, analogy);
    }));


    int jobSize = prjsVec.count() > 4*QThread::idealThreadCount() ? prjsVec.count()/QThread::idealThreadCount()/4 : 1;

    int pivot = 0;
    QVector<QPair<int, int> > ranges;
    for(pivot = 0; pivot < prjsVec.count() - jobSize; pivot += jobSize) {
        qDebug() << "Range:" << pivot << " - " << pivot + jobSize - 1;
        ranges.push_back(QPair<int, int>(pivot, pivot + jobSize - 1));

    }

    ranges.push_back(QPair<int, int>(pivot, prjsVec.count() - 1));


    OutlierDetectWorker::m_mreMmrePrjPair.clear();
    OutlierDetectWorker::m_mreMmrePrjPair.resize(prjsVec.count());

    ThreadManager::instance()->process(QtConcurrent::map(ranges.begin(), ranges.end(), [&](const QPair<int,int> &range){
        OutlierDetectWorker worker(
                            initvalue,
                           range.first,
                           range.second,
                           attributeTypes,
                           prjsVec,
                           realVals,
                           weightsPair,
                           analogy,
                           eval,
                           activeprjs);

        worker.run();
    }));




//    OutlierDetectWorker::m_mreMmrePrjPair.reserve(prjsVec.count());
//    for(int index = 0; index < prjsVec.count(); index++) {
//        QVector<double> row = prjsVec[index];
//        prjsVec.remove(index);
//        double realval = realVals[index];
//        realVals.remove(index);

//        QVector<QVector<double> > stdPrjVec;
//        QVector<QPair<int, QVector<double> > > indexedMatrix,indexedStdMatrix;
//        DataBase::standardizeMatrix(prjsVec, attributeTypes, stdPrjVec);
//        DataBase::createIndexMatrix(stdPrjVec, indexedStdMatrix);
//        DataBase::createIndexMatrix(prjsVec, indexedMatrix);

//        Jackknife jack(analogy, indexedMatrix, indexedStdMatrix, realVals, weightsPair);
//        jack.perform();

//        double evalvalue = eval.evaluate(jack.getEstimates(), realVals, analogy);

//        OutlierDetectWorker::m_mreMmrePrjPair.push_back(QPair<double, QString>(std::abs(evalvalue - initvalue)/initvalue, activeprjs[index]));

//        prjsVec.insert(index, row);

//        realVals.insert(index, realval);
//    }

    return OutlierDetectWorker::m_mreMmrePrjPair;

}
