#include <QFile>
#include <QSettings>
#include "Exception.hpp"
#include "BestParameters.hpp"
#include "Enums.h"
#include "AnalogyMethod.hpp"

AnalogyMethodParameters::AnalogyMethodParameters()
{
	init();
}

void AnalogyMethodParameters::init()
{
    analogies = 0;
    distanceMetric = InvalidDistanceMetric;
    statistic = InvalidStatistic;
    bestEvaluation = -1;
    sizeAdjustedAttribute = "";
    estimationAttribute = "";
    minkowskiLamda = 3;
}


void AnalogyMethodParameters::setEstimationAttribute(const QString &EstimationAttr)
{
    this->estimationAttribute = EstimationAttr;
}

QString AnalogyMethodParameters::getSizeAdjustedAttribute() const
{
    return sizeAdjustedAttribute;
}

QString AnalogyMethodParameters::getEstimationAttribute() const
{
    return estimationAttribute;
}

void AnalogyMethodParameters::setAnalogies(int analogies)
{
    this->analogies = analogies;
}

void AnalogyMethodParameters::setStatistic(const QString &statistic)
{
    this->statistic = statistic;
}

void AnalogyMethodParameters::setDistanceMetric(const QString &distancemetric)
{
    this->distanceMetric = distancemetric;
}

void AnalogyMethodParameters::setSubset(const QMap<QString, QVariant> &attrSubs)
{
    attributesSubset = attrSubs;
}

void AnalogyMethodParameters::setSubset(const QVector<QPair<QString, double> > &attrSubs)
{
    attributesSubset.clear();
    std::for_each(attrSubs.begin(), attrSubs.end(), [&] (const QPair<QString, double> &p) {
        attributesSubset.insert(p.first, p.second);
    });
}

void AnalogyMethodParameters::setBestEvaluation(double bestEvaluation)
{
        this->bestEvaluation = bestEvaluation;
}

int AnalogyMethodParameters::getAnalogies() const
{
    return analogies;
}

QString AnalogyMethodParameters::getStatistic() const
{
    return statistic;
}

QString AnalogyMethodParameters::getDistanceMetric() const
{
    return distanceMetric;
}

QVector<QPair<QString, double> > AnalogyMethodParameters::subset() const
{
    QVector<QPair<QString, double> > subsetVec;

    QMap<QString, QVariant>::const_iterator iter;

    for(iter = attributesSubset.begin(); iter != attributesSubset.end(); ++iter) {
        subsetVec.append(QPair<QString, double>(iter.key(), iter.value().toDouble()));
    }
    return subsetVec;
}

QVector<double> AnalogyMethodParameters::getSubsetVectorByAttributesSequence() const
{
    QVector<double> weights;
    QMap<QString, QVariant>::const_iterator iter;

    std::for_each(attributeNames.cbegin(), attributeNames.cend(), [&](const QString &attr) {
        iter = attributesSubset.find(attr);
        if(iter != attributesSubset.end())
            weights.append(iter.value().toDouble());
    });

    return weights;
}

double AnalogyMethodParameters::getBestEvaluation() const
{
    return bestEvaluation;
}

void AnalogyMethodParameters::setMinkowskiLamda(int lamda)
{
    minkowskiLamda = lamda;
}

int AnalogyMethodParameters::getMinkowskiLamda() const
{
    return minkowskiLamda;
}

void AnalogyMethodParameters::exportFile(const QString &file) const
{
    QFile f(file);
    f.remove();
    QSettings set(file,QSettings::IniFormat);
    int prjIndex = 0;

    set.setValue("type","AnalogyMethodParameters");
    set.setValue("version","1.0");
    set.beginGroup("AnalogyMethod");
    set.setValue("analogies", analogies);
    set.setValue("estimator", statistic);
    set.setValue("distancemetric", distanceMetric);
    set.setValue("estimationattribute",estimationAttribute);
    set.setValue("evaluationmethod",evaluationMethod);
    set.setValue("evaluationvalue",bestEvaluation);
    set.setValue("estimationvalues", getStringListFromDoubleVector(estimationAttributeVector));

    set.endGroup();

    set.beginGroup("Projects");
    set.setValue("attributenames", attributeNames);
    QStringList attrWeights;

    std::for_each(attributeNames.begin(), attributeNames.end(), [&](const QString &name){
        attrWeights << attributesSubset[name].toString();
    });
    set.setValue("attributeweights", attrWeights);
    set.setValue("attributetypes", attributeTypes);
    set.setValue("projectdescriptions", projectDescriptions);
    set.setValue("projectsnum",projectsMatrix.count());
    auto iterateProjects = [&](const QStringList &prjStrList) {
        set.setValue(QString("project") + QString::number(++prjIndex), prjStrList);
    };

    std::for_each(projectsMatrix.begin(), projectsMatrix.end(), iterateProjects);

    set.endGroup();
}

void AnalogyMethodParameters::validateMethod()
{

    if(attributeNames.count() == 0 || attributeNames.count() != attributeWeights.count()) {
        throw CorruptedIniFileException();
        return;
    }


    if(projectDescriptions.count() == projectsMatrix.count()){
        int index = 0;
        std::for_each(projectDescriptions.begin(), projectDescriptions.end(), [&](const QString &desc){
            QStringList values = DataBase::instance()->getProjectValuesByDescription(desc, attributeNames);
            QStringList projectsMatrixRow = projectsMatrix[index];
            int rowIndex = 0;
            std::for_each(projectsMatrixRow.begin(), projectsMatrixRow.end(), [&](const QString &value){
                if(values[rowIndex].toDouble() != value.toDouble()) {
                    throw MethodProjectValuesDifferentFromDatabase();
                }
               rowIndex++;
            });
            index++;
        });
    } else {
         throw MethodProjectValuesDifferentFromDatabase();
    }
}

void AnalogyMethodParameters::importFile(const QString &file)
{
    QSettings set(file,QSettings::IniFormat);

    if(set.value("type").toString() != "AnalogyMethodParameters" ||
            set.value("version") != "1.0") {

        throw ImportMethodError();
        return;
    }


    analogies = set.value("AnalogyMethod/analogies").toInt();

    statistic = set.value("AnalogyMethod/estimator").toString();

    distanceMetric = set.value("AnalogyMethod/distancemetric").toString();


    estimationAttribute = set.value("AnalogyMethod/estimationattribute").toString();

    evaluationMethod = set.value("AnalogyMethod/evaluationmethod").toString();

    bestEvaluation = set.value("AnalogyMethod/evaluationvalue").toDouble();

    QStringList estimationAttributeStrList = set.value("AnalogyMethod/estimationvalues").toStringList();
    convertStringListToDoubleVector(estimationAttributeStrList, estimationAttributeVector);

    attributeNames = set.value("Projects/attributenames").toStringList();
    attributeWeights = set.value("Projects/attributeweights").toStringList();


    int prjCount = set.value("Projects/projectsnum").toInt();
    attributeTypes = set.value("Projects/attributetypes").toStringList();
    projectDescriptions = set.value("Projects/projectdescriptions").toStringList();
    projectsMatrix.clear();
    QStringList prjStrList;
    projectsMatrix.reserve(prjCount);
    for(int prjIndex = 1; prjIndex <= prjCount; prjIndex++) {
        prjStrList = set.value(QString("Projects/project") + QString::number(prjIndex)).toStringList();
        projectsMatrix.push_back(prjStrList);
    }


    validateMethod();

    for(int index = 0; index < attributeNames.count(); index++) {
        attributesSubset.insert(attributeNames[index], QVariant(attributeWeights[index]));
    }

}

QVector<QStringList> AnalogyMethodParameters::getProjectsMatrix(const QList<int> &indices) const
{
    QVector<QStringList> activeProjectsMatrix;

    if(indices.count()) {
        activeProjectsMatrix.reserve(indices.count());
        std::for_each(indices.begin(), indices.end(), [&](int index){
            activeProjectsMatrix.push_back(projectsMatrix.at(index));
        });
        return activeProjectsMatrix;
    }
    return projectsMatrix;
}

void AnalogyMethodParameters::setProjectsMatrix(const QVector<QStringList > &value)
{
    projectsMatrix = value;
}

void AnalogyMethodParameters::setAttributes(const QStringList &value)
{
    attributeNames = value;
}

QStringList AnalogyMethodParameters::getAttributeNames() const
{
    return attributeNames;
}

void AnalogyMethodParameters::setEvaluationMethod(const QString &value)
{
    evaluationMethod = value;
}

QString AnalogyMethodParameters::getEvaluationMethod() const
{
    return evaluationMethod;
}
QVector<double> AnalogyMethodParameters::getEstimationAttributeVector(const QList<int> &indices) const
{
    QVector<double> activeProjectsEstims;

    if(indices.count()) {
        activeProjectsEstims.reserve(indices.count());
        std::for_each(indices.begin(), indices.end(), [&](int index){
            activeProjectsEstims.push_back(estimationAttributeVector.at(index));
        });
        return activeProjectsEstims;
    }

    return estimationAttributeVector;
}

void AnalogyMethodParameters::setEstimationAttributeVector(const QVector<double> &value)
{
    estimationAttributeVector = value;
}

QStringList AnalogyMethodParameters::getAttributeTypes() const
{
    return attributeTypes;
}

void AnalogyMethodParameters::setAttributeTypes(const QStringList &value)
{
    attributeTypes = value;
}

QStringList AnalogyMethodParameters::getProjectDescriptions() const
{
    return projectDescriptions;
}

void AnalogyMethodParameters::setProjectDescriptions(const QStringList &value)
{
    projectDescriptions = value;
}

double MMRE::evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethodParameters &par, AnalogyMethodParameters &bestPar)
{
    QVector<double> diffvec(estimations.count()), mrevec(estimations.count());
    std::transform(estimations.begin(), estimations.end(), realValues.begin(), diffvec.begin(), std::minus<double>());
    std::for_each(diffvec.begin(), diffvec.end(), [](double &val) {
        val = std::abs(val);
        return val;
    });

    std::transform(diffvec.begin(), diffvec.end(), realValues.begin(),  mrevec.begin(), std::divides<double>());

    double mmre = qMeanValue(mrevec);
    if (bestPar.getBestEvaluation() > mmre || bestPar.getBestEvaluation() == -1) {
        bestPar.setBestEvaluation(mmre);
        bestPar.setStatistic(par.getStatistic());
        bestPar.setDistanceMetric(par.getDistanceMetric());
        bestPar.setAnalogies(par.getAnalogies());
        bestPar.setSubset(par.subset());
    }

    return mmre;
}

double MDMRE::evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethodParameters &par, AnalogyMethodParameters &bestPar)
{
    QVector<double> diffvec(estimations.count()), mrevec(estimations.count());
    std::transform(estimations.begin(), estimations.end(), realValues.begin(), diffvec.begin(), std::minus<double>());
    std::for_each(diffvec.begin(), diffvec.end(), [](double &val) {
        val = std::abs(val);
        return val;
    });

    std::transform(diffvec.begin(), diffvec.end(), realValues.begin(),  mrevec.begin(), std::divides<double>());

    double mdmre = qMedianValue(mrevec);

    if (bestPar.getBestEvaluation() > mdmre || bestPar.getBestEvaluation() == -1) {
        bestPar.setBestEvaluation(mdmre);
        bestPar.setStatistic(par.getStatistic());
        bestPar.setDistanceMetric(par.getDistanceMetric());
        bestPar.setAnalogies(par.getAnalogies());
        bestPar.setSubset(par.subset());
    }

    return mdmre;
}

double PRED25::evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethodParameters &par, AnalogyMethodParameters &bestPar)
{
    QVector<double> diffvec(estimations.count()), mrevec(estimations.count());
    std::transform(estimations.begin(), estimations.end(), realValues.begin(), diffvec.begin(), std::minus<double>());
    std::for_each(diffvec.begin(), diffvec.end(), [](double &val) {
        val = std::abs(val);
        return val;
    });

    std::transform(diffvec.begin(), diffvec.end(), realValues.begin(),  mrevec.begin(), std::divides<double>());

    int mreLessThanPredPercent = 0;
    auto countMreLessThanPredPercent = [&](double mre) {
        if(mre <= 0.25) mreLessThanPredPercent++;
    };

    std::for_each(mrevec.begin(), mrevec.end(), countMreLessThanPredPercent);

    double pred25 = mreLessThanPredPercent/(double)mrevec.count();

    if (bestPar.getBestEvaluation() < pred25 || bestPar.getBestEvaluation() == -1) {
        bestPar.setBestEvaluation(pred25);
        bestPar.setStatistic(par.getStatistic());
        bestPar.setDistanceMetric(par.getDistanceMetric());
        bestPar.setAnalogies(par.getAnalogies());
        bestPar.setSubset(par.subset());
    }

    return pred25;
}

EvaluationMethodFactory::EvaluationMethodFactory(const QString &evalmethodstr, const QString &estimationAttr, const QStringList &prjDescs, const QVector<QStringList > &prjVec, const QVector<double> estimAttrVec, const QStringList &attrs, const QStringList &attrTypes)
{
    if(evalmethodstr == "MMRE") {
        method = std::unique_ptr<EvaluationMethod>(new MMRE);
    } else if(evalmethodstr == "MDMRE") {
        method = std::unique_ptr<EvaluationMethod>(new MDMRE);
    } else if(evalmethodstr == "PRED25") {
        method = std::unique_ptr<EvaluationMethod>(new PRED25);
    }
    bestPar.setProjectDescriptions(prjDescs);
    bestPar.setProjectsMatrix(prjVec);
    bestPar.setAttributes(attrs);
    bestPar.setAttributeTypes(attrTypes);
    bestPar.setEvaluationMethod(evalmethodstr);
    bestPar.setEstimationAttribute(estimationAttr);
    bestPar.setEstimationAttributeVector(estimAttrVec);
}

double EvaluationMethodFactory::evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethod &jack)
{
    AnalogyMethodParameters par;
    par.setStatistic(jack.getEstimatorName().c_str());
    par.setDistanceMetric(jack.getDistanceMetricName().c_str());
    par.setAnalogies(jack.getAnalogies());
    par.setSubset(jack.getWeightsPair());
    return method->evaluate(estimations, realValues, par, bestPar);
}
