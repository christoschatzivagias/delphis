#include "CSVReader.h"
#include "Timer.hpp"
#include <QFutureWatcher>
#include <QtConcurrent/QtConcurrent>

CSVReader::CSVReader()
{
}

void CSVReader::ReadCSV(const QString &filename, QVector<QStringList> &vectors)
{
    QFile file(filename);
    QString lineend="";
	QMutex m;
    int numlines=0;
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream in(&file);
        QString line = in.readAll(); //read one line at a time
        QStringList strings1 = line.split("\n");
        QStringList strings2 = line.split("\r");
        QStringList strings3 = line.split("\r\n");
        QStringList strings;

        if(strings3.length() > 1) {
            numlines=strings3.length();
            lineend="\r\n";
            strings=strings3;
        } else if(strings1.length() > 1) {
            numlines=strings1.length();
            lineend="\n";
            strings=strings1;
        } else if(strings2.length() > 1) {
            numlines=strings2.length();
            lineend="\r";
            strings=strings2;
        }

        vectors.resize(numlines);

		int line_index = 0;
		auto readCSVLinesVector = [&](QString &line) {
            vectors[line_index] = line.split(",");
			line_index++;
		};

        std::for_each(strings.begin(), strings.end(), readCSVLinesVector);

    }
    file.close();
}
