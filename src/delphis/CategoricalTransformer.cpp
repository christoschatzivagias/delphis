#include "CategoricalTransformer.hpp"



CategoricalTransformer::CategoricalTransformer(DataBase *db)
{
    database = db;
    QSqlQuery qr(database->getDb());
    qr.exec("select Description from Attributes where Type=2");
    qr.first();

    qDebug() << qr.lastError();
    do {
        QSqlQuery qr1(database->getDb());
        QString attrName = qr.value(0).toString();
        qr1.exec("select " + attrName + " from Projects;");

        QSet<QString> ss;
        while(qr1.next()) {
            bool ok = false;
            ss.insert(qr1.value(0).toString());
        }

        attributesValues.push_back(QPair<QString, QSet<QString> >(attrName, ss));
    } while(qr.next());
}

QVector<QPair<QString, QSet<QString> > > CategoricalTransformer::getAttributesValues() const
{
    return attributesValues;
}
