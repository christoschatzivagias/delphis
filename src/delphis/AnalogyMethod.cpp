#include "Enums.h"
#include "DistanceMetrics.hpp"
#include "AnalogyMethod.hpp"
#include "Estimators.hpp"
#include "DataBase.hpp"
#include "CommonFunctionsContainer.hpp"
#include <vector>
#include <cstdio>
#include <cmath>
#include <map>


void AnalogyMethod::calculateDistances(const QVector<QPair<int, QVector<double> > > &matrix, const QVector<double> &prj, const QVector<double> &weights, QVector<QPair<int, double> > &distances) const
{
    distances.reserve(matrix.count());
    double distanceVal = 0;
    auto calculateDistance = [&] (const QPair<int, QVector<double> > &itprj) {
        distanceVal = distanceMetric->calculate(prj.toStdVector(), itprj.second.toStdVector(), weights.toStdVector());
        distances.push_back(QPair<int, double>(itprj.first, distanceVal));
    };

    std::for_each(matrix.begin(), matrix.end(), calculateDistance);


    std::sort(distances.begin(), distances.end(), [](const QPair<int, double> &left, const QPair<int, double> &right) {
        return left.second < right.second;
    });

}

QVector<QPair<QString, double> > AnalogyMethod::getWeightsPair() const
{
    return weightsPair;
}
unsigned int AnalogyMethod::getAnalogies() const
{
    return analogies;
}

AnalogyMethod::AnalogyMethod(const QVector<QPair<QString, double> > &weightsPair,const Distance *dst, const Estimator *est, unsigned int anlg)
{
    distanceMetric = dst;
    estimator = est;
    analogies = anlg;
    this->weightsPair = weightsPair;
}

double AnalogyMethod::estimate(const QVector<QPair<int, QVector<double> > > &matrix, const QVector<QPair<int, QVector<double> > > &stdmatrix, const QVector<double> &prj, const QVector<double> &weights, const QVector<double> &estimVec) const
{
    QVector<QPair<int, double> > distances;
    calculateDistances(stdmatrix, prj, weights, distances);


    QVector<double> minEstimVec;
    QVector<QVector<double> > trainMatrix;
    trainMatrix.reserve(this->analogies);
    minEstimVec.reserve(this->analogies);
    auto createEstimationVector = [&] (const QPair<int,double> &distanceMetric) {
        minEstimVec.push_back(estimVec[distanceMetric.first]);
        trainMatrix.push_back(matrix[distanceMetric.first].second);
    };


    std::for_each(distances.begin(), distances.begin() + analogies, createEstimationVector);


    double value = estimator->estimate(minEstimVec, trainMatrix);
#ifdef TEST_PRINT
    qDebug() << "Sorted Distances:" << distances;

    qDebug()<< "TrainMatrix: " << trainMatrix;
    qDebug()<< "EstimVector: " << minEstimVec;
    qDebug()<< "Estimation: " << value;
#endif
    return value;
}

std::string AnalogyMethod::getEstimatorName() const
{
    if(estimator == nullptr)
        return std::string();
    return estimator->getName();
}

std::string AnalogyMethod::getDistanceMetricName() const
{
    if(distanceMetric == nullptr)
        return std::string();

    return distanceMetric->getName();
}


