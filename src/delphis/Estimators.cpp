#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include "Estimators.hpp"
#include <QCoreApplication>
#include "CommonFunctionsContainer.hpp"


bool ComparisonFunction(double a,double b)
{
    return (a < b);
}

EstimatorsContainer *EstimatorsContainer::instancePtr = nullptr;


EstimatorsContainer::EstimatorsContainer(const QStringList &estimatorsStrList) : QObject(QCoreApplication::instance())
{
    Estimators = {
       new Mean(this),
       new Median(this),
       new FibonacciSerie(this)
    };
    auto checkAndAddEstimator = [&] (const QString &name) {
        auto insertEstimatorWithName = [&] (const Estimator *est) {
            if(name.toStdString() == est->getName())
                CurrentEstimators.push_back(est);
        };

        std::for_each(Estimators.begin(), Estimators.end(), insertEstimatorWithName);
    };

    std::for_each(estimatorsStrList.begin(), estimatorsStrList.end(), checkAndAddEstimator);
}

EstimatorsContainer::~EstimatorsContainer()
{

}

const std::vector<const Estimator *> &EstimatorsContainer::getCurrentEstimators() const
{
    return CurrentEstimators;
}

const Estimator *EstimatorsContainer::getEstimatorByName(const QString &name) const
{
    auto itret = find_if(Estimators.cbegin(), Estimators.cend(), [&] (const Estimator *est) {
        return name == est->getName().c_str();
    });
    return *itret;
}

Mean::Mean(QObject *parent) : Estimator(parent)
{
    name = "Mean";
}

double Mean::estimate(const QVector<double> &values, const QVector<QVector<double> > &matrix) const
{
    return mean(values.toStdVector());
}

Median::Median(QObject *parent) : Estimator(parent)
{
    name = "Median";
}

double Median::estimate(const QVector<double> &values, const QVector<QVector<double> > &matrix) const
{
    return median(values.toStdVector());
}

FibonacciSerie::FibonacciSerie(QObject *parent) : Estimator(parent)
{
    name = "FibonacciSerie";
}

double FibonacciSerie::estimate(const QVector<double> &values, const QVector<QVector<double> > &matrix) const
{
    double estimation = 0;
    int index = 0;
    if(!values.empty()) {

        QVector<int> fiboVec = fibonacciGenerator(values.count() + 2);
        std::reverse(fiboVec.begin(), fiboVec.end());
        fiboVec.remove(fiboVec.count() - 2, 2);


        int fibonacciSum = std::accumulate(fiboVec.begin(), fiboVec.end(), 0.0);

        auto calculateFibonacciEstimation = [&] (double value) {
            estimation += value * fiboVec.at(index);
            index++;
        };

        std::for_each(values.begin(), values.end(), calculateFibonacciEstimation);

        estimation = estimation/fibonacciSum;
    }
    return estimation;
}

Estimator::Estimator(QObject *parent) : QObject(parent)
{

}
