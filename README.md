# Abstract
In the IT industry, precisely estimate the effort of each software project the development cost and schedule are count for much to the software company. So precisely estimation of man power seems to be getting more important. In the past time, the IT companies estimate the work effort of man power by human experts, using statistics method. However, the outcomes are always unsatisfying the management level. Recently it becomes an interesting topic if computing intelligence techniques can do better in this field. Estimation by Analogy is a popular method in the field of software cost estimation. However, the configuration of the method affects estimation accuracy,which has a great effect on project management decisions.
# Introduction
Analogy based estimation has recently emerged as a promising approach, with comparable accuracy to algorithmic methods in some studies, and it is potentially easier to understand and apply. Analogy Based Software Estimation is based on the principle that actual values achieved within the organization in an earlier and similar project are better indicators and predict the future project performance much better than an estimate developed afresh from scratch. It also facilitates bringing the organizational experience to bear on the new projects.



# Omen
**Omen** loads and shows **csv** and **arrf** files by detecting the attributes of the records. It detects the attribute values and classifies the attributes according to their values whether are numerical or categorical.It also has the function to convert the categorical values into numerical ones and to fill in the missing values.

## How to use

**Omen** can be used for importing a **csv** or **arrf** file. When database is loaded the user can select which of the attributes or projects could be active or not.
If there are **missing values**, they must be completed with a specific statistic method or you can set the attribute or the project with the missing value as **inactive**.
If there is any project with missing value it must be filled first in order to proceed to best analogy method search.User must also convert the categorical value to negative
numeric in order to be readable by __Anex__.__Anex__ imports the exported configuration and search for the most suitable method to the imported dataset, __Anex__ exports
a file with the parameters of the best evaluated method which can be imported the __Omen__ and estimate a new project


#### Importing Files
You can import **csv** and **arff** files
1. _File -> Import_ csv File (Warning temporarely you must have a header in the first line for the attributes names).
2. _File -> Import_ arff File.


#### What you see
The are 3 tabs on **Omen** the Projects tab, the Attributes tab and the Calculation Parameters tab

* _Projects tab_
    Displays the projects of the loaded database, their status if it is active or not.
* _Attributes tab_ 
    Displays the attributes of the loaded database, their type (Numeric or Categorical) and their status if it is active or not.
* _Calculation Parameters tab_
    Displays the target projects and the loaded trained models from anex, the user can select a method and apply it to the target projects.

#### Database Functionality
* _Fill Missing Values_ - (Action -> Value Completer)
    * Detects database's missing values
    * Fills database's missing values
        * Zero
        * Mean
        * Median
        * Mode (the value observed most)
    
* _Outlier Detection_ - (Action -> Outlier Detection)
    * Select a specific method 1.Distance Metric, 2.Estimator, 3.Analogies 4.Attribute Weights removes each Project and calculate evaluation metric selected. 
      If evaluation metric improves with the **X Project** absence then **X Project** maybe an outlier.

# Anex
**Anex** can be used for the search of the best analogy method, it takes as input the configuration file exported by **Omen**.
**Anex** takes as parameter the configuration file and exports a file with the *best analogy method* found.
Analogy method consists of main 3 parameters:
    
#### __Distance Metric__ 
The metric which used to find the n nearest projects to the target.

* [Euclidean](https://en.wikipedia.org/wiki/Euclidean_distance)
* [Manhattan](https://en.wikipedia.org/wiki/Taxicab_geometry)
* [Minowski](https://en.wikipedia.org/wiki/Minkowski_distance)
* [Canberra](https://en.wikipedia.org/wiki/Canberra_distance)
* [Czekanowski]()
* [Chebyshev](https://en.wikipedia.org/wiki/Chebyshev_distance)

#### __Analogies__ 
The multitude of the nearests projects will be used to estimate the target.
   
#### __Estimator__ 
The calculation method used to estimate the target.

* [Mean](https://en.wikipedia.org/wiki/Mean)
* [Median](https://en.wikipedia.org/wiki/Median)
* [Fibonacci Serie](https://en.wikipedia.org/wiki/Fibonacci_number)
 
#### __Weights of attributes__ 
The factors of attributes involved in the calculation of n nearest projects

**Anex** uses jackknife method for cross-validation to evaluate each method. Every method is evaluated by the evaluation method selected by __Omen__
* Mean [MRE](https://en.wikipedia.org/wiki/Approximation_error)
* Median [MRE](https://en.wikipedia.org/wiki/Approximation_error)
* Pred25 is the percentage of provisions that deviate less than 25% from the actual value

__Anex__ finds the best analogy method and exports a file with the method paramaters and it's evaluation after that operation exports an method ini file with the method parameters.
Method ini file can be imported to __Omen__ and apply the selected method to each project in the Targets table and displays the estimate results.

_File -> import_ method, to import the selected method to __Omen__ database.

## What is Estimation by Analogy

Estimating by analogy means comparing the proposed project to previously completed similar project where the project development information id known. Actual data from the completed projects are extrapolated to estimate the proposed project. This method can be used either at system-level
or at the component-level.Estimating by analogy is relatively straightforward. Actually in some respects, it is a systematic form of expert judgment since experts often search for analogous situations so as to inform their opinion.

The steps using estimating by analogy are:
1. Characterizing the proposed project .
2. Selecting the most similar completed projects whose characteristics have been stored in the historical data base using the __Distance Metric__.
3. Deriving the estimate for the proposed project from the __n__  most similar completed projects (__Analogies__) by analogy using the __Estimator__.

The main advantages of this method are:
* The estimation are based on actual project characteristic data.
* The estimator's past experience and knowledge can be used which is not easy to be quantified.
* The differences between the completed and the proposed project can be identified and impacts estimated.

However there are also some problems with this method.
Using this method, we have to determine how best to describe projects. The choice of variables must be restricted to information that is available at the point that the prediction required. Possibilities include the type of application domain, the number of inputs, the number of distinct entities referenced, the number of screens and so forth.
Even once we have characterized the project, we have to determine the similarity and how much confidence can we place in the analogies. Too few analogies might lead to maverick projects being used; too many might lead to the dilution of the effect of the closest analogies. Martin Shepperd etc. introduced the method of finding the analogies by measuring Euclidean distance in n-dimensional space where each dimension corresponds to a variable. Values are standardized so that each dimension contributes equal weight to the process of finding analogies. Generally speaking, two analogies are the most effective.
Finally, we have to derive an estimate for the new project by using known effort values from the analogous projects. Possibilities include means and weighted means which will give more influence to the closer analogies.

It has been estimated that estimating by analogy is superior technique to estimation via algorithmic model in at least some circumstances. It is a more intuitive method so it is easier to understand the reasoning behind a particular prediction.

### Jackknife
In statistics, the jackknife is a resampling technique especially useful for variance and bias estimation. The jackknife predates other common resampling methods such as the bootstrap. The jackknife estimator of a parameter is found by systematically leaving out each observation from a dataset and calculating the estimate and then finding the average of these calculations.
Given a sample of size **n**, the jackknife estimate is found by aggregating the estimates of each **(n-1)**-sized sub-sample.



## Build & Deploy

### Prerequisites
* [Qt5](http://doc.qt.io/qt-5/index.html) - The GUI framework used
* [CMake](https://cmake.org/) - Build System
* [GCC](https://gcc.gnu.org/) - Linux compiler
* [Microsoft Visual Studio 2017](https://www.visualstudio.com/) - Windows Development Platform

### Build Linux
```
cd omen

mkdir build

cd build

cmake ../

make (or ninja)
```

### Build Windows
```
cd omen

mkdir build

cd build

cmake -G"Visual Studio 15 Win64" ..\

cmake --build .
```

<!--## Example run-->


<!--What things you need to install the software and how to install them-->

<!--```-->
<!--Give examples-->
<!--```-->

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Qt5](http://doc.qt.io/qt-5/index.html) - The GUI framework used
* [CMake](https://cmake.org/) - Build System
* [GCC](https://gcc.gnu.org/) - Linux compiler
* [Microsoft Visual Studio 2017](https://www.visualstudio.com/) - Windows Development Platform


## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors

* **Christos Chatzivagias** - *Initial work* - [christoschatzivagias](https://gitlab.com/christoschatzivagias)

See also the list of [contributors](https://gitlab.com/christoschatzivagias/delphis/graphs/master) who participated in this project.

## License

This project is licensed under the Simple Non Code License (SNCL) Version 2.1.0 - see the [LICENSE.md](LICENSE.md) file for details

