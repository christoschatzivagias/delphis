#pragma once

#include <memory>

#include <QMainWindow>
#include <QAction>
#include <QPointer>
//#include "TabArea.hpp"
#include "AsciiFileManager.hpp"
#include <QCoreApplication>
#include <cassert>
class QProgressBar;
class QStatusBar;
class MainMenuBar;
class MainToolBar;
class QFileDialog;
class BestParametersDialog;
class TabArea;
class MainWindow : public QMainWindow
{
    Q_OBJECT
public :
    MainWindow(QWidget *wid = nullptr, Qt::WindowFlags = Qt::WindowFlags());
    ~MainWindow();
public slots:
    void CreateFileChooserDialogSlot();
    void CreateCSVImportFileDialogSlot();
    void CreateARFFImportFileDialogSlot();
    void CreateFileSaverDialogSlot();
    void CreateARFFExportFileDialogSlot();
    void CreateMethodImporterDialogSlot();
    void CreateCSVTargetsImportFileDialogSlot();

    int NewDataBaseSlot();
    void LoadAttrsAndProjectsAscii(const QString &);
    void LoadAttrsAndProjectsCSV(const QString &);
    void LoadAttrsAndTargetsCSV(const QString &);

    void LoadAttrsAndProjectsArff(const QString &);
    void MissingValueScanSlot();

    void ImportMethodIni(const QStringList &Files);

    void CloseSlot();

    static MainWindow *instance();

    void setProgressValue(int value);

protected:
    virtual void closeEvent(QCloseEvent *event) ;
    static MainWindow *instancePtr;

signals:
    void CopySelectedProjectsToTargetsSignal();
    void ActivateSelectedProjectsSignal();
    void DeactivateSelectedProjectsSignal();
    void ActivateSelectedTargetsSignal();
    void DeactivateSelectedTargetsSignal();
    void ActivateSelectedAttributesSignal();
    void DeactivateSelectedAttributesSignal();

private :
    MainMenuBar *mp_mainMenuBar = nullptr;
    MainToolBar *mp_mainToolBar = nullptr;
    TabArea *mp_tabWidget = nullptr;
    QFileDialog *mp_chooseFileDialog = nullptr;
  //  QFileDialog *mp_methodImporterDialog = nullptr;

  //  QFileDialog *mp_saveFileDialog = nullptr;
  //  QFileDialog *mp_exportFileDialog = nullptr;
    QStatusBar *mp_statusBar = nullptr;
    QProgressBar *mp_progressBar = nullptr;
    BestParametersDialog *mp_bestParametersDialog = nullptr;
    std::unique_ptr<AsciiFileManager> mp_asciiFMan = std::make_unique<AsciiFileManager>();
    QString m_loadFileName;
    bool m_fileSaved;
};




class ActionsContainer : public QObject
{
public:
    virtual ~ActionsContainer(){}
    static ActionsContainer *instance()
    {
        if(!instancePtr)
            instancePtr = new ActionsContainer;

        return instancePtr;
    }

    QAction *getActionByName(const QString &name)
    {
        QHash<QString, QAction*>::iterator it = m_actions.find(name);

        assert(it != m_actions.end());

        return it.value();
    }

private:
    static ActionsContainer *instancePtr;

    ActionsContainer();
    QHash<QString, QAction*> m_actions = {
        { "Load",                    new QAction(QIcon(":/sources/delphis_icons/omen_project_open.svg"), "&Load", this) },
        { "Save",                    new QAction(QIcon(":/sources/delphis_icons/omen_save.svg"), "&Save", this) },
        { "ImportARFF",              new QAction(QIcon(":/sources/delphis_icons/omen_file_import_aarf.svg"), "Import ARFF File", this) },
        { "ImportCSV",               new QAction(QIcon(":/sources/delphis_icons/omen_file_import_CSV.svg"), "Import CSV File", this) },
        { "ImportMethod",            new QAction(QIcon(":/sources/delphis_icons/omen_estimation_import.svg"),"Import Method", this) },
        { "CopyProjectsToTargets",   new QAction(QIcon(), "Copy Selected Projects to Targets", this) },
        { "ActivateProjects",        new QAction(QIcon(), "Activate Selected Projects", this) },
        { "DeactivateProjects",      new QAction(QIcon(), "Deactivate Selected Projects", this) },
        { "ActivateTargets",         new QAction(QIcon(), "Activate Selected Targets", this) },
        { "DeactivateTargets",       new QAction(QIcon(), "Deactivate Selected Targets", this) },
        { "ActivateAttributes",      new QAction(QIcon(), "Activate Selected Attributes", this) },
        { "DeactivateAttributes",    new QAction(QIcon(), "Deactivate Selected Attributes", this) },
        { "Font",                    new QAction(QIcon(":/sources/font.png"), "Font", this) },
        { "Exit",                    new QAction(QIcon(":/sources/exit.png"), "&Exit", this) },
        { "ImportCSVTargets" ,       new QAction(QIcon(":/sources/delphis_icons/omen_import.svg"),"Import Targets CSV", this) },
     //   { "BestParameterEstimation", new QAction(QIcon(":/sources/estimation1.png"), "Best Parameters Estimation", this) },
        { "ValueCompleter",          new QAction(QIcon(":/sources/fillvalues.png"), "Value Completer", this) },
        { "Contents",                new QAction(QIcon(":/sources/contents.png"), "Contents", this) },
        { "About",                   new QAction(QIcon(":/sources/about.png"), "About", this) },
        { "OutlierDetector",         new QAction(QIcon(":/sources/delphis_icons/omen_outlier_detection.svg"), "Outlier Detector", this) },
        { "AddTarget",               new QAction(QIcon(":/sources/delphis_icons/omen_project_add.svg"), "Add Target", this) },
        { "ExportResults",           new QAction(QIcon(":/sources/delphis_icons/omen_export.svg"), "Export Results", this) }


    };
};
