#pragma once

#include <QFrame>
#include <QTreeView>
#include <QFile>
#include <QTextStream>
#include <QTabWidget>

#include <stdio.h>
#include <vector>
#include "MethodModel.hpp"


class QPushButton;
class QHBoxLayout;
class QVBoxLayout;
class QGroupBox;
class QComboBox;
class QCheckBox;
class QSpinBox;
class QRadioButton;
class QLabel;
class QScrollArea;
class ProjectsTableView;
class MainWindow;
class QSplitter;
class QToolBar;
class TabArea;

class MethodProjectsTreeView: public QTreeView
{
public:
    MethodProjectsTreeView (QWidget *parent = nullptr);
    void setCurrentMethodProjectsTable(QTreeView *value);
    void setCurrentMethodEstimationAttributeTable(QTreeView *value);

protected:
    QTreeView *CurrentMethodProjectsTable = nullptr;
    QTreeView *CurrentMethodEstimationAttributeTable = nullptr;

    void currentChanged(const QModelIndex &current, const QModelIndex &previous);
};


class ResultExporter : QObject
{
public:
    ResultExporter();

    void exportResultModel(const QStandardItemModel *md, const QString &filePath);
};

class CalculationParametersTab: public QTabWidget
{
    Q_OBJECT

public:
    CalculationParametersTab(TabArea *parent = nullptr);

    QList<int> getActiveProjectsIndices(QStandardItemModel *ProjectsModel);

public slots:
    void calculate();
    void addTarget();

    static void exportResults();
private:

    QFrame *mp_mainFrame = new QFrame(this);
    QSplitter *mp_mainSplitter = nullptr;
    QFrame *mp_methodFrame = nullptr;
    QFrame *mp_targetResultsFrame = nullptr;
    QVBoxLayout *mp_methodFrameLayout = nullptr;
    ProjectsTableView *mp_targetProjectsTable = nullptr;
    QTreeView *mp_resultsTable = nullptr;
    QToolBar *mp_toolbar = nullptr;
    MethodProjectsTreeView *mp_methodsTable = nullptr;
    QTreeView *mp_currentMethodProjectsTable = nullptr;
    QTreeView *mp_currentMethodEstimationAttributeTable = nullptr;
    QHBoxLayout *mp_methodOptionsLayout = nullptr;
    static QStandardItemModel m_resultsModel;
    QStringList m_resultsModelColums = {"OMEN_ID", "Target_Description", "Estimation"};

};
