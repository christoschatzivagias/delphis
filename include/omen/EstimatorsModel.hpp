#pragma once

#include <QStandardItemModel>


class EstimatorsModel : public QStandardItemModel
{
    public:
        EstimatorsModel(QObject *parent = nullptr);
        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

        QStringList getSelectedEstimators() const;
    private:
        std::vector< std::pair<std::string, bool> > Estimators = {
                                                                       {"Mean",               true},
                                                                       {"Median",             true},
                                                                       {"FibonacciSerie",   true}
                                                                    //   {"SizeAdjustedMedian", true}
                                                                      };
};
