#pragma once

#include <vector>
#include <QCompleter>
#include <QSortFilterProxyModel>
#include <QFrame>
#include "Project.hpp"
#include <QLineEdit>
#include <QTableView>

#include <QCoreApplication>
#include <QDebug>

class QTableView;
class QToolBar;
class QLineEdit;
class QCompleter;
class MainWindow;
class ProjectsModel;
class ProjectsTableView;
class TabArea;
class Completer:public QCompleter
{
Q_OBJECT
public:
    explicit Completer(QStringList stringList, QObject *parent=0);
    virtual QString pathFromIndex(const QModelIndex &index)const;
    virtual QStringList splitPath(const QString&)const;

public slots:
    void onLineEditTextChanged();
private:
    mutable int cursorPos_;
};

class ExpressionLineEdit: public QLineEdit
{
    Q_OBJECT
    public:
        explicit ExpressionLineEdit(QWidget *parent=0);
private:
    QStringList stringList;
    Completer *completer_;
};

class ProjectsTab: public QFrame
{
	Q_OBJECT
	public:
        ProjectsTab(TabArea *parent = nullptr);
        ~ProjectsTab();

		void init();

	private:
        ProjectsTableView *ProjectTable = nullptr;
        QToolBar *toolbar = nullptr;
        ExpressionLineEdit *kle = nullptr;
        QCompleter *cm = nullptr;
};
