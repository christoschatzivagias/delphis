#pragma once
#include <QDialog>

class QHBoxLayout;
class QLabel;
class QTextEdit;

class AboutWindow : public QDialog
{
	public:
                AboutWindow(QWidget *parent = nullptr);
                void init();

    private:
                QHBoxLayout *mp_mainLayout = nullptr;
                QLabel *mp_iconLabel = nullptr;
                QTextEdit *mp_textLabel = nullptr;

                QString m_license = "Simple Non Code License (SNCL)	Version 2.1.0";
                QString m_aboutText =  "<p><h2>" + qApp->applicationName() + " " + qApp->applicationVersion() + "<h2></p>" +
                                       "<p><h3>License</h3>" + m_license + "</p>" +
                                       "<p><h3>Development</h3>Christos Chatzivagias &lt;chatzich@gmail.com&gt;</p>" + 
									   "<p><h3>Design</h3>Nikos Kostakis &lt;nikos_kostakis@yahoo.gr&gt;</p>";
};
