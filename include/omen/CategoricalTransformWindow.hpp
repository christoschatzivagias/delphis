#pragma once
#include <QDialog>
#include <QStandardItemModel>
#include <memory>
#include "CategoricalTransformer.hpp"

class QVBoxLayout;
class QTableView;
class QDialogButtonBox;
class QStatusBar;


class CategoricalTransformerWindow : public QDialog
{
    Q_OBJECT

public:
    CategoricalTransformerWindow(QWidget *parent);

protected slots:
    void transformCategoricalValues();

private:
    std::unique_ptr<CategoricalTransformer> categoricalTransformer = std::make_unique<CategoricalTransformer>(DataBase::instance());
    QStandardItemModel model;
    QVBoxLayout *mainLayout = nullptr;
    QTableView *table = nullptr;
    QDialogButtonBox *dialogButtonBox = nullptr;
    QStatusBar *statusbar = nullptr;
};
