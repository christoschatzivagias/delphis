#pragma once
#include <QMenuBar>

class MainWindow;
class MainMenuBar : public QMenuBar
{
	public:
        MainMenuBar(MainWindow *parent = nullptr);
	private:
        QMenu *FileMenu = nullptr;
        QMenu *EditMenu = nullptr;
        QMenu *FontMenu = nullptr;
        QMenu *ActionMenu = nullptr;
        QMenu *HelpMenu = nullptr;
};
