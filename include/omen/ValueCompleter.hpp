#pragma once
#include "DataBase.hpp"
#include <QDialog>
#include <QItemDelegate>
#include <QComboBox>
#include <QStyledItemDelegate>
#include <QStandardItemModel>
#include <QStandardItem>
class QTableView;
class QVBoxLayout;
class QDialogButtonBox;
class QStatusBar;
class ComboBoxDelegate : public QItemDelegate
{
    Q_OBJECT

public:
    ComboBoxDelegate(QStandardItemModel *model);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:
    QStandardItemModel *model = nullptr;
};

class ValueCompleter
{
public:
    ValueCompleter(DataBase *db);
    void findMissingValues(QMap<QString, QVector<int> > &nullValues);
    QString fillAndUpdate(const QString &attrname, const QString &fillmethod);
    QVector<int> getNullValuesID(const QString &attrname);

private:
    void collectValidValues(const QString &attrname, QVector<double> &validValues);
    void collectValidValues(const QString &attrname, QVector<QString> &validValues);

    QMap<QString, QVector<int> > nullValues;
    DataBase *db = nullptr;
}; 


class ValueCompleterWindow : public QDialog
{
    Q_OBJECT

public:
    ValueCompleterWindow(QWidget *parent);
protected slots:
    void fillMissingValuesSlot();

private:
    std::unique_ptr<ValueCompleter> valueCompleter = std::make_unique<ValueCompleter>(DataBase::instance());
    QStandardItemModel model;
    QVBoxLayout *mainLayout = nullptr;
    QTableView *table = nullptr;
    QDialogButtonBox *dialogButtonBox = nullptr;
    QStatusBar *statusbar = nullptr;
};

