#pragma once

#include <QStandardItemModel>

class DistanceMetricsModel : public QStandardItemModel
{
    public:
        DistanceMetricsModel(QObject *parent = nullptr);

        bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

        QStringList getSelectedDistanceMetrics() const;

    private:
        std::vector< std::pair<std::string, bool> > DistanceMetrics = {
                                                                       {"Manhattan",          true},
                                                                       {"Euclidean",         true},
                                                                       {"Minkowski",         true},
                                                                       {"Canberra",          true},
                                                                       {"Czekanowski",       true},
                                                                       {"Chebychev",         true},
                                                                    //   {"Kaufmanrousseeuw", true}
                                                                      };
};
