#pragma once


#include <QFrame>


class QTableView;
class QToolBar;
class MainWindow;
class TabArea;

class AttributesTab: public QFrame
{
	Q_OBJECT
	public:
        AttributesTab(TabArea *parent = nullptr);

	private:
        QTableView *mp_attributesTable;
};
