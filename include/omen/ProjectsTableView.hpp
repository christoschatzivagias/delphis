#pragma once
#include <QTableView>
#include <QSortFilterProxyModel>
#include <QCoreApplication>

#include "DataBaseModel.hpp"


class ProjectsSortFilterProxyModel : public QSortFilterProxyModel
{
public:
    ProjectsSortFilterProxyModel() : QSortFilterProxyModel(QCoreApplication::instance()) {

    }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        return sourceModel()->headerData(section, orientation, role);
    }

    virtual bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
    {
        if(!source_right.isValid() || !source_right.isValid())
            return false;

        QString rightStr = sourceModel()->data(source_right).toString();
        QString leftStr  = sourceModel()->data(source_left).toString();

        bool leftOk = false, rightOk = false;

        double leftNum = leftStr.toDouble(&leftOk);
        double rightNum = rightStr.toDouble(&rightOk);

        if(leftOk && rightOk)
            return rightNum > leftNum;


        std::string new_rightstring = rightStr.toStdString().substr(0, rightStr.toStdString().find_first_of("0123456789"));
        std::string new_rightstring2 = rightStr.toStdString().substr(new_rightstring.size(), rightStr.toStdString().size() - 1);

        std::string new_leftstring = leftStr.toStdString().substr(0, leftStr.toStdString().find_first_of("0123456789"));
        std::string new_leftstring2 = leftStr.toStdString().substr(new_leftstring.size(), leftStr.toStdString().size() - 1);


        if(new_rightstring == new_leftstring)
            return QString::fromStdString(new_rightstring2).toDouble() > QString::fromStdString(new_leftstring2).toDouble();


        return QSortFilterProxyModel::lessThan(source_left, source_right);
    }
};



class ProjectsTableView : public QTableView
{
public:
    ProjectsTableView(QWidget *parent = nullptr);

    void setModel(ProjectsModel *pmd);
    void setModel(TargetsModel *tmd);

private:
    ProjectsSortFilterProxyModel *mp_sortFilterProxy = new ProjectsSortFilterProxyModel;

};
