#pragma once
#include <QDialog>
#include <QItemDelegate>
#include <QDoubleSpinBox>
#include <QFrame>
#include <vector>
#include <set>
#include "DistanceMetrics.hpp"
#include "EstimatorsModel.hpp"
#include "DistanceMetricsModel.hpp"
#include "DataBaseModel.hpp"
#include "AnalogyMethod.hpp"
#include "Estimators.hpp"

class QSpinBox;
class QProgressBar;
class QGroupBox;
class QGridLayout;
class QCheckBox;
class QRadioButton;
class QComboBox;
class QLabel;
class QVBoxLayout;
class QHBoxLayout;
class QScrollArea;
class QFileDialog;
class QTableView;
class QLineEdit;
class QStatusBar;
class QToolBar;
class QStackedWidget;
class TabArea;



class BestParametersDialog : public QFrame
{
    Q_OBJECT
public:
    BestParametersDialog(TabArea *parent);
    ~BestParametersDialog();


public slots:
    virtual void exportConfiguration() const;

protected:

    QSpinBox *AnalogiesStartBox = nullptr;
    QSpinBox *AnalogiesEndBox = nullptr;
    QComboBox *AttrToBeEstimatedComboBox = nullptr;
    QComboBox *SizeAdjustingAttrComboBox = nullptr;
    QVBoxLayout *mainLayout = nullptr;
    QHBoxLayout *centralLayout = nullptr;

    QAction *PlayAction = nullptr;
protected slots:
    void browse();
private:

    int getMinAnalogies() const;
    int getMaxAnalogies() const;
    std::string getSizeAdjustingAttribute() const;
    int getMinkowskiLamda() const;
    QString getEvaluationMetric() const;
    int getPredPercentage() const;
    QString getAttributeToBeEstimated() const;


    QComboBox *createComboBox(const QString &text);
    static void updateComboBox(QComboBox *comboBox);

    QToolBar *toolBar = nullptr;

    QVBoxLayout *leftLayout = nullptr;
    QVBoxLayout *rightLayout = nullptr;
    QGroupBox *DistanceMetricGroup = nullptr;


    QGroupBox *StatisticsGroup = nullptr;

    QGroupBox *MinkowskiLamdaGroup = nullptr;
    QSpinBox *MinkowskiLamdaBox = nullptr;


    QGroupBox *AnalogiesGroup = nullptr;

    QLabel *AnalogiesStartLabel = nullptr;
    QLabel *AnalogiesEndLabel = nullptr;

    QGroupBox *EvaluationsMetricGroup = nullptr;
    QRadioButton *MmreRadioButton = nullptr;
    QRadioButton *MdmreRadioButton = nullptr;
    QRadioButton *PredRadioButton = nullptr;
    QSpinBox *PredValueSpinBox = nullptr;
    QLabel *configFilesCounterLabel = nullptr;

    QLabel *SizeAdjustingAttrLabel = nullptr;
    QGroupBox *AttrToBeEstimatedGroup = nullptr;

    QVBoxLayout *AttributesLayout = nullptr;

    QTableView *distanceMetricView = nullptr;
    QTableView *estimatorsView = nullptr;

    QStringList AttrsNames;
    DistanceMetricsModel distanceMetricModel;
    EstimatorsModel estimatorModel;
    QFileDialog *SaveConfigFileDialog = nullptr;
    QComboBox *directoryComboBox = nullptr;
    QLineEdit *prefixLineEdit = nullptr;
    QComboBox *methodType = nullptr;

    QPushButton *browseButton = nullptr;
    QSpinBox *configFilesCounterSpinbox = nullptr;
    void updateAnalogiesBox();
};


