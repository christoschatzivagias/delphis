﻿#pragma once
#include <QFrame>
#include <QTableWidget>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QMatrix4x4>
#include <QToolTip>

#include <QtCharts/QChartView>
#include <QtCharts/QScatterSeries>

#include "CalculationParametersTab.hpp"
#include "ProjectsTab.hpp"
#include "AttributesTab.hpp"
#include "DataBase.hpp"

typedef QPair<QPointF, QString> Data;
typedef QList<Data> DataList;
typedef QList<DataList> DataTable;

class MainWindow;
class QMdiArea;
class QPaintEvent;
class QChart;
class BestParametersDialog;
class OmenScatterSeries : public QtCharts::QScatterSeries
{
public:
    OmenScatterSeries(QObject *parent = nullptr) : QtCharts::QScatterSeries(parent) {
        connect(this, &QScatterSeries::hovered, [&] (const QPointF &point, bool state) {
           QHash<QString, QString>::iterator it = m_labeledPoints.find(QString::number(point.x()) + ":" + QString::number(point.y()));
           if(it != m_labeledPoints.end()) {
                QToolTip::showText(QCursor::pos(), it.value());
           }
        });
    }

    void appendLabeledPoint(const QPointF &point, const QString &label) {
        m_labeledPoints.insert(QString::number(point.x()) + ":" + QString::number(point.y()) ,label);
        this->append(point);
    }

private:
    QHash<QString,QString> m_labeledPoints;
};

class OmenChart : public QtCharts::QChart {

public:
    OmenChart() : QtCharts::QChart() {

    }

    void setSerie(OmenScatterSeries *series) {
        this->QtCharts::QChart::removeAllSeries();
        this->QtCharts::QChart::addSeries(series);
    }

    const OmenScatterSeries *serie() {
        return dynamic_cast<OmenScatterSeries *>(this->QtCharts::QChart::series().first());
    }

};

class OmenChartView : public QtCharts::QChartView
{
public:
    OmenChartView(QtCharts::QChart *chart, QWidget *parent = nullptr):QtCharts::QChartView(chart,parent) {

    } 
};
class TabArea;
class ChartsTab : public QFrame
{
public:
    ChartsTab(TabArea *parent);

    OmenChart *createScatterChart(const DataList &list, const QString &attrX, const QString &attrY) const;

private:
    DataTable m_dataTable;
    QComboBox *mp_attributeComboBoxX = nullptr;
    QComboBox *mp_attributeComboBoxY = nullptr;

    QMdiArea *mp_mdiArea = nullptr;
};

class TabArea: public QTabWidget
{
    Q_OBJECT
    public:
        TabArea(MainWindow *parent);
    private:
        ProjectsTab *mp_projectsTab = nullptr;
        AttributesTab *mp_attributesTab = nullptr;
        BestParametersDialog *mp_methodSearchTab = nullptr;
        CalculationParametersTab *mp_calculationsParametersTab = nullptr;
        ChartsTab *mp_chartsBar = nullptr;

};

