#pragma once
#include <QStandardItemModel>
#include <QIcon>
#include "BestParameters.hpp"


class MethodItem : public QStandardItem
{
public:
    enum {
        MethodRole =  Qt::UserRole + 1
    };

    MethodItem(const QString &text);

    unsigned int getId() const;

private:
    unsigned int id = -1;
};



class MethodModel : public QStandardItemModel
{
    Q_OBJECT

public:
    MethodModel(QObject *parent = nullptr);

    void addMethod(const AnalogyMethodParameters &params);
    std::tuple<QStandardItemModel *, QStandardItemModel *> getMethodModels(unsigned int id);
    static QVector<std::tuple<AnalogyMethodParameters, QStandardItemModel *, QStandardItemModel *> > methods;

    static AnalogyMethodParameters getMethodParameters(unsigned int index);

public slots:
    void clearMethods();

private:
    QStandardItemModel *addProjectMatrixModel(const QStringList &attrNames, const QVector<QStringList > &projectMatrix);
    QStandardItemModel *addEstimationAttrModel(const QString &estimAttr,const QVector<double> &estimAttrVec);
    QStringList columns = { "Name", "Value", "Evaluation", "Apply" };

};
