#pragma once
#include <QDialog>
#include <QStandardItemModel>

#include "DistanceMetricsModel.hpp"
#include "EstimatorsModel.hpp"
#include "DataBaseModel.hpp"

class QComboBox;
class QSpinBox;
class QVBoxLayout;
class QHBoxLayout;
class QToolBar;
class QStackedWidget;
class QFrame;
class QGroupBox;
class QGridLayout;
class QRadioButton;
class QTableView;
class QLabel;
class QCheckBox;
class QStatusBar;

class AnalogyOutlierDialog:public QDialog
{
    Q_OBJECT
public:
    AnalogyOutlierDialog(QWidget *parent);

    ~AnalogyOutlierDialog(){}


    QComboBox *createComboBox(const QString &text);
    static void updateComboBox(QComboBox *comboBox);

protected:

    QSpinBox *AnalogiesBox = nullptr;
    QComboBox *AttrToBeEstimatedComboBox = nullptr;
    QVBoxLayout *mainLayout = nullptr;
    QHBoxLayout *centralLayout = nullptr;

    QAction *PlayAction = nullptr;

private:


    QString getEvaluationMetric() const;

    QToolBar *toolBar = nullptr;
    QLabel *methodsLabel = nullptr;
    QComboBox *methodsCombobox = nullptr;
    QComboBox *pagesComboBox = nullptr;

    QVBoxLayout *leftLayout = nullptr;
    QVBoxLayout *middleLayout = nullptr;
    QVBoxLayout *rightLayout = nullptr;
    QStackedWidget *stackWidget = nullptr;
    QFrame *parametersWidget = nullptr;
    QLabel *statisticsInfo = nullptr;
    QFrame *resultsFrame = nullptr;
    QVBoxLayout *resultsLayout = nullptr;

    QGroupBox *DistanceMetricGroup = nullptr;
    QVBoxLayout *DistanceMetricGroupLayout = nullptr;


    QGroupBox *StatisticsGroup = nullptr;
    QVBoxLayout *StatisticsGroupLayout = nullptr;


    QGroupBox *MinkowskiLamdaGroup = nullptr;
    QVBoxLayout *MinkowskiLamdaGroupLayout = nullptr;
    QSpinBox *MinkowskiLamdaBox = nullptr;


    QGroupBox *AnalogiesGroup = nullptr;
    QGridLayout *AnalogiesGroupLayout = nullptr;


    QGroupBox *EvaluationsMetricGroup = nullptr;
    QVBoxLayout *EvaluationsMetricGroupLayout = nullptr;
    QRadioButton *MmreRadioButton = nullptr;
    QRadioButton *MdmreRadioButton = nullptr;
    QRadioButton *PredRadioButton = nullptr;
    QSpinBox *PredValueSpinBox = nullptr;


    QGroupBox *ProjectAttributesGroup = nullptr;
    QVBoxLayout *ProjectAttributesGroupLayout = nullptr;
    QTableView *mp_outlierResultView = nullptr;
    QTableView *mp_attributesView = nullptr;
    QLabel *AttrToBeEstimatedLabel = nullptr;

    QGroupBox *ProjectAttributesGroupBox = nullptr;
    QVBoxLayout *AttributesLayout = nullptr;
    QCheckBox *DisabledAttribute = nullptr;

    QComboBox *distanceMetricView = nullptr;
    QComboBox *estimatorsView = nullptr;

    QStringList AttrsNames;
    QStatusBar *StatusBar = nullptr;
    DistanceMetricsModel distanceMetricModel;
    EstimatorsModel estimatorModel;
    QStandardItemModel m_outlierResultsModel;
    AttributesSelectionModel *attributesModel = nullptr;
};
