#pragma once

#include <QSqlRelationalTableModel>
#include "DataBase.hpp"
#include "MethodModel.hpp"

class DataBaseModel;
class AttributesModel: public QSqlRelationalTableModel
{
    Q_OBJECT
public:
    explicit AttributesModel(DataBase *database, QSqlDatabase db = QSqlDatabase());

    virtual void setTableConfig();
    QVariant data(const QModelIndex &idx, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void reset();


private:
    DataBase *mp_database = nullptr;
    std::vector<std::string> columns = {"ID", "Description", "Status", "Type"};
    const int STATUS_INDEX = 2;
    const int TYPE_INDEX = 3;
};

class AttributesSelectionModel: public QStandardItemModel
{

public:
    AttributesSelectionModel(DataBase *db, QObject *parent = nullptr);

    void setWeights(const QVector<QPair<QString, double> > &weights) {
        std::for_each(weights.begin(), weights.end(), [&](const QPair<QString, double> &weightPair){
            QList<QStandardItem *> items = this->findItems(weightPair.first);
            std::for_each(items.begin(), items.end(), [&](QStandardItem *item){
               QStandardItem *weightItem = this->item(item->row(),2);
               weightItem->setText(QString::number(weightPair.second));
            });
        });
    }

    QVector<QPair<QString, double> > getWeights() {

        QVector<QPair<QString, double> > weights;
        weights.reserve(rowCount());
        for(int index = 0; index < this->rowCount(); index++) {
            bool ok = false;
            double weight = item(index, 2)->text().toDouble(&ok);

            QString attrname = item(index, 0)->text();

            if(item(index, 1)->checkState() == Qt::Checked) {
                weights.push_back(QPair<QString, double> (attrname, weight));
            }
        }
        return weights;
    }

    QPair<QStringList, QList<int> > getSelectedAttributes();
    DataBase *database = nullptr;
};

class ProjectsModel: public QSqlTableModel
{
    Q_OBJECT
public:
    explicit ProjectsModel(DataBase *database, AttributesModel *attrsModel, QSqlDatabase db = QSqlDatabase());
    void reset();

protected:

    void setTableConfig();
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual QVariant data(const QModelIndex &idx, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    AttributesModel *attributesModel = nullptr;
    DataBase *mp_database = nullptr;
    std::vector<std::string> columns = {"ID", "Description", "Status"};
    const int STATUS_INDEX = 2;
};

class TargetsModel: public ProjectsModel
{
    Q_OBJECT
public:
    explicit TargetsModel(DataBase *database, AttributesModel *attrsModel, QSqlDatabase db = QSqlDatabase());
    void reset();

protected:
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);

    virtual QVariant data(const QModelIndex &idx, int role) const;
    void setTableConfig();
};

class DataBaseModel : public QObject
{
    Q_OBJECT
public:
    DataBaseModel(DataBase *database);
    AttributesModel *getAttributesModel() const;
    ProjectsModel *getProjectsModel() const;
    TargetsModel *getTargetsModel() const;
    MethodModel *getMethodModel() const;
	static DataBaseModel *instance();
	
public slots:
    void update();
    void resetAttributesModel();
    void resetProjectsModel();
    void resetTargetsModel();
    void addTarget();

private:
	static DataBaseModel *instancePtr;
    AttributesModel *attributesModel = nullptr;
    ProjectsModel *projectsModel = nullptr;
    TargetsModel *targetsModel = nullptr;
    MethodModel *methodModel = nullptr;
};
