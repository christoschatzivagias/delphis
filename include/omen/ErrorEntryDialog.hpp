#pragma once

#include <QWidget>

class QVBoxLayout;
class QLabel;
class QLineEdit;
class QPushButton;



class ErrorEntryDialog : public QWidget
{
	Q_OBJECT
	public:
        ErrorEntryDialog(QWidget *parent = nullptr, Qt::WindowFlags f = 0);
		void setErrorMessage(QString);
		QString getValidEntry();
	public slots:
		void SendEntryToDataBase();
	signals: 
		void SubmitValue(QString);
	private:
		QVBoxLayout *MainLayout;
		QLabel *ErrorMessage;
		QLineEdit *RightEntry;
		QPushButton *SubmitButton;
}; 
