#pragma once
#include <QString>
#include <QSettings>

#include "DataBase.hpp"
#include "CommonFunctionsContainer.hpp"

class Configuration
{
public:
    Configuration();

    void importConfig(const QString &file);
   // void exportConfig(const QString &file, const QString &evalMethod, const QString &estimAttr, const QString &sizeAdjustedAttr, QStringList attributes, int minAnalogies, int maxAnalogies, const QStringList &selectedDistanceMetrics, const QStringList &selectedEstimators);
    void exportConfig(const QString &path, const QString &prefix, const QString &evaluationMethod, const QString &estimAttr, const QString &sizeAdjustedAttr, const QStringList &activeAttributes, int minAnalogies, int maxAnalogies, const QStringList &selectedDistanceMetrics, const QStringList &selectedEstimators, int methodType = 0, unsigned int fileNum = 1);

    //void exportConfig(const QString &file, const QString &evalMethod, const QString &estimAttr,  const QString &sizeAdjustedAttr, QStringList attributes);
    void exportConfig(const QString &path, const QString &prefix, const QString &evalMethod, const QString &estimAttr, const QString &sizeAdjustedAttr,  const QStringList &activeAttributes, unsigned int fileNum = 1);

    int getMinAnalogies() const;
    void setMinAnalogies(int value);

    int getMaxAnalogies() const;
    void setMaxAnalogies(int value);

    QStringList getSelectedEstimators() const;
    void setSelectedEstimators(const QStringList &value);

    QStringList getSelectedDistanceMetrics() const;
    void setSelectedDistanceMetrics(const QStringList &value);

    QStringList getEstimAttrValsStr() const;
    void setEstimAttrValsStr(const QStringList &value);

    QVector<QStringList> getSubsets() const;
    void setSubsets(const QVector<QStringList> &value);

    QVector<double> getEstimAttrVals() const;
    void setEstimAttrVals(const QVector<double> &value);

    int getProjectsNum() const;
    int getAttrsNum() const;

    QVector<QStringList> getProjectsMatrix() const;

    QVector<QVector<QPair<QString, double> > > getSubsetWeights() const;


    QVector<double> getSizeAdjustedAttrVals() const;

    QString getEvaluationMethod() const;

    QStringList getAttributeNames() const;

    QString getEstimAttrName() const;

    QStringList getAttributeTypes() const;

    QStringList getProjectDescriptions() const;

private:
    int attrsNum = -1;
    int projectsNum = -1;
    int minAnalogies = -1;
    int maxAnalogies = -1;
    QStringList selectedEstimators;
    QStringList selectedDistanceMetrics;
    QString estimAttrName;
    QStringList estimAttrValsStr;
    QVector<QStringList> subsets;
    QVector<QVector<QPair<QString, double> > > subsetWeights;
    QStringList sizeAdjustedAttrValsStr;
    QStringList attributeNames;
    QStringList attributeTypes;
    QStringList projectDescriptions;

    QVector<double> sizeAdjustedAttrVals;
    QVector<double> estimAttrVals;
    QVector<QStringList> projectsMatrix;
    QString evaluationMethod;
};
