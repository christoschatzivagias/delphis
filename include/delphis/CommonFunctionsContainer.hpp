#pragma once


#include <string>
#include <vector>
#include <set>

#include <QString>
#include <QVector>
#include <QVariant>



int factorial(int n);
int findSubsetCombinationsCount(int n);
double apow(const float &x, int y);


double stod_(const std::string &strInput);
#if 0
bool caseInsensitiveStringCompare(const string& str1, const string& str2);
#endif
bool next_combination(const std::vector<std::string>& set, std::vector<std::string>& combo);
std::vector<bool> subset_tally(const std::vector<std::string>& set, const std::vector<std::string>& subset);
std::vector<std::string> generate_subset(const std::vector<std::string>& set, const std::vector<bool>& member);
bool battle_transform(std::vector<bool>& bitfield);
std::ostream& operator<<(std::ostream& out, const std::vector<std::string>& v);
double manhattan(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights);
double euklidean(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights);
double canberra(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights);
double chebychev(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights);
double minkowski(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights, int lamda);
double czekanowski(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights);
double kaufman_rousseeuw(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &weights, const std::vector<double> &ranges);

double manhattan(const std::vector<double> &point1, const std::vector<double> &point2);
double euklidean(const std::vector<double> &point1, const std::vector<double> &point2);
double canberra(const std::vector<double> &point1, const std::vector<double> &point2);
double chebychev(const std::vector<double> &point1, const std::vector<double> &point2);
double minkowski(const std::vector<double> &point1, const std::vector<double> &point2, int lamda);
double czekanowski(const std::vector<double> &point1, const std::vector<double> &point2);
double kaufman_rousseeuw(const std::vector<double> &point1, const std::vector<double> &point2, const std::vector<double> &ranges);
static char *integerToBinaryString(unsigned int val, char *buff, int sz);
int binaryStringToInteger(const char *str);
QVector< QVector<QString> > getAllSubsets(const QVector<QString> &set);
void transpose(const QVector<QVector<double> > data, QVector<QVector<double> > &result);
void standardizeVector(const QVector<double> &inputVec, QVector<double> &outputVec);
void numerifyMatrix(const QVector<QStringList> &matrix, const QList<int> &attributeTypes, QVector<QVector<double> > &num_matrix);
void numerifyVector(const QStringList &vec, const QList<int> &attributeTypes, QVector<double> &num_vec);
void numerifyIntVector(const QStringList &invec, QVector<int> &outvec);
QVector<int> getNumerifyIntVector(const QStringList &invec);
QVector<double> getNumerifyVector(const QStringList &vec, const QList<int> &attributeTypes);
QStringList getStringifyIntList(const QList<int> &lst);

void getMinMaxVectors(const QVector<QVector<double> > &matrix, QVector<double> &minVec, QVector<double> &maxVec);
void handleException(const std::exception &e, bool gui = true, const std::string &extra_message = std::string());
double qMeanValue(const QVector<double> &vec);
double qMedianValue(QVector<double> vec);
QString qModeValue(const QVector<double> &vec);
QString qModeValue(const QVector<QString> &vec);

QVector<int> fibonacciGenerator(int count);
void convertDoubleVectorToStringList(const QVector<double> &inputVec, QStringList &outList);
QStringList getStringListFromDoubleVector(const QVector<double> &inputVec);
const QVector<double> getDoubleVectorFromStringList(const QStringList &inputVec);




void convertStringListToDoubleVector(const QStringList &inputVec, QVector<double> &outList);
void getAllWeightSubsets(unsigned int length, QVector<QString> &rvalue);
void mywait(size_t secs);
void printProgress(const std::string &label, int step, int total );
uint64_t hash_str_uint64(const std::string& str);
unsigned long long hash(const std::string& str);
QString convertCommaInsideApostrophs(const QString &line);

double mean(const std::vector<double> &v);
double median(const std::vector<double> &v);
double variance(const std::vector<double> &v);
double standardDeviation(const std::vector<double> &v);
