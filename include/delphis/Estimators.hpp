#pragma once
#include <QObject>
#include <vector>
#include <map>
#include <string>
#include "Project.hpp"
#include "DistanceMetrics.hpp"
#include "CommonFunctionsContainer.hpp"
#include <QVector>

class Estimator : public QObject
{
public:
    Estimator(QObject *parent = nullptr);
    virtual std::string getName() const { return name; }

    inline virtual double estimate(const QVector<double> &values, const QVector<QVector<double> > &matrix = QVector<QVector<double> >()) const
    {return 0;}
    static void setSizeAdjustedAttributeVals(const QVector<double> &value);

protected:
    std::string name;
    static QVector<double> sizeAdjustedAttributeVals;

};

class Mean : public Estimator
{
public:
    Mean(QObject *parent = nullptr);

    virtual double estimate(const QVector<double> &values, const QVector<QVector<double> > &matrix = QVector<QVector<double> > ()) const;
};

class Median : public Estimator
{
public:
    Median(QObject *parent = nullptr);

    virtual double estimate(const QVector<double> &values, const QVector<QVector<double> > &matrix = QVector<QVector<double> >()) const;
};

class FibonacciSerie : public Estimator
{
public:
    FibonacciSerie(QObject *parent = nullptr);

    virtual double estimate(const QVector<double> &values, const QVector<QVector<double> > &matrix = QVector<QVector<double> >()) const;
};

class EstimatorsContainer : public QObject
{
    public:
        EstimatorsContainer(const QStringList &estimatorsStrList = QStringList());

        virtual ~EstimatorsContainer();

        const std::vector<const Estimator *> &getCurrentEstimators() const;

        const Estimator *getEstimatorByName(const QString &name) const;

        static EstimatorsContainer *instance() {
            if(!instancePtr)
                instancePtr = new EstimatorsContainer;

            return instancePtr;
        }

private:
        static EstimatorsContainer *instancePtr;
        std::vector<const Estimator *> Estimators;

        std::vector<const Estimator *> CurrentEstimators;
};
