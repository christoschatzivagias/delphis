#pragma once

#include <functional>
#include <chrono>
#include <iostream>


struct ScopedNanoTimer
{
    std::chrono::high_resolution_clock::time_point t0;
    std::function<void(int)> cb;
    ScopedNanoTimer()
        : t0(std::chrono::high_resolution_clock::now())
    {
        std::cout << "Begin Counting" << std::endl;
    }
    ~ScopedNanoTimer(void)
    {
        auto  t1 = std::chrono::high_resolution_clock::now();
        auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0).count();
        std::cout << "sec      : " << nanos/10e8 << std::endl;

    }
};
