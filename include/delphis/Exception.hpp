#pragma once

class BadCSVLineException : public std::exception
{
public:
    const char * what () const throw () {
          return "CSV line attributes more than headers";
       }
};


class CorruptedCSVException : public std::exception
{
public:
    const char * what () const throw () {
          return "Unable to read CSV file";
       }
};

class CorruptedIniFileException : public std::exception
{
public:
	const char * what() const throw () {
		return "Error corrupted ini file";
	}
};

class MethodProjectValuesDifferentFromDatabase : public std::exception
{
public:
    const char * what() const throw () {
        return "Error imported method project values are not the same with the corresponding in database";
    }
};

class InvalidIniFileException : public std::exception
{
public:
    const char * what () const throw () {
          return "Invalid ini file format";
       }
};

class InvalidProjectValue: public std::exception
{
public:
    InvalidProjectValue(const std::string &desc = std::string(),
                        const std::string &attr = std::string(),
                        const std::string &value = std::string()) : std::exception() {
        m_description = desc;
        m_attribute = attr;
        m_value = value;

        if(m_description.size() != 0)
            m_message += std::string("Project:") + m_description + std::string("\n") +
                         std::string("Attribute:") + m_attribute + std::string("\n") +
                         std::string("Value:") + m_value;
    }

    virtual const char* what() const throw()
    {
        return m_message.c_str();
    }
private:
    std::string m_message = std::string("Invalid Project Value. Validate the database first!\n");
    std::string m_description = std::string();
    std::string m_attribute = std::string();
    std::string m_value = std::string();

};

class TooLongConfiguration: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Invalid Configuration! Please Try to deactivate some of attributes to be less than 32!";
  }
};

class InvalidAttributeName: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Invalid Attribute Name! It seems that you read a corrupted ini file or a csv without a header line!";
  }
};

class DatabaseOpenError: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Error, unable to open database!";
  }
};

class ImportMethodError: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Error, unable to import method, invalid method file!";
  }
};


class IncompatibleMethodError: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Error, incompatible method can not import attributes do not comply with these in database!";
  }
};

class ImportConfigurationError: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Error, unable to import configuration, invalid configuration file!";
  }
};
