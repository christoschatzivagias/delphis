#pragma once

#include <string>
#include <vector>
#include "Attribute.hpp"
#include "Project.hpp"

class AsciiFileManager
{
	public:
        AsciiFileManager();


        void readAsciiFile(const std::string &);
        void saveAsciiFile(const std::string &);
        void saveArffFile(const std::string &);
        const std::vector<Attribute> &getDBAttributes();
        const std::vector<Project> &getDBProjects();
        const std::vector<Project> &getDBTargets();


        void readArffFile(const std::string &file);
        void readCSVFile(const std::string &file);
        void importCSVTargets(const std::string &file);

	private:
        void tmpArffFileWithoutComments(const std::string &file) const;

        void readAttributes(const std::string &);
        void readProjects(const std::string &);
        void readTargets(const std::string &);
        std::vector<Project> AscDBProjects;
        std::vector<Project> AscDBTargets;
        std::vector<Attribute> AscDBAttributes;
};
