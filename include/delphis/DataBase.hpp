#pragma once

#include <string>
#include <vector>
#include <memory>
#include <exception>
#include "Project.hpp"
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlRelationalTableModel>
#include <QTimer>
#include <QDebug>
#include <QSqlError>

#include "BestParameters.hpp"
inline double validateValue(const QString &prjDesc, const QString &attr, const QString &value);


class DataBase:public QObject
{
    Q_OBJECT
public:
    DataBase(QObject *parent = nullptr);
    static DataBase *instance();


    void setDBAttributes(const std::vector<Attribute> &DBAttrs);
    void setDBProjects(const std::vector<Project> &DBPrjs);
    void setDBTargets(const std::vector<Project> &DBTrgs);
    void clear();
    const std::vector<Attribute> &getDBAttributes() const;
    const std::vector<Project> &getDBProjects() const;
    const std::vector<Project> &getDBTargets() const;
    std::vector<std::string> getDBAttributesNames();

    int getActiveProjectsCount();
    int getAttributeType(const std::string &attrname) const;

    QStringList getProjectValuesByDescription(const QString &desc, const QStringList &attrs);

    const QStringList &getActiveAttributes() const;
    void getActiveAttributesVector(const QStringList &attrs, QVector<QPair<int, QStringList> > &prjsAttrVals, Project::Type type = Project::AS_PROJECT);
    QStringList getActiveAttributeVector(const QString &attr);

    QStringList getActiveProjectsVector() const;

    bool hasAttributes();

    void exec(const std::string &query);
    void clearQuery();

    void updateTables();
    QSqlDatabase getDb() const;
    void addMethod(const AnalogyMethodParameters &analogyParams);
    QString getAttributeDescriptionById(unsigned int id) const;
    QString getProjectDescriptionById(unsigned int id) const;
    QString getTargetDescriptionById(unsigned int id) const;


	void setAttributeStatus(unsigned int index, int status);
	void setProjectStatus(unsigned int index, int status);
	void setTargetStatus(unsigned int index, int status);

    static void standardizeMatrix(const QVector<QVector<double> > &matrix, const QList<int> &attrtypes, QVector<QVector<double> > &stdMatrix);
    static void createIndexMatrix(const QVector<QVector<double> > &matrix, QVector<QPair<int, QVector<double> > > &indexMatrix);
    static void convertSubsetWeightsToWeights(const QVector<QPair<QString, double> > &subsetWeights, QVector<double> &weights);

    void validateCategoricalAttributes(const QStringList &attrs) const;
    void addDBTarget(const Project &trg);
    void addDBTargets(const std::vector<Project> &trgs);

    QList<int> getTypeOfAttributes(const QStringList &attrNames) const;
    bool hasAttribute(const QString &attr);
    bool hasAttributes(const QStringList &attrs);


    void copyProjectsToTargets(const QModelIndexList &prjIds);
    void activateAttributes(const QModelIndexList &attrIds, bool active);
    void activateProjects(const QModelIndexList &prjIds, bool active);
    void activateTargets(const QModelIndexList &prjIds, bool active);

public slots:
    void updateTablesSlot();

signals:
    void resetAttributesModelSignal();
    void resetProjectsModelSignal();
    void resetTargetsModelSignal();
    void updateTablesSignal();
    void attributesInserted();
    void projectsInserted();

private:
	static DataBase *instancePtr;

    QVector<AnalogyMethodParameters> DBMethods;
    std::vector<Project> DBProjects;
    std::vector<Project> DBTargets;
    std::vector<Attribute> DBAttributes;
    void addConnection();
    QSqlDatabase db;
    QTimer updateTimer;
    mutable QStringList activeAttrs;
    std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>("", db);
};

