#pragma once


#include <string>
#include <vector>
#include "Attribute.hpp"

class Project
{
    friend class DataBase;
public:
    enum Type{
        AS_PROJECT = 0,
        AS_TARGET
    };
    Project(int Nr = -1, const std::string &Desc = std::string(), int Status = 1, const std::vector<Attribute> &Attrs = std::vector<Attribute>(), const Type t = AS_PROJECT);

    int getNr() const;
    std::string getDescription() const;
    int getStatus() const;
    std::vector<Attribute> getAttributes() const;
    void addAttribute(const Attribute &);
    const std::string &sqlQuery(enum Type t) const;

private:

    void setNr(int);
    void setDescription(const std::string &);
    void setStatus(int);
    void setAttributes(const std::vector<Attribute> &);
    mutable std::string query;
    int Nr = 0;
    std::string Description;
    int Status = 1;
    std::vector<Attribute> Attributes;
};
