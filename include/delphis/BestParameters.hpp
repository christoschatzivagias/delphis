#pragma once
#include <memory>

#include <QVector>
#include <QMap>
#include <QString>
#include <QVariant>
#include "Jackknife.hpp"
#include "CommonFunctionsContainer.hpp"


class AnalogyMethodParameters
{
    public:
        AnalogyMethodParameters();
        void init();
        void setEstimationAttribute(const QString &EstimationAttr);
        QString getSizeAdjustedAttribute() const;
        QString getEstimationAttribute() const;


        void setAnalogies(int analogies);
        void setStatistic(const QString &statistic);
        void setDistanceMetric(const QString &distancemetric);
        void setMinkowskiLamda(int lamda);
        void setSubset(const QMap<QString, QVariant> &attrSubs);
        void setSubset(const QVector<QPair<QString, double> > &attrSubs);

        void setBestEvaluation(double bestEvaluation);
        int getAnalogies() const;
        QString getStatistic() const;
        QString getDistanceMetric() const;
        QVector<QPair<QString, double> > subset() const;
        QVector<double> getSubsetVectorByAttributesSequence() const;

        double getBestEvaluation() const ;
        int getMinkowskiLamda() const;


        void exportFile(const QString &file) const;
        void importFile(const QString &file);

        QVector<QStringList> getProjectsMatrix(const QList<int> &indices = QList<int>()) const;
        void setProjectsMatrix(const QVector<QStringList> &value);
        void setAttributes(const QStringList &value);
        QStringList getAttributeNames() const;


        void setEvaluationMethod(const QString &value);
        int parametersCount() const {
            return 3 + attributesList.size();
        }


        QString getEvaluationMethod() const;
        QVector<double> getEstimationAttributeVector(const QList<int> &indices = QList<int>()) const;
        void setEstimationAttributeVector(const QVector<double> &value);

        static QVector<QString> DistanceMetricsNames;

        QStringList getAttributeTypes() const;
        void setAttributeTypes(const QStringList &value);

        QStringList getProjectDescriptions() const;
        void setProjectDescriptions(const QStringList &value);

private:
        QStringList attributesList;
        QVector<QStringList> projectsMatrix;
        QMap<QString, QVariant> attributesSubset;

        QVector<double> estimationAttributeVector;

        int analogies = 0;
        QString statistic;
        QString distanceMetric;
        double bestEvaluation = 0;
        int minkowskiLamda = 0;
        QString sizeAdjustedAttribute;
        QString estimationAttribute;
        QString evaluationMethod;
        QStringList attributeNames;
        QStringList attributeWeights;

        QStringList attributeTypes;
        QStringList projectDescriptions;
        void validateMethod();
};


class EvaluationMethod
{
    public:

        EvaluationMethod() {}

        virtual double evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethodParameters &par, AnalogyMethodParameters &bestPar) {
            return -1;
        }

        std::string getName() const {
            return name;
        }
    protected:
        std::string name = "EvaluationMethod";

};


class MMRE : public EvaluationMethod
{
public:

    MMRE() {
        name = "MMRE";
    }

    virtual double evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethodParameters &par, AnalogyMethodParameters &bestPar);
};

class MDMRE : public EvaluationMethod
{
public:
    MDMRE() {
        name = "MDMRE";
    }

    virtual double evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethodParameters &par, AnalogyMethodParameters &bestPar);
};


class PRED25 : public EvaluationMethod
{
public:
    PRED25() {
        name = "PRED25";
    }

    virtual double evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethodParameters &par, AnalogyMethodParameters &bestPar);
};


class EvaluationMethodFactory
{
public:
    EvaluationMethodFactory(const QString &evalmethodstr = QString(),
                            const QString &estimationAttr = QString(),
                            const QStringList &prjDescs = QStringList(),
                            const QVector<QStringList > &prjVec = QVector<QStringList>(),
                            const QVector<double> estimAttrVec = QVector<double>(),
                            const QStringList &attrs = QStringList(),
                            const QStringList &attrTypes = QStringList());

    EvaluationMethodFactory(const EvaluationMethodFactory &evalf) {
        bestPar = evalf.bestPar;
        if(evalf.method.get()->getName() == "MMRE") {
            method = std::unique_ptr<EvaluationMethod>(new MMRE);
        } else if(evalf.method.get()->getName() == "MDMRE") {
            method = std::unique_ptr<EvaluationMethod>(new MDMRE);
        } else if(evalf.method.get()->getName() == "PRED25") {
            method = std::unique_ptr<EvaluationMethod>(new PRED25);
        }
    }


    void operator=(const EvaluationMethodFactory &evalf)
    {
        bestPar = evalf.bestPar;
        if(evalf.method.get()->getName() == "MMRE") {
            method = std::unique_ptr<EvaluationMethod>(new MMRE);
        } else if(evalf.method.get()->getName() == "MDMRE") {
            method = std::unique_ptr<EvaluationMethod>(new MDMRE);
        } else if(evalf.method.get()->getName() == "PRED25") {
            method = std::unique_ptr<EvaluationMethod>(new PRED25);
        }
    }

    double evaluate(const QVector<double> &estimations, const QVector<double> &realValues, const AnalogyMethod &jack);

    std::unique_ptr<EvaluationMethod> method = nullptr;
    AnalogyMethodParameters bestPar;
};


