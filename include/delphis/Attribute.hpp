#pragma once

#include <QVariant>
#include <string>


class Attribute
{
    friend class AsciiFileManager;
    friend class DataBase;
	public:
        Attribute(int Nr = 0, const std::string &Description = std::string(), int Type = 1, int Status = 1, const std::string &Value = std::string("0"), double WeightFactor = 1.0f);

        int getNr() const;
        std::string getDescription() const;
        int getType() const;
        int getStatus() const;
        double getWeightFactor() const;

        QString getValue() const;

        std::string sqlQuery() const;
        bool validate() const;

private:
        void setType(int Type);
        void setStatus(int status);
        void setWeightFactor(double WeightFactor);

        void setValue(const QString &Value);


        int Nr;
        std::string Description;
        int Type;
        int Status;
        double WeightFactor;

        QVariant Value;
};


