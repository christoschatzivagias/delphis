#pragma once
#include "DataBase.hpp"


class CategoricalTransformer
{
public:
    CategoricalTransformer(DataBase *db);

    QVector<QPair<QString, QSet<QString> > > getAttributesValues() const;

    void setTransformedValue(const QString &attrname, const QString &initvalue, const QString &tranvalue) {
        QSqlQuery qr(database->getDb());
        QString queryStr = "update projects set " + attrname + "=" + tranvalue + " where " + attrname + "='" + initvalue + "'";
        qr.exec(queryStr);
        database->updateTables();
    }

private:
    QVector<QPair<QString, QSet<QString> > > attributesValues;
    DataBase *database = nullptr;
};

