#pragma once
#include <QVector>
#include <QPair>

class AnalogyMethod;

class Jackknife
{
public:
    Jackknife(const AnalogyMethod &analogy,
              const QVector<QPair<int, QVector<double> > > &indexedMatrix,
              const QVector<QPair<int, QVector<double> > > &indexedStdMatrix,
              const QVector<double> &realVals,
              const QVector<QPair<QString, double> > &weightsPair);

    void perform() const;

    const  QVector<double> &getEstimates() const;
private:
    const AnalogyMethod *m_analogy = nullptr;
    const QVector<QPair<int, QVector<double> > > *m_indexedMatrix = nullptr;
    const QVector<QPair<int, QVector<double> > > *m_indexedStdMatrix = nullptr;
    const QVector<double> *m_realVals = nullptr;
    const QVector<QPair<QString, double> > *m_weightsPair = nullptr;
    mutable QVector<double> m_estimates;
};
