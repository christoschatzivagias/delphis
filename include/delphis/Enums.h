#pragma once

enum ReadFileError {
		NoError = 0,
		SyntaxError = 1,
		DuplicateAttributeName,
		InvalidAttributeType,
		InvalidAttributeStatus,
		DuplicateProjectName,
		InvalidProjectStatus,
		InvalidValueForNumericAttribute,
		IOError
};

enum AttributeType {
		NUMERICAL = 1,
		CATEGORICAL = 2,
		WRONGTYPE = 3	
};


enum Statistic {
                InvalidStatistic = 0,
                Mean = 1,
                Median = 2,
                SizeAdjustedMean = 3,
                SizeAdjustedMedian = 4 
};

enum DistanceMetric {
                InvalidDistanceMetric = 0,
                Euclidean = 1,
                Manhattan = 2,
                Canberra = 3,
                Czekanowski = 4,
                Chebychev = 5,
                Kaufmann_Roussee = 6,
                Minkowski = 7
};

enum EvaluationMetric {
                InvalidEvaluationMetric = 0,
                Mmre = 1,
                Mdmre = 2,
                Pred = 3
};

enum SvmModelType {
                InvalidSvmModelType = 0,
                EpsilonSVR = 1,
                NuSVR = 2
};

enum KernelFunction {
		InvalidKernelFunction = 0,
		Linear = 1,
		RBF = 2,
		Polynomial = 3,
		Sigmoid = 4
};

enum ClusteringType {
		NoCluster = 0,
		Spectral = 1,
		KMeans = 2
};

