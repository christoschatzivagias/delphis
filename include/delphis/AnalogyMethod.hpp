#pragma once

#include "Project.hpp"
#include "DistanceMetrics.hpp"
#include "DataBase.hpp"

#include <QVector>
#include <QPair>


class Estimator;
class DataBase;


class AnalogyMethod
{
public:
    AnalogyMethod(const QVector<QPair<QString, double> > &weightsPair = QVector<QPair<QString, double> >(),
                  const Distance *dst = nullptr,
                  const Estimator *est = nullptr, unsigned int anlg = 0);

    double estimate(const QVector<QPair<int, QVector<double> > > &matrix, const QVector<QPair<int, QVector<double> > > &stdMatrix, const QVector<double> &prj, const QVector<double> &weights, const QVector<double> &estimVec) const;


    std::string getEstimatorName() const;
    std::string getDistanceMetricName() const;
    unsigned int getAnalogies() const;

    QVector<QPair<QString, double> > getWeightsPair() const;

private:
    void calculateDistances(const QVector<QPair<int,QVector<double> > > &matrix, const QVector<double> &prj, const QVector<double> &weights, QVector<QPair<int, double> > &distances) const;
    const Distance *distanceMetric = nullptr;
    const Estimator *estimator = nullptr;
    QVector<QPair<QString, double> > weightsPair;
    unsigned int analogies = 3;

};


