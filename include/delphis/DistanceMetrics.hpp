#pragma once
#include <QObject>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include "Project.hpp"

class Distance;

struct floatoper
{
  bool operator()(double s1, double s2) const
  {
    return (s1 < s2);
  }
};

class DistanceMetricsContainer : public QObject
{
    private:
        std::vector<const Distance *> CurrentDistances;
        std::vector<const Distance *> Distances;

        static DistanceMetricsContainer *instancePtr;

    public:
        static DistanceMetricsContainer *instance();

        virtual ~DistanceMetricsContainer(){

        }
        enum Type {
           EUCLIDEAN = 0,
           MANHATTAN,
           CANBERRA,
           CHEBYCHEV,
           CZEKANOWSKI,
           MINKOWSKI,
           KAUFMANROUSSEEUW
        };

        DistanceMetricsContainer(const QStringList &distMetics = QStringList());
        const std::vector<const Distance *> &getCurrentDistances() const;
        const Distance *getDistanceMetricByName(const QString &name) const;
};

class Distance : public QObject
{
public:
    Distance(QObject *parent = nullptr) : QObject(parent){

    }
    virtual ~Distance(){

    }
    inline virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const {
        return 0;
    }
    std::string getName() const;

    bool getActive() const;
    void setActive(bool value);

protected:
    std::string name;
    DistanceMetricsContainer::Type typeId;
    bool active = true;
};

class Euclidean : public Distance
{
public:
    Euclidean(QObject *parent = nullptr);
    virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const;
private:
};

class Manhattan : public Distance
{
public:
    Manhattan(QObject *parent = nullptr);
    virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const;
};


class Canberra : public Distance
{
public:
    Canberra(QObject *parent = nullptr);
    virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const;
};

class Chebychev : public Distance
{
public:
    Chebychev(QObject *parent = nullptr);
    virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const;
};

class Czekanowski : public Distance
{
public:
    Czekanowski(QObject *parent = nullptr);
    virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const;
};

class Minkowski : public Distance
{
public:
    Minkowski(QObject *parent = nullptr,int lamda = 3);
    virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const;

private:
    int lamda = 3;
};

class KaufmanRousseeuw : public Distance
{
public:
    KaufmanRousseeuw(QObject *parent = nullptr);
    virtual double calculate(const std::vector<double> & point1, const std::vector<double> & point2, const std::vector<double> &weights) const;
};
