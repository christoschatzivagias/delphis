#ifndef CSVREADER_HPP
#define CSVREADER_HPP

#include <QVector>
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QString>
#include <QStringList>
#include <QDir>
#include <QDateTime>
#include <QtMath>


class CSVReader
{
public:
    explicit CSVReader();
    void ReadCSV(const QString &filename, QVector<QStringList > &vectors);
};

#endif // FILERWKIT_H
