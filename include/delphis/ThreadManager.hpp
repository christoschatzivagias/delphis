#pragma once
#include <QObject>
#include <QFutureWatcher>
#include <QFuture>
#include <QMutex>


class ThreadManager : public QObject
{
	Q_OBJECT
	public:
		ThreadManager();
        void process(const QFuture<void> &ft, const QString &str = QString());
		static ThreadManager *instance();
		QMutex mutex;
        void setBusy() {
            emit setBusySignal();
        }

        void resetBusy() {
            emit resetBusySignal();
        }
	signals:
		void progressValueChanged(int value);
        void setBusySignal();
        void resetBusySignal();

        void messageChanged(QString message, int timeout = 0);

	private:
		static ThreadManager *instancePtr;
		QFutureWatcher<void> futureWatcher;
};
