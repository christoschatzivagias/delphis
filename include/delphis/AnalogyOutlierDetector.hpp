#pragma once

#include <QString>
#include <QPair>

class AnalogyOutlierDetector
{
public:
    AnalogyOutlierDetector();
    QVector<QPair<double, QString> > search(const QString &evaluationMethod, const QVector<QPair<QString, double> > &weightsPair, const QString &estimattr, const QString &distanceMetric, const QString &estimator, int analogies);

private:
    QVector<QPair<int, double> > evaluations;

};
