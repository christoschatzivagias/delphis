#include "AsciiFileManager.hpp"
#include <assert.h>
#include <string>
#define CC CURRENT_SOURCE_DIR
int main()
{
    AsciiFileManager asciifm;

    std::string path(CURRENT_SOURCE_DIR);
    path += "/databases/albrecht.ini";
    asciifm.readAsciiFile(path);
    printf("projects %d %s\n",(int)asciifm.getDBProjects().size(), path.c_str());
    assert(asciifm.getDBProjects().size() == 24);
    return 0;
}
