#include <assert.h>
#include <QVector>
#include "Estimators.hpp"
int main()
{
    Median median;

    double value = median.estimate(QVector<double>{4,3}, QVector<QVector<double> > ());
    assert(value == 3.5f);
    return 0;
}
