#include <assert.h>
#include "DistanceMetrics.hpp" 
#include <vector>
int main()
{
    Minkowski dist;
    std::vector<double> vec1 = {1, 2, 3}, vec2 = {4, 5, 6}, weights = {1, 0.5f, 0};
    double value = dist.calculate(vec1, vec2, weights);
    printf("value = %.32f\n",value);
    assert(value == 3.43414272765999539771542004018556); 
    return 0;
}
